# TOIS Correlation Matlab Code #

This is the Matlab code for running the experiments described in the paper

N. Ferro, (2017). What Does Affect the Correlation Among Evaluation Measures?.

This code is based on the MATTERS Matlab library available at: [http://matters.dei.unipd.it/](http://matters.dei.unipd.it/)

For more information on the Grid-of-Points (GoP), please visit: [http://gridofpoints.dei.unipd.it/](http://gridofpoints.dei.unipd.it/)

