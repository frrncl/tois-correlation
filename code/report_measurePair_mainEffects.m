%% report_measurePair_mainEffects
% 
% Reports the main effects of the measure pair factor correlations for the 
% given tracks and saves them to a |.tex| file.
%
%% Synopsis
%
%   [] = report_measurePair_mainEffects(tag, varargin)
%  
%
% *Parameters*
%
% * *|tag|* - the identifier of research question whose main effects have
% to be processed.
% * *|trackID|* - the identifier of the track to process.
%
% *Returns*
%
% Nothing
%

%% Information
% 
% * *Author*: <mailto:ferro@dei.unipd.it Nicola Ferro>
% * *Version*: 1.00
% * *Since*: 1.00
% * *Requirements*: MATTERS 1.0 or higher; Matlab 2013b or higher
% * *Copyright:* (C) 2015 <http://ims.dei.unipd.it/ Information 
% Management Systems> (IMS) research group, <http://www.dei.unipd.it/ 
% Department of Information Engineering> (DEI), <http://www.unipd.it/ 
% University of Padua>, Italy
% * *License:* <http://www.apache.org/licenses/LICENSE-2.0 Apache License, 
% Version 2.0>

%%
%{
 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at
 
      http://www.apache.org/licenses/LICENSE-2.0
 
 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.
%}

function [] = report_measurePair_mainEffects(tag, varargin)

    persistent CORR_LIST CORR_LIST_LENGTH;
    
    if isempty(CORR_LIST)
        CORR_LIST = {'tau', 'tauAP'};
        CORR_LIST_LENGTH = length(CORR_LIST);
    end;

    % check the number of input arguments
    narginchk(2, inf);

    % setup common parameters
    common_parameters;
        
    tracks = length(varargin);
    
    for t = 1:tracks
        
        % check that trackID is a non-empty string
        validateattributes(varargin{t}, {'char', 'cell'}, {'nonempty', 'vector'}, '', 'trackID');
        
        if iscell(varargin{t})            
            % check that trackID is a cell array of strings with one element
            assert(iscellstr(varargin{t}) && numel(varargin{t}) == 1, ...
                'MATTERS:IllegalArgument', 'Expected trackID to be a cell array of strings containing just one string.');
        end
        
        % remove useless white spaces, if any, and ensure it is a char row
        varargin{t} = char(strtrim(varargin{t}));
        varargin{t} = varargin{t}(:).';
                
        % check that trackID assumes a valid value
        validatestring(varargin{t}, ...
            EXPERIMENT.collection.list, '', 'trackID');
    end;
    
    if (tracks > 1)
        tracks_list = strjoin(varargin, '');
    else
        tracks_list = varargin{1};
    end;
   
  
    % start of overall computations
    startComputation = tic;

    fprintf('\n\n######## Report %s measure pair main effects on collection %s (%s) ########\n\n', ...
        tag, tracks_list, EXPERIMENT.label.paper);

    fprintf('+ Settings\n');
    fprintf('  - computed on %s\n', datestr(now, 'yyyy-mm-dd at HH:MM:SS'));
    fprintf('  - analysis %s\n', tag);
    fprintf('  - tracks %s\n', tracks_list);

    % each row is a correlation between a measure pair, each column is
    % track, each plane is a coefficient
    correlations = NaN(CORR_LIST_LENGTH, EXPERIMENT.correlation.labels.number);

    fprintf('+ Loading data\n');
    
    for c = 1:CORR_LIST_LENGTH
        
        anovaMeID = EXPERIMENT.pattern.identifier.anovaMe(tag, CORR_LIST{c}, tracks_list);
        
        serload(EXPERIMENT.pattern.file.anova(tag, CORR_LIST{c}, tracks_list), {anovaMeID, 'me'})
        
        correlations(c, :) = me.factorA.mean;
        
        % free space
        clear('me');
            
    end; % correlation
    
    
    % the file where the report has to be written
    fid = fopen(EXPERIMENT.pattern.file.report(tag, sprintf('mp_me_%s', tracks_list)), 'w');


    fprintf(fid, '\\documentclass[11pt]{article} \n\n');

    fprintf(fid, '\\usepackage{amsmath}\n');
    fprintf(fid, '\\usepackage{multirow}\n');
    fprintf(fid, '\\usepackage{colortbl}\n');
    fprintf(fid, '\\usepackage{lscape}\n');
    fprintf(fid, '\\usepackage{pdflscape}\n');
    fprintf(fid, '\\usepackage[a3paper]{geometry}\n\n');
    
    fprintf(fid, '\\usepackage{xcolor}\n');
    fprintf(fid, '\\definecolor{lightgrey}{RGB}{219, 219, 219}\n');
    fprintf(fid, '\\definecolor{verylightblue}{RGB}{204, 229, 255}\n');
    fprintf(fid, '\\definecolor{lightblue}{RGB}{124, 216, 255}\n');
    fprintf(fid, '\\definecolor{blue}{RGB}{32, 187, 253}\n');
    
    fprintf(fid, '\\begin{document}\n\n');
    
    fprintf(fid, '\\title{Summary Report on the Measure Pair Main Effects for %s}\n\n', upper(tag));
    
    fprintf(fid, '\\maketitle\n\n');
    
    fprintf(fid, 'Tracks:\n');
    fprintf(fid, '\\begin{itemize}\n');
        
    % for each track
    for t = 1:tracks
        fprintf(fid, '\\item %s\n', EXPERIMENT.collection.(varargin{t}).name);
    end;
    
    fprintf(fid, '\\end{itemize}\n');
    
    
    %fprintf(fid, '\\begin{landscape}  \n');
    fprintf(fid, '\\begin{table}[p] \n');
    % fprintf(fid, '\\tiny \n');
    fprintf(fid, '\\centering \n');
    %fprintf(fid, '\\hspace*{-6.5em} \n');
    
    fprintf(fid, '\\begin{tabular}{|ll|*{%d}{r|}} \n', EXPERIMENT.measure.number);
    
    fprintf(fid, '\\hline \n');
    
    fprintf(fid, '& ');
    for m = 1:EXPERIMENT.measure.number
        fprintf(fid, '& \\multicolumn{1}{c|}{\\textbf{%s}} ', EXPERIMENT.measure.getAcronym(m));
    end; % measure
    fprintf(fid, '\\\\ \n');
    
    fprintf(fid, '\\hline \n');
    
    
    % for each correlation pair
    for i = 1:EXPERIMENT.measure.number
        
        
        % tau 
        fprintf(fid, '\\multirow{2}{*}{\\textbf{%s}} & $\\tau$ ', EXPERIMENT.measure.getAcronym(i));
                        
        for j = 1:EXPERIMENT.measure.number
            
            if (j < i)
                fprintf(fid, '& -- ');
            elseif (j == i)
                fprintf(fid, '& %.4f ', 1.000);
            else
                % the index of this pair in the vectors
                idx = ismember(EXPERIMENT.correlation.labels.list, sprintf('%s_%s', EXPERIMENT.measure.getID(i), EXPERIMENT.measure.getID(j)));
                
                fprintf(fid, '& %.4f ', correlations(1, idx));
            end;
        end;
        fprintf(fid, '\\\\ \n');
        
        % tauAP 
        fprintf(fid, ' & $\\tau_{AP}$ ');
        
        
        for j = 1:EXPERIMENT.measure.number            
            if (j < i)
                fprintf(fid, '& -- ');
            elseif (j == i)
                fprintf(fid, '& %.4f ', 1.000);
            else
                % the index of this pair in the vectors
                idx = ismember(EXPERIMENT.correlation.labels.list, sprintf('%s_%s', EXPERIMENT.measure.getID(i), EXPERIMENT.measure.getID(j)));
                
                fprintf(fid, '& %.4f ', correlations(2, idx));                
            end;
        end;
        fprintf(fid, '\\\\ \n');
        
        fprintf(fid, '\\hline \n');
    end;
           
    fprintf(fid, '\\end{tabular} \n');
    
    fprintf(fid, '\\caption{%s Measure Pair main effects of the $\\tau$ and $\\tau_{AP}$ correlation coefficients.}\n', upper(tag));
    
    fprintf(fid, '\\label{tab:%s-mp-me} \n', tag);
    
    fprintf(fid, '\\end{table} \n\n');
    
    %fprintf(fid, '\\end{landscape}  \n');
    
    fprintf(fid, '\\end{document} \n\n');
    
    
    fclose(fid);
    
    
               
    fprintf('\n\n######## Total elapsed time for reporting %s measure pair main effects on collection %s (%s): %s ########\n\n', ...
            tag, tracks_list, EXPERIMENT.label.paper, elapsedToHourMinutesSeconds(toc(startComputation)));

    diary off;
end
