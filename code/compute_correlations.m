
function [] = compute_correlations(trackID, topicsID, remove1Q, varargin)
    compute_correlation(trackID, 's0010', topicsID, remove1Q, varargin{:});
    compute_correlation(trackID, 's0025', topicsID, remove1Q, varargin{:});
    compute_correlation(trackID, 's0050', topicsID, remove1Q, varargin{:});
    compute_correlation(trackID, 's0075', topicsID, remove1Q, varargin{:});
    compute_correlation(trackID, 's0100', topicsID, remove1Q, varargin{:});
    compute_correlation(trackID, 's0125', topicsID, remove1Q, varargin{:});
    compute_correlation(trackID, 's0150', topicsID, remove1Q, varargin{:});
    compute_correlation(trackID, 's0200', topicsID, remove1Q, varargin{:});
    compute_correlation(trackID, 's0250', topicsID, remove1Q, varargin{:});
    compute_correlation(trackID, 's0500', topicsID, remove1Q, varargin{:});
    compute_correlation(trackID, 's0750', topicsID, remove1Q, varargin{:});
    compute_correlation(trackID, 's1000', topicsID, remove1Q, varargin{:});
    compute_correlation(trackID, 's1250', topicsID, remove1Q, varargin{:});
end