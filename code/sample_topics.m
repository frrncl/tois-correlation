%% sample_topics
% 
% Samples the requested number of topics from a track and saves them 
% to a |.mat| file.
%
%% Synopsis
%
%   [] = sample_topics(trackID, varargin)
%  
%
% *Parameters*
%
% * *|trackID|* - the identifier of the track to process.
% * *|topicsID|* - the identifier of the topics sample to be used.
% computed.
%
% *Returns*
%
% Nothing
%

%% Information
% 
% * *Author*: <mailto:ferro@dei.unipd.it Nicola Ferro>
% * *Version*: 1.00
% * *Since*: 1.00
% * *Requirements*: MATTERS 1.0 or higher; Matlab 2013b or higher
% * *Copyright:* (C) 2015 <http://ims.dei.unipd.it/ Information 
% Management Systems> (IMS) research group, <http://www.dei.unipd.it/ 
% Department of Information Engineering> (DEI), <http://www.unipd.it/ 
% University of Padua>, Italy
% * *License:* <http://www.apache.org/licenses/LICENSE-2.0 Apache License, 
% Version 2.0>

%%
%{
 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at
 
      http://www.apache.org/licenses/LICENSE-2.0
 
 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.
%}

function [] = sample_topics(trackID, varargin)
   
    % check the number of input arguments
    narginchk(2, inf);

    % setup common parameters
    common_parameters;
        
    % check that trackID is a non-empty string
    validateattributes(trackID, {'char', 'cell'}, {'nonempty', 'vector'}, '', 'trackID');

     if iscell(trackID)
        % check that trackID is a cell array of strings with one element
        assert(iscellstr(trackID) && numel(trackID) == 1, ...
            'MATTERS:IllegalArgument', 'Expected trackID to be a cell array of strings containing just one string.');
    end

    % remove useless white spaces, if any, and ensure it is a char row
    trackID = char(strtrim(trackID));
    trackID = trackID(:).';
    
    % check that trackID assumes a valid value
    validatestring(trackID, ...
        EXPERIMENT.collection.list, '', 'trackID');
        
    % load the first measure and get the topic identifiers out of it
    measureID = EXPERIMENT.pattern.identifier.measure(EXPERIMENT.measure.list{1}, trackID);
                
    % load the measure in the tmp variable
    serload(EXPERIMENT.pattern.file.measure(trackID, measureID), {measureID, 'tmp'});    
    
    % the list of all the topics
    topics = tmp.Properties.RowNames(:).';
    
    clear(measureID, 'tmp');
    
    for k = 1:length(varargin)

        topicsID = varargin{k};
        
        % check that topicsID is a non-empty string
        validateattributes(topicsID, {'char', 'cell'}, {'nonempty', 'vector'}, '', 'topicsID');
        
        if iscell(topicsID)
            % check that topicsID is a cell array of strings with one element
            assert(iscellstr(topicsID) && numel(topicsID) == 1, ...
                'MATTERS:IllegalArgument', 'Expected topicsID to be a cell array of strings containing just one string.');
        end
        
        % remove useless white spaces, if any, and ensure it is a char row
        topicsID = char(strtrim(topicsID));
        topicsID = topicsID(:).';
        
        % check that topicsID assumes a valid value
        validatestring(topicsID, ...
            EXPERIMENT.topics.list, '', 'topicsID');
        
        samples = cell(EXPERIMENT.correlation.subjects, EXPERIMENT.topics.(topicsID).sample);
        
        for s = 1:EXPERIMENT.correlation.subjects
            samples(s, :) = EXPERIMENT.topics.(topicsID).compute(topics);
        end;
                
        sampleID = EXPERIMENT.pattern.identifier.sample.topics(topicsID, EXPERIMENT.correlation.subjects, trackID);
        
        eval(sprintf('%s = samples;', sampleID));
        
        %sersave(EXPERIMENT.pattern.file.sample(sampleID), sampleID(:));
        save(EXPERIMENT.pattern.file.sample(sampleID), sampleID);
        
        clear('samples', sampleID);
    end;
     
end
