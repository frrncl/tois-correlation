%% all_systems
% 
% List all the systems in a GoP.

%% Synopsis
%
%   [list] = all_systems()
%  
%
% *Returns*
%
% * *|list|*  - the list of all the system identifiers in a GoP.

%% Information
% 
% * *Author*: <mailto:ferro@dei.unipd.it Nicola Ferro>
% * *Version*: 1.00
% * *Since*: 1.00
% * *Requirements*: Matlab 2015b or higher
% * *Copyright:* (C) 2016 <http://ims.dei.unipd.it/ Information 
% Management Systems> (IMS) research group, <http://www.dei.unipd.it/ 
% Department of Information Engineering> (DEI), <http://www.unipd.it/ 
% University of Padua>, Italy
% * *License:* <http://www.apache.org/licenses/LICENSE-2.0 Apache License, 
% Version 2.0>

%%
%{
 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at
 
      http://www.apache.org/licenses/LICENSE-2.0
 
 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.
%}

%%

function [list] = all_systems()

    % handles to anonymous functions to ease computations
    persistent NAME;
    
    % initialize anonymous functions
    if isempty(NAME)        
        % create the name of a system
        NAME = @(stop, stem, model) sprintf('%1$s_%2$s_%3$s_noltr_medium', stop, stem, model);
    end;

    % load common parameters
    common_parameters
    
    % total number of elements in the list
    N = EXPERIMENT.taxonomy.stop.number *  EXPERIMENT.taxonomy.lugall.number * EXPERIMENT.taxonomy.model.number;
    
    % the name of the systems
    list = cell(1, N);
    
    % the current element in the list
    currentElement = 1;
    
    for stop = 1:EXPERIMENT.taxonomy.stop.number
        for lugall = 1:EXPERIMENT.taxonomy.lugall.number
            for model = 1:EXPERIMENT.taxonomy.model.number
                
                list{currentElement} = NAME(EXPERIMENT.taxonomy.stop.id{stop}, ...
                    EXPERIMENT.taxonomy.lugall.id{lugall}, EXPERIMENT.taxonomy.model.id{model});
                
                currentElement = currentElement + 1;
            end; % model
        end; % stem
    end; % stop
    
end

