%% rq1_plot_interactionEffects
% 
% Plots the interaction effects for the Measure Pair/Topic Size/System Size Effects ANOVA.

%% Synopsis
%
%   [] = rq1_plot_interactionEffects(trackID, correlationID)
%  
% *Parameters*
%
% * *|trackID|* - the identifier of the track for which the processing is
% performed.
% * *|correlationID|* - the identifier of correlation to be plotted.
%
% *Returns*
%
% Nothing.

%% Information
% 
% * *Author*: <mailto:ferro@dei.unipd.it Nicola Ferro>
% * *Version*: 1.00
% * *Since*: 1.00
% * *Requirements*: Matlab 2015b or higher
% * *Copyright:* (C) 2016 <http://ims.dei.unipd.it/ Information 
% Management Systems> (IMS) research group, <http://www.dei.unipd.it/ 
% Department of Information Engineering> (DEI), <http://www.unipd.it/ 
% University of Padua>, Italy
% * *License:* <http://www.apache.org/licenses/LICENSE-2.0 Apache License, 
% Version 2.0>

%%
%{
 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at
 
      http://www.apache.org/licenses/LICENSE-2.0
 
 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.
%}

%%

function [] = rq1_plot_interactionEffects(trackID, correlationID)

    persistent TAG TOPICS TOPS;
    
    if isempty(TAG)
        TAG = 'rq1';
        
        TOPICS = [10 20 30 40 50 60 70];
        TOPS = length(TOPICS);
        TOPICS = strtrim(cellstr(num2str(TOPICS.'))).';                
    end;

    % check the number of input parameters
    narginchk(2, 2);

    % load common parameters
    common_parameters

    % check that trackID is a non-empty string
    validateattributes(trackID, {'char', 'cell'}, {'nonempty', 'vector'}, '', 'trackID');

     if iscell(trackID)
        % check that trackID is a cell array of strings with one element
        assert(iscellstr(trackID) && numel(trackID) == 1, ...
            'MATTERS:IllegalArgument', 'Expected trackID to be a cell array of strings containing just one string.');
    end

    % remove useless white spaces, if any, and ensure it is a char row
    trackID = char(strtrim(trackID));
    trackID = trackID(:).';
    
    % check that trackID assumes a valid value
    validatestring(trackID, ...
        EXPERIMENT.collection.list, '', 'trackID');
      
    % check that correlationID is a non-empty string
    validateattributes(correlationID, {'char', 'cell'}, {'nonempty', 'vector'}, '', 'correlationID');
    
    if iscell(correlationID)
        % check that correlationID is a cell array of strings with one element
        assert(iscellstr(correlationID) && numel(correlationID) == 1, ...
            'MATTERS:IllegalArgument', 'Expected correlationID to be a cell array of strings containing just one string.');
    end
    
    % remove useless white spaces, if any, and ensure it is a char row
    correlationID = char(strtrim(correlationID));
    correlationID = correlationID(:).';
    
    % check that correlationID assumes a valid value
    validatestring(correlationID, ...
        EXPERIMENT.correlation.list, '', 'correlationID');
    
    % start of overall computations
    startComputation = tic;
            
    fprintf('\n\n######## Plotting %s interaction effects for correlation %s on collection %s (%s) ########\n\n', ...
        TAG, correlationID, EXPERIMENT.collection.(trackID).name, EXPERIMENT.label.paper);
    
    fprintf('+ Settings\n');
    fprintf('  - computed on %s\n', datestr(now, 'yyyy-mm-dd at HH:MM:SS'));
    fprintf('  - analysis %s\n', TAG);
                   
    % always load the tau data to set the order 
    anovaIeID = EXPERIMENT.pattern.identifier.anovaIe(TAG, 'tau', trackID);
        
    serload(EXPERIMENT.pattern.file.anova(TAG, 'tau', trackID), {anovaIeID, 'ie'})
    
    % the measure pairs ordered by descending tau correlation
    [~, idx] = sort(mean(ie.factorBA.mean), 'descend');
    
    
    anovaIeID = EXPERIMENT.pattern.identifier.anovaIe(TAG, correlationID, trackID);
        
    serload(EXPERIMENT.pattern.file.anova(TAG, correlationID, trackID), {anovaIeID, 'ie'})
    
    labels = cell(1, EXPERIMENT.correlation.labels.number);
    
    % for each label
    for l = 1:EXPERIMENT.correlation.labels.number
        
        first = EXPERIMENT.measure.(EXPERIMENT.correlation.labels.first{l}).acronym;
        second = EXPERIMENT.measure.(EXPERIMENT.correlation.labels.second{l}).acronym;
        
        labels{l} = sprintf('%s vs %s ', first, second);
    end;
        
    colors = parula(EXPERIMENT.correlation.labels.number);
        
    currentFigure = figure('Visible', 'on');
        
        data = ie.factorBA.mean.';
        
        %[~, idx] = sort(mean(data.'), 'descend');
        
        data = data(idx, :);
        
        hold on;
        % for each label
        for l = 1:EXPERIMENT.correlation.labels.number
            if strcmp(EXPERIMENT.correlation.labels.list{idx(l)}, 'ap_twist')
                plot(1:TOPS, data(l, :), 'Color', colors(l, :), ...
                    'LineStyle', '-',  'LineWidth', 7);%, 'Marker', 's', 'MarkerSize', 10, ...
                    %'MarkerEdgeColor', colors(l, :), ...
                    %'MarkerFaceColor', colors(l, :));  
             elseif strcmp(EXPERIMENT.correlation.labels.list{idx(l)}, 'p10_rbp')
                plot(1:TOPS, data(l, :), 'Color', colors(l, :), ...
                    'LineStyle', ':',  'LineWidth', 7);%, 'Marker', 's', 'MarkerSize', 10, ...
                    %'MarkerEdgeColor', colors(l, :), ...
                    %'MarkerFaceColor', colors(l, :));  
            elseif strcmp(EXPERIMENT.correlation.labels.list{idx(l)}, 'rprec_rbp')
                plot(1:TOPS, data(l, :), 'Color', colors(l, :), ...
                    'LineStyle', '-',  'LineWidth', 7);%, 'Marker', 's', 'MarkerSize', 10, ...
                    %'MarkerEdgeColor', colors(l, :), ...
                    %'MarkerFaceColor', colors(l, :));
            else
                plot(1:TOPS, data(l, :), 'Color', colors(l, :), ...
                    'LineStyle', '-',  'LineWidth', 1);%, 'Marker', 's', 'MarkerSize', 10, ...
                    %'MarkerEdgeColor', colors(l, :), ...
                    %'MarkerFaceColor', colors(l, :));
            end;
        end;
            
        ax1 = gca;
        ax1.TickLabelInterpreter = 'tex';
        ax1.FontSize = 42;
        
        ax1.XTick = 1:TOPS;
        ax1.XTickLabel = TOPICS;
        %ax1.XTickLabelRotation = 90;
        
        ax1.XLabel.Interpreter = 'tex';
        ax1.XLabel.String = 'Topic Size';
        
        ax1.YLabel.Interpreter = 'tex';
        ax1.YLabel.String = sprintf('%s Marginal Mean', EXPERIMENT.correlation.(correlationID).label.tex);
        
        title('Measure Pair*Topic Size Interaction', 'FontSize', 42, 'Interpreter', 'tex');
        
        legend(labels(idx), 'Location', 'BestOutside', 'Interpreter', 'tex')
        

    currentFigure.PaperPositionMode = 'auto';
    currentFigure.PaperUnits = 'centimeters';
    currentFigure.PaperSize = [82 62];
    currentFigure.PaperPosition = [1 1 80 60];
    
    figureID = EXPERIMENT.pattern.identifier.figure.interactionEffects(TAG, correlationID, trackID);
    
    print(currentFigure, '-dpdf', EXPERIMENT.pattern.file.figure(TAG, figureID));
    
    close(currentFigure)
    
       
    fprintf('\n\n######## Total elapsed time for plotting %s interaction effects for correlation %s on collection %s (%s): %s ########\n\n', ...
           TAG, correlationID, ...
           EXPERIMENT.collection.(trackID).name, EXPERIMENT.label.paper, elapsedToHourMinutesSeconds(toc(startComputation)));
       
    diary off;
end

