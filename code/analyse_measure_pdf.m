%% compute_measures
% 
% Computes measures for the given track and saves them to a |.mat| file.
%
%% Synopsis
%
%   [] = compute_measures(trackID, startMeasure, endMeasure)
%  
%
% *Parameters*
%
% * *|trackID|* - the identifier of the track to process.
% * *|startMeasure|* - the index of the start measure to analyse. Optional.
% * *|endMeasure|* - the index of the end measure to analyse. Optional.
%
% *Returns*
%
% Nothing
%

%% Information
% 
% * *Author*: <mailto:ferro@dei.unipd.it Nicola Ferro>
% * *Version*: 1.00
% * *Since*: 1.00
% * *Requirements*: MATTERS 1.0 or higher; Matlab 2013b or higher
% * *Copyright:* (C) 2015 <http://ims.dei.unipd.it/ Information 
% Management Systems> (IMS) research group, <http://www.dei.unipd.it/ 
% Department of Information Engineering> (DEI), <http://www.unipd.it/ 
% University of Padua>, Italy
% * *License:* <http://www.apache.org/licenses/LICENSE-2.0 Apache License, 
% Version 2.0>

%%
%{
 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at
 
      http://www.apache.org/licenses/LICENSE-2.0
 
 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.
%}

function [] = analyse_measure_pdf(trackID, startMeasure, endMeasure)

    % check the number of input arguments
    narginchk(1, 3);

    % setup common parameters
    common_parameters;
        
    % check that trackID is a non-empty string
    validateattributes(trackID,{'char', 'cell'}, {'nonempty', 'vector'}, '', 'trackID');

     if iscell(trackID)
        % check that trackID is a cell array of strings with one element
        assert(iscellstr(trackID) && numel(trackID) == 1, ...
            'MATTERS:IllegalArgument', 'Expected trackID to be a cell array of strings containing just one string.');
    end

    % remove useless white spaces, if any, and ensure it is a char row
    trackID = char(strtrim(trackID));
    trackID = trackID(:).';
    
     % check that trackID assumes a valid value
    validatestring(trackID, ...
        EXPERIMENT.collection.list, '', 'trackID');
    
    if nargin == 3
        validateattributes(startMeasure, {'numeric'}, ...
            {'nonempty', 'integer', 'scalar', '>=', 1, '<=', EXPERIMENT.measure.number }, '', 'startMeasure');
        
        validateattributes(endMeasure, {'numeric'}, ...
            {'nonempty', 'integer', 'scalar', '>=', startMeasure, '<=', EXPERIMENT.measure.number }, '', 'endMeasure');

    else 
        startMeasure = 1;
        endMeasure = EXPERIMENT.measure.number;
    end
     
   
    % start of overall computations
    startComputation = tic;

    fprintf('\n\n######## Analysing PDF of measures on collection %s (%s) ########\n\n', EXPERIMENT.collection.(trackID).name, EXPERIMENT.label.paper);

    fprintf('+ Settings\n');
    fprintf('  - computed on %s\n', datestr(now, 'yyyy-mm-dd at HH:MM:SS'));
    fprintf('  - slice \n');
    fprintf('    * start measure: %d\n', startMeasure);
    fprintf('    * end measure: %d\n', endMeasure);


    % for each measure
    for m = startMeasure:endMeasure
        
        start = tic;
        
        fprintf('+ Analysing %s\n', EXPERIMENT.measure.getAcronym(m));
                       
        gopID = EXPERIMENT.pattern.identifier.measure(EXPERIMENT.measure.list{m}, trackID);
        origID = EXPERIMENT.pattern.identifier.measure(EXPERIMENT.measure.list{m}, EXPERIMENT.collection.(trackID).original);
        
        serload(EXPERIMENT.pattern.file.measure(trackID, gopID), gopID);
        serload(EXPERIMENT.pattern.file.measure(EXPERIMENT.collection.(trackID).original, origID), origID);
        
        pdfGopID = EXPERIMENT.pattern.identifier.pdf(EXPERIMENT.measure.list{m}, trackID);
        pdfOrigID = EXPERIMENT.pattern.identifier.pdf(EXPERIMENT.measure.list{m}, EXPERIMENT.collection.(trackID).original);
        
        fprintf('  - computing PDF of %s\n', gopID);
        
        evalf(EXPERIMENT.analysis.pdf.kde.compute, ...
            {gopID}, ...
            {pdfGopID});
        
        fprintf('  - computing PDF of %s\n', origID);
        
        evalf(EXPERIMENT.analysis.pdf.kde.compute, ...
            {origID}, ...
            {pdfOrigID});
        
        
        kldGopID = EXPERIMENT.pattern.identifier.kld(EXPERIMENT.measure.list{m}, trackID);
        
        fprintf('  - computing KL divergence between PDFs of %s and %s\n', origID, gopID);
        
        evalf(EXPERIMENT.analysis.pdf.kld.compute, ...
            {pdfOrigID, pdfGopID}, ...
            {kldGopID});
        
        sersave(EXPERIMENT.pattern.file.pdf(trackID, kldGopID), pdfGopID(:), pdfOrigID(:), kldGopID(:));

        
        currentFigure = figure('Visible', 'off');
        
            style = '-';
            evalf(EXPERIMENT.analysis.pdf.kld.plot, ...
                {pdfGopID, 'style'}, ...
                {});
        
            hold on
            
            style = '--';
            evalf(EXPERIMENT.analysis.pdf.kld.plot, ...
                {pdfOrigID, 'style'}, ...
                {});
            
            currentFigure.CurrentAxes.FontSize = 24;
            
            currentFigure.CurrentAxes.XLim = [0 1];
            currentFigure.CurrentAxes.XLabel.String = EXPERIMENT.measure.getAcronym(m);
            %currentFigure.CurrentAxes.XLabel.FontSize = 16;
            
            currentFigure.CurrentAxes.YLabel.String = 'Probability Density Function';
            %currentFigure.CurrentAxes.YLabel.FontSize = 16;
            
            currentFigure.CurrentAxes.Title.String = EXPERIMENT.collection.(EXPERIMENT.collection.(trackID).original).name;
            %currentFigure.CurrentAxes.Title.FontSize = 16;
                        
            l = legend('GoP Runs', 'Original TREC Runs');
            %l.FontSize = 14;
            
            text(0.7, 0.75, sprintf('KLD = %-6.4f', eval(kldGopID)), 'Units', 'Normalized', 'FontSize', 24, 'FontWeight', 'Bold');
            
            currentFigure.PaperPositionMode = 'auto';
            currentFigure.PaperUnits = 'centimeters';
            currentFigure.PaperSize = [29 21];
            currentFigure.PaperPosition = [1 1 27 19];   

            figureID = EXPERIMENT.pattern.identifier.figure.pdf(EXPERIMENT.measure.getID(m), trackID);

            print(currentFigure, '-dpdf', EXPERIMENT.pattern.file.figure(trackID, figureID));

            close(currentFigure)


        % free space
        clear(gopID, origID, pdfGopID, pdfOrigID, kldGopID);
        
        fprintf('  - elapsed time: %s\n', elapsedToHourMinutesSeconds(toc(start)));
        
    end % for measures
               
    fprintf('\n\n######## Total elapsed time for analysing PDF of measures on collection %s (%s): %s ########\n\n', ...
            EXPERIMENT.collection.(trackID).name, EXPERIMENT.label.paper, elapsedToHourMinutesSeconds(toc(startComputation)));

end
