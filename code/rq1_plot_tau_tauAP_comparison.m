%% rq1_plot_tau_tauAP_comparison
% 
% Plots the comparison of the measure pair main effects for both Kendall's
% tau and AP correlation for the Measure Pair/Topic Size/System Size ANOVA.

%% Synopsis
%
%   [] = rq1_plot_tau_tauAP_comparison(trackID)
%  
% *Parameters*
%
% * *|trackID|* - the identifier of the track for which the processing is
% performed.
% * *|correlationID|* - the identifier of correlation to be plotted.
% * *|color|* - the color to be used in the plot. Optional
%
% *Returns*
%
% Nothing.

%% Information
% 
% * *Author*: <mailto:ferro@dei.unipd.it Nicola Ferro>
% * *Version*: 1.00
% * *Since*: 1.00
% * *Requirements*: Matlab 2015b or higher
% * *Copyright:* (C) 2016 <http://ims.dei.unipd.it/ Information 
% Management Systems> (IMS) research group, <http://www.dei.unipd.it/ 
% Department of Information Engineering> (DEI), <http://www.unipd.it/ 
% University of Padua>, Italy
% * *License:* <http://www.apache.org/licenses/LICENSE-2.0 Apache License, 
% Version 2.0>

%%
%{
 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at
 
      http://www.apache.org/licenses/LICENSE-2.0
 
 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.
%}

%%

function [] = rq1_plot_tau_tauAP_comparison(trackID)

    persistent TAG;
    
    if isempty(TAG)
        TAG = 'rq1';
    end;
    

    % check the number of input parameters
    narginchk(1, 1);

    % load common parameters
    common_parameters

    % check that trackID is a non-empty string
    validateattributes(trackID, {'char', 'cell'}, {'nonempty', 'vector'}, '', 'trackID');

     if iscell(trackID)
        % check that trackID is a cell array of strings with one element
        assert(iscellstr(trackID) && numel(trackID) == 1, ...
            'MATTERS:IllegalArgument', 'Expected trackID to be a cell array of strings containing just one string.');
    end

    % remove useless white spaces, if any, and ensure it is a char row
    trackID = char(strtrim(trackID));
    trackID = trackID(:).';
       
    labels = cell(1, EXPERIMENT.correlation.labels.number);
    
    % for each label
    for l = 1:EXPERIMENT.correlation.labels.number
        
        first = EXPERIMENT.measure.(EXPERIMENT.correlation.labels.first{l}).acronym;
        second = EXPERIMENT.measure.(EXPERIMENT.correlation.labels.second{l}).acronym;
        
        labels{l} = sprintf('%s vs %s', first, second);
    end;
    
    xTick = 1:2;

    colors = parula(EXPERIMENT.correlation.labels.number);
    colors = colors(end:-1:1, :);
        
    % start of overall computations
    startComputation = tic;
            
    fprintf('\n\n######## Plotting %s comparison for correlation %s on collection %s (%s) ########\n\n', ...
        TAG, 'tau_tauAP', EXPERIMENT.collection.(trackID).name, EXPERIMENT.label.paper);
    
    fprintf('+ Settings\n');
    fprintf('  - computed on %s\n', datestr(now, 'yyyy-mm-dd at HH:MM:SS'));
    fprintf('  - analysis %s\n', TAG);
    
    anovaMeID = EXPERIMENT.pattern.identifier.anovaMe(TAG, 'tau', trackID);
               
    serload(EXPERIMENT.pattern.file.anova(TAG, 'tau', trackID), {anovaMeID 'me'});
    
    tau_factorA = me.factorA.mean;
    
    [~, idxTau] = sort(tau_factorA, 'ascend');
    
    clear me;
    
    anovaMeID = EXPERIMENT.pattern.identifier.anovaMe(TAG, 'tauAP', trackID);
               
    serload(EXPERIMENT.pattern.file.anova(TAG, 'tauAP', trackID), {anovaMeID 'me'});
    
    tauAP_factorA = me.factorA.mean;
    
    [~, idxTauAP] = sort(tauAP_factorA, 'ascend');
    
    clear me;
          
    % correlation among rankings of evauation measures
    tau = corr(tau_factorA, tauAP_factorA, 'type', 'Kendall');
    tauAP = apCorr(tau_factorA, tauAP_factorA, 'Ties', false);

    
    % rmse between vectors
    v1 = tau_factorA - mean(tau_factorA);
    v2 = tauAP_factorA - mean(tauAP_factorA);
    rmse = rmseCoeff(v1(:), v2(:));
    
    
    yTickLabel = cell(2, EXPERIMENT.correlation.labels.number);
    % for each label
    for l = 1:EXPERIMENT.correlation.labels.number        
        yTickLabel{1, l} = sprintf('%s - %-6.4f', labels{l}, tau_factorA(l));
        yTickLabel{2, l} = sprintf('%-6.4f - %s', tauAP_factorA(l), labels{l});
    end;
    
    [~, idx] = ismember(idxTau, idxTauAP);
    
    data = 1:EXPERIMENT.correlation.labels.number;
    
    data = [data.' idx];

    currentFigure = figure('Visible', 'off');    
    
        ax = axes('Parent', currentFigure);
        ax.TickLabelInterpreter = 'tex';
        ax.FontSize = 42;
        ax.Position = [0.2500 0.1100 0.5000 0.8150];

        ax.XLim = xTick;
        ax.XTick = xTick;
        ax.XTickLabel = {EXPERIMENT.correlation.('tau').symbol.tex, EXPERIMENT.correlation.('tauAP').symbol.tex};

        ax.YAxisLocation = 'left';
        ax.YLim = [1 EXPERIMENT.correlation.labels.number];
        ax.YTick = 1:EXPERIMENT.correlation.labels.number;
        ax.YTickLabel = yTickLabel(1, idxTau);

        ax.YLabel.Interpreter = 'tex';
        ax.YLabel.String = sprintf('RoMP by %s', EXPERIMENT.correlation.('tau').label.tex);


        ax2 = axes('Parent', currentFigure);
        ax2.TickLabelInterpreter = ax.TickLabelInterpreter;
        ax2.FontSize = ax.FontSize;
        ax2.Position = ax.Position;

        ax2.XLim = ax.XLim;
        ax2.XTick = ax.XTick;
        ax2.XTickLabel = ax.XTickLabel;

        ax2.YAxisLocation = 'right';
        ax2.YLim = ax.YLim;
        ax2.YTick = ax.YTick;
        ax2.YTickLabel = yTickLabel(2, idxTauAP);

        ax2.YLabel.Interpreter = ax.YLabel.Interpreter;
        ax2.YLabel.String = sprintf('RoMP by %s', EXPERIMENT.correlation.('tauAP').label.tex);

        hold on;

        % for each label
        for l = 1:EXPERIMENT.correlation.labels.number        
            plot(xTick, data(l, :), '-', 'Color', colors(l, :), ...
                'Marker', 'o', 'MarkerSize', 6, 'LineWidth', 3, ...
                'MarkerFaceColor', colors(l, :), ...
                'MarkerEdgeColor',  colors(l, :));
        end;

        title(sprintf('Correlation among RoMP: %s = %-6.4f; %s = %-6.4f', ...
            'tauCorr', tau, ...
            'apCorr', tauAP), ...
            'FontSize', 42, 'Interpreter', 'tex');
        
        currentFigure.PaperPositionMode = 'auto';
        currentFigure.PaperUnits = 'centimeters';
        currentFigure.PaperSize = [102 52];
        currentFigure.PaperPosition = [1 1 100 50];

        figureID = EXPERIMENT.pattern.identifier.figure.general(TAG, 'tau_tauAP_ranks', trackID);

        print(currentFigure, '-dpdf', EXPERIMENT.pattern.file.figure(TAG, figureID));

        close(currentFigure)
        
    currentFigure = figure('Visible', 'off');
    
        idx = idxTau(end:-1:1);
    
        plot(1:EXPERIMENT.correlation.labels.number, tau_factorA(idx) - mean(tau_factorA(idx)), ...
            'Color', EXPERIMENT.correlation.('tau').color.main, ...
            'LineStyle', '-', 'LineWidth', 3);
        hold on
        plot(1:EXPERIMENT.correlation.labels.number, tauAP_factorA(idx) - mean(tauAP_factorA(idx)), ...
            'Color', EXPERIMENT.correlation.('tauAP').color.main, ...
            'LineStyle', '--', 'LineWidth', 3);
        
        
        ax = gca;
        ax.TickLabelInterpreter = 'tex';
        ax.FontSize = 42;    
        
        ax.XTick = 1:EXPERIMENT.correlation.labels.number;
        ax.XTickLabel = labels(idx);
        ax.XTickLabelRotation = 90;
    
        ax.YLabel.Interpreter = ax.YLabel.Interpreter;
        ax.YLabel.String = sprintf('Correlation Marginal Mean Centered around Zero');
        
        
        title(sprintf('RMSE between %s and %s curves = %-6.4f.', ...
            EXPERIMENT.correlation.('tau').symbol.tex, ...
             EXPERIMENT.correlation.('tauAP').symbol.tex, rmse), ...
            'FontSize', 42, 'Interpreter', 'tex');
                
        legend({EXPERIMENT.correlation.('tau').symbol.tex, EXPERIMENT.correlation.('tauAP').symbol.tex}, ...
            'Location', 'SouthWest', 'Interpreter', 'tex', 'FontSize', 32)
                    
        currentFigure.PaperPositionMode = 'auto';
        currentFigure.PaperUnits = 'centimeters';
        currentFigure.PaperSize = [52 52];
        currentFigure.PaperPosition = [1 1 50 50];
    
        figureID = EXPERIMENT.pattern.identifier.figure.general(TAG, 'tau_tauAP_mp_me', trackID);
    
        print(currentFigure, '-dpdf', EXPERIMENT.pattern.file.figure(TAG, figureID));
    
        close(currentFigure)
    
       
    fprintf('\n\n######## Total elapsed time for plotting %s comparison for correlation %s on collection %s (%s): %s ########\n\n', ...
           TAG, 'tau_tauAP', ...
           EXPERIMENT.collection.(trackID).name, EXPERIMENT.label.paper, elapsedToHourMinutesSeconds(toc(startComputation)));
       
    diary off;
end

