%% rq3_plot_mainEffects
% 
% Plots the main effects for the Measure Pair/Corpus/Topic Set Effects ANOVA.

%% Synopsis
%
%   [] = rq3_plot_mainEffects(correlationID, varargin)
%  
% *Parameters*
%
% * *|correlationID|* - the identifier of correlation to be plotted.
% * *|trackID|* - the identifier of the track for which the processing is
% performed.
%
% *Returns*
%
% Nothing.

%% Information
% 
% * *Author*: <mailto:ferro@dei.unipd.it Nicola Ferro>
% * *Version*: 1.00
% * *Since*: 1.00
% * *Requirements*: Matlab 2015b or higher
% * *Copyright:* (C) 2016 <http://ims.dei.unipd.it/ Information 
% Management Systems> (IMS) research group, <http://www.dei.unipd.it/ 
% Department of Information Engineering> (DEI), <http://www.unipd.it/ 
% University of Padua>, Italy
% * *License:* <http://www.apache.org/licenses/LICENSE-2.0 Apache License, 
% Version 2.0>

%%
%{
 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at
 
      http://www.apache.org/licenses/LICENSE-2.0
 
 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.
%}

%%

function [] = rq3_plot_mainEffects(correlationID, varargin)

    persistent TAG;
    
    if isempty(TAG)
        TAG = 'rq3';
    end;

    % check the number of input parameters
    narginchk(2, inf);

    % load common parameters
    common_parameters

    % check that correlationID is a non-empty string
    validateattributes(correlationID, {'char', 'cell'}, {'nonempty', 'vector'}, '', 'correlationID');
    
    if iscell(correlationID)
        % check that correlationID is a cell array of strings with one element
        assert(iscellstr(correlationID) && numel(correlationID) == 1, ...
            'MATTERS:IllegalArgument', 'Expected correlationID to be a cell array of strings containing just one string.');
    end
    
    % remove useless white spaces, if any, and ensure it is a char row
    correlationID = char(strtrim(correlationID));
    correlationID = correlationID(:).';
    
    % check that correlationID assumes a valid value
    validatestring(correlationID, ...
        EXPERIMENT.correlation.list, '', 'correlationID');
    
    tracks = length(varargin);
    
    for t = 1:tracks
        
        % check that trackID is a non-empty string
        validateattributes(varargin{t}, {'char', 'cell'}, {'nonempty', 'vector'}, '', 'trackID');
        
        if iscell(varargin{t})
            
            % check that trackID is a cell array of strings with one element
            assert(iscellstr(varargin{t}) && numel(varargin{t}) == 1, ...
                'MATTERS:IllegalArgument', 'Expected trackID to be a cell array of strings containing just one string.');
        end
        
        % remove useless white spaces, if any, and ensure it is a char row
        varargin{t} = char(strtrim(varargin{t}));
        varargin{t} = varargin{t}(:).';
                
        % check that trackID assumes a valid value
        validatestring(varargin{t}, ...
            EXPERIMENT.collection.list, '', 'trackID');
    end;
    
    tracks_list = strjoin(varargin, '');
    
    labels = cell(1, EXPERIMENT.correlation.labels.number);
    
     % for each label
    for l = 1:EXPERIMENT.correlation.labels.number
        
        first = EXPERIMENT.measure.(EXPERIMENT.correlation.labels.first{l}).acronym;
        second = EXPERIMENT.measure.(EXPERIMENT.correlation.labels.second{l}).acronym;
        
        labels{l} = sprintf('%s vs %s ', first, second);
    end;
    
    % start of overall computations
    startComputation = tic;
            
    fprintf('\n\n######## Plotting %s main effects for correlation %s on collection(s) %s (%s) ########\n\n', ...
        TAG, correlationID, tracks_list, EXPERIMENT.label.paper);
    
    fprintf('+ Settings\n');
    fprintf('  - computed on %s\n', datestr(now, 'yyyy-mm-dd at HH:MM:SS'));
    fprintf('  - analysis %s\n', TAG);
    fprintf('  - tracks %s\n', tracks_list);
    
    % always load the tau data to set the order 
    anovaMeID = EXPERIMENT.pattern.identifier.anovaMe(TAG, 'tau', tracks_list);
        
    serload(EXPERIMENT.pattern.file.anova(TAG, 'tau', tracks_list), {anovaMeID, 'me'})
    
    % the measure pairs ordered by descending tau correlation
    [~, idxTau] = sort(me.factorA.mean, 'descend');
    
    anovaMeID = EXPERIMENT.pattern.identifier.anovaMe(TAG, correlationID, tracks_list);
        
    serload(EXPERIMENT.pattern.file.anova(TAG, correlationID, tracks_list), {anovaMeID, 'me'})
    
    anovaObsID = EXPERIMENT.pattern.identifier.anovaObs(TAG, correlationID, tracks_list);
    
    serload(EXPERIMENT.pattern.file.anova(TAG, correlationID, tracks_list), {anovaObsID, 'obs'})
    
    yMin = Inf;
    yMax = -Inf;
    
    currentFigure = figure('Visible', 'off');

    subplot(1, 5, [1 2]);
    
        xTick = 1:EXPERIMENT.correlation.labels.number;
        data.mean = me.factorA.mean.';
        data.mean = data.mean(idxTau);
    
        plot(xTick, data.mean, 'Color', EXPERIMENT.correlation.(correlationID).color.main, ...
            'LineStyle', ':',  'LineWidth', 1.5, 'Marker', 's', 'MarkerSize', 10, ...
            'MarkerEdgeColor', EXPERIMENT.correlation.(correlationID).color.main, ...
            'MarkerFaceColor', EXPERIMENT.correlation.(correlationID).color.main);
           
        ax1 = gca;
        ax1.TickLabelInterpreter = 'tex';
        ax1.FontSize = 42;
        
        ax1.XTick = xTick;
        ax1.XTickLabel = labels;
        ax1.XTickLabelRotation = 90;
        
        ax1.Title.Interpreter = 'tex';
        ax1.Title.String = 'Measure Pair';
        
        ax1.YLabel.Interpreter = 'tex';
        ax1.YLabel.String = sprintf('%s Marginal Mean', EXPERIMENT.correlation.(correlationID).label.tex);
               
        yMin = min(yMin, ax1.YLim(1));
        yMax = max(yMax, ax1.YLim(2));
        
        
    subplot(1, 5, 3);
    
        xTick = 1:length(obs.corpora);
        data.mean = me.factorB.mean.';
    
        plot(xTick, data.mean, 'Color', EXPERIMENT.correlation.(correlationID).color.main, ...
            'LineStyle', ':',  'LineWidth', 1.5, 'Marker', 's', 'MarkerSize', 10, ...
            'MarkerEdgeColor', EXPERIMENT.correlation.(correlationID).color.main, ...
            'MarkerFaceColor', EXPERIMENT.correlation.(correlationID).color.main);
            
        ax2 = gca;
        ax2.TickLabelInterpreter = 'tex';
        ax2.FontSize = 42;
        
        ax2.XTick = xTick;
        ax2.XTickLabel = obs.corpora;
        %ax2.XTickLabelRotation = 90;
        
        ax2.Title.Interpreter = 'tex';
        ax2.Title.String = 'Corpus';
        
        ax2.YLabel.Interpreter = 'tex';
        %ax2.YLabel.String = sprintf('%s Marginal Mean', EXPERIMENT.correlation.(correlationID).label.tex);
               
        yMin = min(yMin, ax2.YLim(1));
        yMax = max(yMax, ax2.YLim(2));
        
        
   subplot(1, 5, [4 5]);
    
        xTick = 1:length(obs.topicSets);
        data.mean = me.factorC.mean.';
    
        plot(xTick, data.mean, 'Color', EXPERIMENT.correlation.(correlationID).color.main, ...
            'LineStyle', ':',  'LineWidth', 1.5, 'Marker', 's', 'MarkerSize', 10, ...
            'MarkerEdgeColor', EXPERIMENT.correlation.(correlationID).color.main, ...
            'MarkerFaceColor', EXPERIMENT.correlation.(correlationID).color.main);
            
        ax3 = gca;
        ax3.TickLabelInterpreter = 'tex';
        ax3.FontSize = 42;
        
        ax3.XTick = xTick;
        ax3.XTickLabel = obs.topicSets;
        %ax3.XTickLabelRotation = 90;
        
        ax3.Title.Interpreter = 'tex';
        ax3.Title.String = 'Topic Set';
        
        ax3.YLabel.Interpreter = 'tex';
        %ax3.YLabel.String = sprintf('%s Marginal Mean', EXPERIMENT.correlation.(correlationID).label.tex);
               
        yMin = min(yMin, ax3.YLim(1));
        yMax = max(yMax, ax3.YLim(2));
        
        
    yMin = min(yMin, ax3.YLim(1));
    yMax = max(yMax, ax3.YLim(2));
    
    ax1.YLim = [yMin yMax];
       
    ax2.YLim = [yMin yMax];
    
    ax3.YLim = [yMin yMax];    
        
    currentFigure.PaperPositionMode = 'auto';
    currentFigure.PaperUnits = 'centimeters';
    currentFigure.PaperSize = [132 56];
    currentFigure.PaperPosition = [1 20 130 30];
    
    figureID = EXPERIMENT.pattern.identifier.figure.mainEffects(TAG, correlationID, tracks_list);
    
    print(currentFigure, '-dpdf', EXPERIMENT.pattern.file.figure(TAG, figureID));
    
    close(currentFigure)
    
       
    fprintf('\n\n######## Total elapsed time for plotting %s main effects for correlation %s on collection(s) %s (%s): %s ########\n\n', ...
           TAG, correlationID, tracks_list, ...
           EXPERIMENT.label.paper, elapsedToHourMinutesSeconds(toc(startComputation)));
       
    diary off;
end

