%% rq3_plot_correlation_ranks
% 
% Plots the correlation and ranking of evaluation measure pairs across 
% several tracks for the given correlation coefficient.

%% Synopsis
%
%   [] = rq3_plot_correlation_ranks(correlationID, varargin)
%  
% *Parameters*
%
% * *|correlationID|* - the identifier of correlation to be plotted.
% * *|trackID|* - the identifier of the track for which the processing is
% performed.
%
% *Returns*
%
% Nothing.

%% Information
% 
% * *Author*: <mailto:ferro@dei.unipd.it Nicola Ferro>
% * *Version*: 1.00
% * *Since*: 1.00
% * *Requirements*: Matlab 2015b or higher
% * *Copyright:* (C) 2016 <http://ims.dei.unipd.it/ Information 
% Management Systems> (IMS) research group, <http://www.dei.unipd.it/ 
% Department of Information Engineering> (DEI), <http://www.unipd.it/ 
% University of Padua>, Italy
% * *License:* <http://www.apache.org/licenses/LICENSE-2.0 Apache License, 
% Version 2.0>

%%
%{
 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at
 
      http://www.apache.org/licenses/LICENSE-2.0
 
 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.
%}

%%

function [] = rq3_plot_correlation_ranks(correlationID, varargin)

    persistent TAG TOPICS SYSTEMS REMOVE_1Q;
    
    if isempty(TAG)
        TAG = 'rq3';
        TOPICS = 'tall';
        SYSTEMS = 's0100';
        REMOVE_1Q = false;
    end;
    

    % check the number of input parameters
    narginchk(2, inf);

    % load common parameters
    common_parameters
    
     % check that correlationID is a non-empty string
    validateattributes(correlationID, {'char', 'cell'}, {'nonempty', 'vector'}, '', 'correlationID');
    
    if iscell(correlationID)
        % check that correlationID is a cell array of strings with one element
        assert(iscellstr(correlationID) && numel(correlationID) == 1, ...
            'MATTERS:IllegalArgument', 'Expected correlationID to be a cell array of strings containing just one string.');
    end
    
    % remove useless white spaces, if any, and ensure it is a char row
    correlationID = char(strtrim(correlationID));
    correlationID = correlationID(:).';
    
    % check that correlationID assumes a valid value
    validatestring(correlationID, ...
        EXPERIMENT.correlation.list, '', 'correlationID');

    tracks = length(varargin);
    
    for t = 1:tracks
        
        % check that trackID is a non-empty string
        validateattributes(varargin{t}, {'char', 'cell'}, {'nonempty', 'vector'}, '', 'trackID');
        
        if iscell(varargin{t})
            
            % check that trackID is a cell array of strings with one element
            assert(iscellstr(varargin{t}) && numel(varargin{t}) == 1, ...
                'MATTERS:IllegalArgument', 'Expected trackID to be a cell array of strings containing just one string.');
        end
        
        % remove useless white spaces, if any, and ensure it is a char row
        varargin{t} = char(strtrim(varargin{t}));
        varargin{t} = varargin{t}(:).';
                
        % check that trackID assumes a valid value
        validatestring(varargin{t}, ...
            EXPERIMENT.collection.list, '', 'trackID');
    end;
    
    tracks_list = strjoin(varargin, '');
                
    labels = cell(1, EXPERIMENT.correlation.labels.number);
    
    % for each label
    for l = 1:EXPERIMENT.correlation.labels.number
        
        first = EXPERIMENT.measure.(EXPERIMENT.correlation.labels.first{l}).acronym;
        second = EXPERIMENT.measure.(EXPERIMENT.correlation.labels.second{l}).acronym;
        
        labels{l} = sprintf('%s vs %s', first, second);
    end;
    
    xTick = 1:tracks;

    colors = parula(EXPERIMENT.correlation.labels.number);
    colors = colors(end:-1:1, :);
    
        
    % start of overall computations
    startComputation = tic;
            
    fprintf('\n\n######## Plotting %s the rankings of evaluation measure pairs for correlation %s on collection %s (%s) ########\n\n', ...
        TAG, correlationID, tracks_list, EXPERIMENT.label.paper);
    
    fprintf('+ Settings\n');
    fprintf('  - computed on %s\n', datestr(now, 'yyyy-mm-dd at HH:MM:SS'));
    fprintf('  - analysis %s\n', TAG);
    fprintf('  - tracks %s\n', tracks_list);
    
    xTickLabel = cell(1, tracks);
    
    correlations = NaN(tracks, EXPERIMENT.correlation.labels.number);
    
    idxCorr = NaN(tracks, EXPERIMENT.correlation.labels.number);
    
    % for each track
    for t = 1:tracks
        
        trackID = varargin{t};
        corrID = EXPERIMENT.pattern.identifier.correlation(correlationID, SYSTEMS, TOPICS, REMOVE_1Q, trackID);
        
        serload(EXPERIMENT.pattern.file.correlation(trackID, corrID), {corrID, 'tmp'});
        
        xTickLabel{t} = sprintf('%s', trackID(1:3));
        
        correlations(t, :) = tmp.mean;
        
        [~, idxCorr(t, :)] = sort(correlations(t, :), 'ascend');
        
        clear('tmp');
    end;
                   
    data = 1:EXPERIMENT.correlation.labels.number;
    data = data .';
    
    % for each track
    for t = 2:tracks
        [~, idx] = ismember(idxCorr(t-1, :), idxCorr(t, :));   
        
        data = [data idx.'];
    end;
       
    currentFigure = figure('Visible', 'off');    
    
        ax = axes('Parent', currentFigure);
        ax.TickLabelInterpreter = 'tex';
        ax.FontSize = 42;
        ax.Position = [0.2500 0.1100 0.5000 0.8150];

        ax.XLim = [1 tracks];
        ax.XTick = xTick;
        ax.XTickLabel = xTickLabel;
        
        ax.XLabel.Interpreter = 'tex';
        ax.XLabel.String = 'Collection';

        ax.YAxisLocation = 'left';
        ax.YLim = [1 EXPERIMENT.correlation.labels.number];
        ax.YTick = 1:EXPERIMENT.correlation.labels.number;
        ax.YTickLabel = labels(idxCorr(1, :));

        ax.YLabel.Interpreter = 'tex';
        ax.YLabel.String = sprintf('ROMP by %s', EXPERIMENT.correlation.(correlationID).label.tex);
  
        hold on;

        % for each track
        for t = 1:tracks
            plot([t t], [1 EXPERIMENT.correlation.labels.number], 'k', 'LineWidth', 0.5)
        end;

        firstIdx = idxCorr(1, :);
        
        % for each label
        for l = 1:EXPERIMENT.correlation.labels.number        
            if strcmp(EXPERIMENT.correlation.labels.list{firstIdx(l)}, 'ndcg_twist')
                plot(xTick, data(l, :), '-', ...
                    'Color', colors(l, :), 'LineWidth', 8, ...
                    'Marker', 'o', 'MarkerSize', 9, ...
                    'MarkerFaceColor', colors(l, :), ...
                    'MarkerEdgeColor',  colors(l, :));
            elseif strcmp(EXPERIMENT.correlation.labels.list{firstIdx(l)}, 'ap_ndcg')
                plot(xTick, data(l, :), ':', ...
                    'Color', colors(l, :), 'LineWidth', 8, ...
                    'Marker', 'o', 'MarkerSize', 9, ...
                    'MarkerFaceColor', colors(l, :), ...
                    'MarkerEdgeColor',  colors(l, :));
            else
                plot(xTick, data(l, :), '-', ...
                    'Color', colors(l, :), 'LineWidth', 3, ...
                    'Marker', 'o', 'MarkerSize', 6, ...
                    'MarkerFaceColor', colors(l, :), ...
                    'MarkerEdgeColor',  colors(l, :));
            end;
        end;
        
        currentFigure.PaperPositionMode = 'auto';
        currentFigure.PaperUnits = 'centimeters';
        currentFigure.PaperSize = [102 52];
        currentFigure.PaperPosition = [1 1 100 50];

        figureID = EXPERIMENT.pattern.identifier.figure.general(TAG, [correlationID '_measure_ranks'], tracks_list);
        
        print(currentFigure, '-dpdf', EXPERIMENT.pattern.file.figure(TAG, figureID));

        close(currentFigure)
        

    currentFigure = figure('Visible', 'off');    
    
        firstIdx = idxCorr(1, end:-1:1);
    
        % re-order the correlations by the first track
        correlations = correlations(:, firstIdx); 
    
        hold on;
        % for each label
        for l = 1:EXPERIMENT.correlation.labels.number        
            if strcmp(EXPERIMENT.correlation.labels.list{firstIdx(l)}, 'ndcg_twist')
                plot(1:tracks, correlations(:, l), 'LineStyle', '-', 'Color', colors(end-l+1, :), 'LineWidth', 8);
            elseif strcmp(EXPERIMENT.correlation.labels.list{firstIdx(l)}, 'ap_ndcg')
                plot(1:tracks, correlations(:, l), 'LineStyle', ':', 'Color', colors(end-l+1, :), 'LineWidth', 8);    
            else
                plot(1:tracks, correlations(:, l), 'LineStyle', '-', 'Color', colors(end-l+1, :), 'LineWidth', 3);
            end;
        end;
        
        ax1 = gca;
        
        ax1.FontSize = 42;
        ax1.TickLabelInterpreter = 'tex';
        
        ax1.XLim = [1 tracks];
        ax1.XTick = xTick;
        ax1.XTickLabel = xTickLabel;
        
        ax1.XLabel.Interpreter = 'tex';
        ax1.XLabel.String = 'Collection';
        
        ax1.YLabel.Interpreter = 'tex';
        ax1.YLabel.String = sprintf('%s', EXPERIMENT.correlation.(correlationID).label.tex);
        
        legend(labels(firstIdx), 'Location', 'BestOutside', 'Interpreter', 'tex')
        
        currentFigure.PaperPositionMode = 'auto';
        currentFigure.PaperUnits = 'centimeters';
        currentFigure.PaperSize = [102 52];
        currentFigure.PaperPosition = [1 1 100 50];

        figureID = EXPERIMENT.pattern.identifier.figure.general(TAG, [correlationID '_correlation'], tracks_list);
        
        print(currentFigure, '-dpdf', EXPERIMENT.pattern.file.figure(TAG, figureID));

        close(currentFigure)

       
    fprintf('\n\n######## Total elapsed time for plotting %s main effects for correlation %s on collection %s (%s): %s ########\n\n', ...
           TAG, correlationID, ...
           tracks_list, EXPERIMENT.label.paper, elapsedToHourMinutesSeconds(toc(startComputation)));
       
    diary off;
end

