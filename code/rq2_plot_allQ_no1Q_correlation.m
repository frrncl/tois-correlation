%% rq2_plot_allQ_no1Q_correlation
% 
% Plots the requested correlation between a pair of measures at different
% system and topic samples for allQ and no1Q.

%% Synopsis
%
%   [] = rq2_plot_allQ_no1Q_correlation(trackID, correlationID, pairID)
%  
% *Parameters*
%
% * *|trackID|* - the identifier of the track for which the processing is
% performed.
% * *|correlationID|* - the identifier of correlation to be plotted.
% * *|pairID|* - the identifier of pair of measures whose correlation has 
% to be plotted.
%
% *Returns*
%
% Nothing.

%% Information
% 
% * *Author*: <mailto:ferro@dei.unipd.it Nicola Ferro>
% * *Version*: 1.00
% * *Since*: 1.00
% * *Requirements*: Matlab 2015b or higher
% * *Copyright:* (C) 2016 <http://ims.dei.unipd.it/ Information 
% Management Systems> (IMS) research group, <http://www.dei.unipd.it/ 
% Department of Information Engineering> (DEI), <http://www.unipd.it/ 
% University of Padua>, Italy
% * *License:* <http://www.apache.org/licenses/LICENSE-2.0 Apache License, 
% Version 2.0>

%%
%{
 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at
 
      http://www.apache.org/licenses/LICENSE-2.0
 
 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.
%}

%%

function [] = rq2_plot_allQ_no1Q_correlation(trackID, correlationID, pairID)

    persistent TAG SYSTEMS SYS TOPICS TOPS;
    
    if isempty(TAG)
        TAG = 'rq2';
        
%        SYSTEMS = {'s0010', 's0025', 's0050', 's0075', 's0100', 's0125', ...
%            's0150', 's0200', 's0250', 's0500', 's0750', 's1000', 's1250'};
        SYSTEMS = {'s0010', 's0025', 's0050', 's0075', 's0100', 's0125', ...
            's0150', 's0200', 's0250', 's0500'};
        SYS = length(SYSTEMS);
        
        TOPICS = {'t010', 't030', 't050', 't070'};
        TOPS = length(TOPICS);
    end;

    % check the number of input parameters
    narginchk(3, 3);

    % load common parameters
    common_parameters


    % check that trackID is a non-empty string
    validateattributes(trackID, {'char', 'cell'}, {'nonempty', 'vector'}, '', 'trackID');

     if iscell(trackID)
        % check that trackID is a cell array of strings with one element
        assert(iscellstr(trackID) && numel(trackID) == 1, ...
            'MATTERS:IllegalArgument', 'Expected trackID to be a cell array of strings containing just one string.');
    end

    % remove useless white spaces, if any, and ensure it is a char row
    trackID = char(strtrim(trackID));
    trackID = trackID(:).';
    
    % check that trackID assumes a valid value
    validatestring(trackID, ...
        EXPERIMENT.collection.list, '', 'trackID');
      
    % check that correlationID is a non-empty string
    validateattributes(correlationID, {'char', 'cell'}, {'nonempty', 'vector'}, '', 'correlationID');
    
    if iscell(correlationID)
        % check that correlationID is a cell array of strings with one element
        assert(iscellstr(correlationID) && numel(correlationID) == 1, ...
            'MATTERS:IllegalArgument', 'Expected correlationID to be a cell array of strings containing just one string.');
    end
    
    % remove useless white spaces, if any, and ensure it is a char row
    correlationID = char(strtrim(correlationID));
    correlationID = correlationID(:).';
    
    % check that correlationID assumes a valid value
    validatestring(correlationID, ...
        EXPERIMENT.correlation.list, '', 'correlationID');
    
    % check that pairID is a non-empty string
    validateattributes(pairID, {'char', 'cell'}, {'nonempty', 'vector'}, '', 'pairID');
    
    if iscell(pairID)
        % check that pairID is a cell array of strings with one element
        assert(iscellstr(pairID) && numel(pairID) == 1, ...
            'MATTERS:IllegalArgument', 'Expected pairID to be a cell array of strings containing just one string.');
    end
    
    % remove useless white spaces, if any, and ensure it is a char row
    pairID = char(strtrim(pairID));
    pairID = pairID(:).';
    
    % check that pairID assumes a valid value
    validatestring(pairID, ...
        EXPERIMENT.correlation.labels.list, '', 'pairID');    
    
    % start of overall computations
    startComputation = tic;
    
    % the index of this pair in the vectors
    idx = ismember(EXPERIMENT.correlation.labels.list, pairID);
    
    label = sprintf('%s vs %s', EXPERIMENT.measure.(EXPERIMENT.correlation.labels.first{idx}).acronym, ...
        EXPERIMENT.measure.(EXPERIMENT.correlation.labels.second{idx}).acronym);
    
    fprintf('\n\n######## Plotting correlation %s for measures %s on collection %s (%s) ########\n\n', ...
        correlationID, pairID, EXPERIMENT.collection.(trackID).name, EXPERIMENT.label.paper);
    
    fprintf('+ Settings\n');
    fprintf('  - computed on %s\n', datestr(now, 'yyyy-mm-dd at HH:MM:SS'));
       
    currentFigure = figure('Visible', 'off');
    
    x = NaN(1, SYS);
    
    axesHandles = cell(1, TOPS);
      
    % upper and lower limits for the axes
    axLimits.lower = NaN(1, TOPS);
    axLimits.upper = NaN(1, TOPS);
    
    for t = 1:TOPS
        
        data = NaN(2, SYS);
        ciLow = NaN(2, SYS);
        ciHigh = NaN(2, SYS);
        
        % load the data
        for s = 1:SYS     
            
            % load correlations over all quartiles
            corrID = EXPERIMENT.pattern.identifier.correlation(correlationID, SYSTEMS{s}, TOPICS{t}, false, trackID);
            
            serload(EXPERIMENT.pattern.file.correlation(trackID, corrID), {corrID, 'tmp'});
            
            data(1, s) = tmp.mean(idx);
            ciLow(1, s) = tmp.mean(idx) - tmp.ci(idx);
            ciHigh(1, s) = tmp.mean(idx) + tmp.ci(idx);
            
            x(s) = EXPERIMENT.systems.(SYSTEMS{s}).sample;
                                    

            % load correlations excluding first quartile
            corrID = EXPERIMENT.pattern.identifier.correlation(correlationID, SYSTEMS{s}, TOPICS{t}, true, trackID);
            
            serload(EXPERIMENT.pattern.file.correlation(trackID, corrID), {corrID, 'tmp'});
            
            data(2, s) = tmp.mean(idx);
            ciLow(2, s) = tmp.mean(idx) - tmp.ci(idx);
            ciHigh(2, s) = tmp.mean(idx) + tmp.ci(idx);
                        
        end; % systems
        
        clear('tmp');
        
        subplot(2, 2, t)
            % correlation on all quartiles
            plot(log10(x), data(1, :), 'LineWidth', 2, 'LineStyle', '-', 'Color', EXPERIMENT.correlation.(correlationID).color.main);
            hold on;
            
            a = ciHigh(1, :);
            b = ciLow(1, :);
            
            hFill = fill([log10(x) fliplr(log10(x))],[a fliplr(b)], EXPERIMENT.correlation.(correlationID).color.main, ...
                    'LineStyle', 'none', 'EdgeAlpha', 0.10, 'FaceAlpha', 0.10);

            % send the fill to back
            uistack(hFill, 'bottom');
            
            % Exclude fill from legend
			set(get(get(hFill, 'Annotation'), 'LegendInformation'), 'IconDisplayStyle','off');
            
            % correlation excluding first quartile
            plot(log10(x), data(2, :), 'LineWidth', 2, 'LineStyle', '--', 'Color', EXPERIMENT.correlation.(correlationID).color.no1Q)
            hold on;
            
            a = ciHigh(2, :);
            b = ciLow(2, :);
            
            hFill = fill([log10(x) fliplr(log10(x))],[a fliplr(b)], EXPERIMENT.correlation.(correlationID).color.no1Q, ...
                    'LineStyle', 'none', 'EdgeAlpha', 0.10, 'FaceAlpha', 0.10);

            % send the fill to back
            uistack(hFill, 'bottom');
            
            % Exclude fill from legend
			set(get(get(hFill, 'Annotation'), 'LegendInformation'), 'IconDisplayStyle','off');
            
            ax = gca;
            ax.FontSize = 42;
            ax.TickLabelInterpreter = 'tex';
            ax.XTick = log10(x);            
            ax.XTickLabel = strtrim(cellstr(num2str(x.'))).';
            ax.XTickLabelRotation = 90;
            ax.XLim = [ax.XTick(1) ax.XTick(end)+0.1];
            
            if (t == 3 || t == 4)
                ax.XLabel.Interpreter = 'tex';
                ax.XLabel.String = sprintf('System Size');
            end;
            
            if (t == 1 || t == 3)
                ax.YLabel.Interpreter = 'tex';
                ax.YLabel.String = sprintf('%s %s', label, EXPERIMENT.correlation.(correlationID).label.tex);
            end;
            
            %if (t <= 1)
            %    legend({'allQ', 'no1Q'}, 'Location', 'NorthWest', 'Interpreter', 'tex')
            %else
                legend({sprintf('%s allQ', EXPERIMENT.correlation.(correlationID).symbol.tex), ...
                sprintf('%s no1Q', EXPERIMENT.correlation.(correlationID).symbol.tex)}, ...
                'Location', 'NorthEast', 'Interpreter', 'tex', 'FontSize', 16)
            %end;
                        
            title(sprintf('%3d topics', EXPERIMENT.topics.(TOPICS{t}).sample), 'FontSize', 42, 'Interpreter', 'tex');
        
            axesHandles{t} = ax;
            axLimits.lower(t)= ax.YLim(1);
            axLimits.upper(t)= ax.YLim(2);
    end % topics
    
    yMin = min(axLimits.lower);
    yMax = max(axLimits.upper);
    
    for t = 1:TOPS
        axesHandles{t}.YLim = [0.4 1.0];
    end;
    
    currentFigure.PaperPositionMode = 'auto';
    currentFigure.PaperUnits = 'centimeters';
    currentFigure.PaperSize = [82 62];
    currentFigure.PaperPosition = [1 1 80 60];
    
    figureID = EXPERIMENT.pattern.identifier.figure.general(TAG, [correlationID '_' pairID], trackID);
    
    print(currentFigure, '-dpdf', EXPERIMENT.pattern.file.figure(TAG, figureID));
    
    close(currentFigure)
       
    fprintf('\n\n######## Total elapsed time for plotting correlation %s for measures %s on collection %s (%s): %s ########\n\n', ...
           correlationID, pairID, ...
           EXPERIMENT.collection.(trackID).name, EXPERIMENT.label.paper, elapsedToHourMinutesSeconds(toc(startComputation)));
       
    diary off;
end

