%% rq2_analysis
% 
% Analyses the requested correlation using all quartile against not using
% the first quartile.

%% Synopsis
%
%   [] = rq2_analysis(trackID, correlationID)
%  
% *Parameters*
%
% * *|trackID|* - the identifier of the track for which the processing is
% performed.
% * *|correlationID|* - the identifier of correlation to be plotted.
%
% *Returns*
%
% Nothing.

%% Information
% 
% * *Author*: <mailto:ferro@dei.unipd.it Nicola Ferro>
% * *Version*: 1.00
% * *Since*: 1.00
% * *Requirements*: Matlab 2015b or higher
% * *Copyright:* (C) 2016 <http://ims.dei.unipd.it/ Information 
% Management Systems> (IMS) research group, <http://www.dei.unipd.it/ 
% Department of Information Engineering> (DEI), <http://www.unipd.it/ 
% University of Padua>, Italy
% * *License:* <http://www.apache.org/licenses/LICENSE-2.0 Apache License, 
% Version 2.0>

%%
%{
 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at
 
      http://www.apache.org/licenses/LICENSE-2.0
 
 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.
%}

%%

function [] = rq2_analysis(trackID, correlationID)

    persistent TAG SYSTEMS SYS TOPICS TOPS;
    
    if isempty(TAG)
        TAG = 'rq2';
        
%        SYSTEMS = {'s0010', 's0025', 's0050', 's0075', 's0100', 's0125', ...
%            's0150', 's0200', 's0250', 's0500', 's0750', 's1000', 's1250'};
        SYSTEMS = {'s0010', 's0025', 's0050', 's0075', 's0100', 's0125', ...
            's0150', 's0200', 's0250', 's0500'};
        SYS = length(SYSTEMS);
        
%        TOPICS = {'t010', 't020', 't030', 't040', 't050', ...
%                  't060', 't070', 't080', 't090', 't100', ...
%                  't110', 't120', 't130', 't140', 't148'};
        TOPICS = {'t010', 't020', 't030', 't040', 't050', 't060', 't070'};
        TOPS = length(TOPICS);
    end;


    % check the number of input parameters
    narginchk(2, 2);

    % load common parameters
    common_parameters


    % check that trackID is a non-empty string
    validateattributes(trackID, {'char', 'cell'}, {'nonempty', 'vector'}, '', 'trackID');

     if iscell(trackID)
        % check that trackID is a cell array of strings with one element
        assert(iscellstr(trackID) && numel(trackID) == 1, ...
            'MATTERS:IllegalArgument', 'Expected trackID to be a cell array of strings containing just one string.');
    end

    % remove useless white spaces, if any, and ensure it is a char row
    trackID = char(strtrim(trackID));
    trackID = trackID(:).';
    
    % check that trackID assumes a valid value
    validatestring(trackID, ...
        EXPERIMENT.collection.list, '', 'trackID');
      
    % check that correlationID is a non-empty string
    validateattributes(correlationID, {'char', 'cell'}, {'nonempty', 'vector'}, '', 'correlationID');
    
    if iscell(correlationID)
        % check that correlationID is a cell array of strings with one element
        assert(iscellstr(correlationID) && numel(correlationID) == 1, ...
            'MATTERS:IllegalArgument', 'Expected correlationID to be a cell array of strings containing just one string.');
    end
    
    % remove useless white spaces, if any, and ensure it is a char row
    correlationID = char(strtrim(correlationID));
    correlationID = correlationID(:).';
    
    % check that correlationID assumes a valid value
    validatestring(correlationID, ...
        EXPERIMENT.correlation.list, '', 'correlationID');
    
    % start of overall computations
    startComputation = tic;
    
    fprintf('\n\n######## Comparing correlations %s with/without first quartile on collection %s (%s) ########\n\n', ...
        correlationID, EXPERIMENT.collection.(trackID).name, EXPERIMENT.label.paper);
    
    fprintf('+ Settings\n');
    fprintf('  - computed on %s\n', datestr(now, 'yyyy-mm-dd at HH:MM:SS'));
    
    fprintf('+ Comparing the correlations\n');
    
    % rows are topic sizes, columns are system sizes
    data.tau = NaN(TOPS, SYS);
    data.tauAP = NaN(TOPS, SYS);
    data.rmse = NaN(TOPS, SYS);
    
    for t = 1:TOPS
        
        for s = 1:SYS
            allqCorrID = EXPERIMENT.pattern.identifier.correlation(correlationID, SYSTEMS{s}, TOPICS{t}, false, trackID);
            no1qCorrID = EXPERIMENT.pattern.identifier.correlation(correlationID, SYSTEMS{s}, TOPICS{t}, true, trackID);
            
            serload(EXPERIMENT.pattern.file.correlation(trackID, allqCorrID), {allqCorrID, 'allq'});
            serload(EXPERIMENT.pattern.file.correlation(trackID, no1qCorrID), {no1qCorrID, 'no1q'});
            
                % correlation among rankings of evauation measures
                data.tau(t, s) = corr(allq.mean.', no1q.mean.', 'type', 'Kendall');
                data.tauAP(t, s) = apCorr(allq.mean.', no1q.mean.', 'Ties', false);
                
                % statistical different between distributions
                % rmse between vectors
                v1 = allq.mean - mean(allq.mean);
                v2 = no1q.mean - mean(no1q.mean);
                data.rmse(t, s) = rmseCoeff(v1(:), v2(:));
                
            
            clear allq no1q;
            
        end; % systems
        
    end % topics
     
    rq2ID = EXPERIMENT.pattern.identifier.rq2(correlationID, trackID);
    
    eval(sprintf('%s = data;', rq2ID));
    
    sersave(EXPERIMENT.pattern.file.rq2(rq2ID), ...
        rq2ID(:))
    
   
       
    fprintf('\n\n######## Total elapsed time for correlations %s with/without first quartile on collection %s (%s): %s ########\n\n', ...
           correlationID, ...
           EXPERIMENT.collection.(trackID).name, EXPERIMENT.label.paper, elapsedToHourMinutesSeconds(toc(startComputation)));
       
    diary off;
end

