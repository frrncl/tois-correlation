%% sample_systems
% 
% Samples the requested number of systems from a GoP and saves them 
% to a |.mat| file.
%
%% Synopsis
%
%   [] = sample_systems(varargin)
%  
%
% *Parameters*
%
% * *|systemsID|* - the identifier of the systems samples to be used.
%
% *Returns*
%
% Nothing
%

%% Information
% 
% * *Author*: <mailto:ferro@dei.unipd.it Nicola Ferro>
% * *Version*: 1.00
% * *Since*: 1.00
% * *Requirements*: MATTERS 1.0 or higher; Matlab 2013b or higher
% * *Copyright:* (C) 2015 <http://ims.dei.unipd.it/ Information 
% Management Systems> (IMS) research group, <http://www.dei.unipd.it/ 
% Department of Information Engineering> (DEI), <http://www.unipd.it/ 
% University of Padua>, Italy
% * *License:* <http://www.apache.org/licenses/LICENSE-2.0 Apache License, 
% Version 2.0>

%%
%{
 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at
 
      http://www.apache.org/licenses/LICENSE-2.0
 
 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.
%}

function [] = sample_systems(varargin)
   
    % check the number of input arguments
    narginchk(1, inf);

    % setup common parameters
    common_parameters;
            
    for k = 1:length(varargin)
        
        systemsID = varargin{k};
        
        % check that systemsID is a non-empty string
        validateattributes(systemsID, {'char', 'cell'}, {'nonempty', 'vector'}, '', 'systemsID');
        
        if iscell(systemsID)
            % check that systemsID is a cell array of strings with one element
            assert(iscellstr(systemsID) && numel(systemsID) == 1, ...
                'MATTERS:IllegalArgument', 'Expected systemsID to be a cell array of strings containing just one string.');
        end
        
        % remove useless white spaces, if any, and ensure it is a char row
        systemsID = char(strtrim(systemsID));
        systemsID = systemsID(:).';
        
        % check that systemsID assumes a valid value
        validatestring(systemsID, ...
            EXPERIMENT.systems.list, '', 'systemsID');
                
        samples = cell(EXPERIMENT.correlation.subjects, EXPERIMENT.systems.(systemsID).sample);
        
        for s = 1:EXPERIMENT.correlation.subjects
            samples(s, :) = EXPERIMENT.systems.(systemsID).compute();
        end;
                
        sampleID = EXPERIMENT.pattern.identifier.sample.systems(systemsID, EXPERIMENT.correlation.subjects);
        
        eval(sprintf('%s = samples;', sampleID));
        
        %sersave(EXPERIMENT.pattern.file.sample(sampleID), sampleID(:));
        save(EXPERIMENT.pattern.file.sample(sampleID), sampleID);
        
        clear(sampleID);
        
    end;
    
    
end
