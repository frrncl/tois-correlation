%% rnd_systems
% 
% List the requested number of systems randomly sampled in a GoP.

%% Synopsis
%
%   [list] = rnd_systems(sample)
%  
%% *Parameters*
%
% * *|sample|* - the number of systems to be randomly sampled.
%
%
% *Returns*
%
% * *|list|*  - the list of all the system identifiers in a GoP.

%% Information
% 
% * *Author*: <mailto:ferro@dei.unipd.it Nicola Ferro>
% * *Version*: 1.00
% * *Since*: 1.00
% * *Requirements*: Matlab 2015b or higher
% * *Copyright:* (C) 2016 <http://ims.dei.unipd.it/ Information 
% Management Systems> (IMS) research group, <http://www.dei.unipd.it/ 
% Department of Information Engineering> (DEI), <http://www.unipd.it/ 
% University of Padua>, Italy
% * *License:* <http://www.apache.org/licenses/LICENSE-2.0 Apache License, 
% Version 2.0>

%%
%{
 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at
 
      http://www.apache.org/licenses/LICENSE-2.0
 
 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.
%}

%%

function [list] = rnd_systems(sample)

    % check the number of input parameters
    narginchk(1, 1);
    
    list = all_systems();
    
    l = length(list);
    
    validateattributes(sample, {'numeric'}, ...
            {'nonempty', 'integer', 'scalar', '>=', 1, '<=', l }, '', 'sample');
        
    list = list(randperm(l, sample));
    
end

