%% rq1_analysis
% 
% Computes the Measure Pair/Topic Size/System Size Effects ANOVA.

%% Synopsis
%
%   [] = rq1_analysis(trackID, correlationID)
%  
% *Parameters*
%
% * *|trackID|* - the identifier of the track for which the processing is
% performed.
% * *|correlationID|* - the identifier of correlation to be plotted.
%
%
% *Returns*
%
% Nothing.

%% Information
% 
% * *Author*: <mailto:ferro@dei.unipd.it Nicola Ferro>
% * *Version*: 1.00
% * *Since*: 1.00
% * *Requirements*: Matlab 2015b or higher
% * *Copyright:* (C) 2016 <http://ims.dei.unipd.it/ Information 
% Management Systems> (IMS) research group, <http://www.dei.unipd.it/ 
% Department of Information Engineering> (DEI), <http://www.unipd.it/ 
% University of Padua>, Italy
% * *License:* <http://www.apache.org/licenses/LICENSE-2.0 Apache License, 
% Version 2.0>

%%
%{
 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at
 
      http://www.apache.org/licenses/LICENSE-2.0
 
 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.
%}

%%

function [] = rq1_analysis(trackID, correlationID)

    persistent TAG REMOVE_1Q SYSTEMS SYS TOPICS TOPS;
    
    if isempty(TAG)
        TAG = 'rq1';
        
        REMOVE_1Q = false;
        
%        SYSTEMS = {'s0010', 's0025', 's0050', 's0075', 's0100', 's0125', 's0150', ...
%               's0200', 's0250', 's0500', 's0750', 's1000', 's1250'};
        SYSTEMS = {'s0010', 's0025', 's0050', 's0075', 's0100', 's0125', 's0150', ...
               's0200', 's0250', 's0500'};
        SYS = length(SYSTEMS);        
    
%         TOPICS = {'t010', 't020', 't030', 't040', 't050', ...
%                   't060', 't070', 't080', 't090', 't100', ...
%                   't110', 't120', 't130', 't140', 't148'};
        TOPICS = {'t010', 't020', 't030', 't040', 't050', ...
                  't060', 't070'};

        TOPS = length(TOPICS);    
    end;

    % check the number of input parameters
    narginchk(2, 2);

    % load common parameters
    common_parameters

    % check that trackID is a non-empty string
    validateattributes(trackID, {'char', 'cell'}, {'nonempty', 'vector'}, '', 'trackID');

    if iscell(trackID)
        % check that trackID is a cell array of strings with one element
        assert(iscellstr(trackID) && numel(trackID) == 1, ...
            'MATTERS:IllegalArgument', 'Expected trackID to be a cell array of strings containing just one string.');
    end

    % remove useless white spaces, if any, and ensure it is a char row
    trackID = char(strtrim(trackID));
    trackID = trackID(:).';
    
    % check that trackID assumes a valid value
    validatestring(trackID, ...
        EXPERIMENT.collection.list, '', 'trackID');
      
    % check that correlationID is a non-empty string
    validateattributes(correlationID, {'char', 'cell'}, {'nonempty', 'vector'}, '', 'correlationID');
    
    if iscell(correlationID)
        % check that correlationID is a cell array of strings with one element
        assert(iscellstr(correlationID) && numel(correlationID) == 1, ...
            'MATTERS:IllegalArgument', 'Expected correlationID to be a cell array of strings containing just one string.');
    end
    
    % remove useless white spaces, if any, and ensure it is a char row
    correlationID = char(strtrim(correlationID));
    correlationID = correlationID(:).';
    
    % check that correlationID assumes a valid value
    validatestring(correlationID, ...
        EXPERIMENT.correlation.list, '', 'correlationID');
    
    % start of overall computations
    startComputation = tic;
                  
    fprintf('\n\n######## Performing %s ANOVA analysis for correlation %s on collection %s (%s) ########\n\n', ...
        TAG, correlationID, EXPERIMENT.collection.(trackID).name, EXPERIMENT.label.paper);
    
    fprintf('+ Settings\n');
    fprintf('  - computed on %s\n', datestr(now, 'yyyy-mm-dd at HH:MM:SS'));
    fprintf('  - analysis %s\n', TAG);
    fprintf('  - subjects %d\n', EXPERIMENT.correlation.subjects);
    fprintf('  - remove first quartile %d\n\n', REMOVE_1Q);
    
    fprintf('+ Analysing the correlations\n');
        
    % total number of data points
    N = EXPERIMENT.correlation.labels.number * EXPERIMENT.correlation.subjects * TOPS * SYS;
        
    % the data
    data = NaN(1, N);         
           
    % subjects: for each pair (topic size, system size) there are 
    % EXPERIMENT.correlation.subjects system rankings; for
    % each ranking the correlations between measures pairs are computed
    subject = repmat(cellstr(reshape(num2str(1:EXPERIMENT.correlation.subjects, 's%04d'), 5, EXPERIMENT.correlation.subjects).'), 1, EXPERIMENT.correlation.labels.number).';
    subject = subject(:);
    subject = repmat(subject, TOPS*SYS, 1);    
    
    % factorA: correlation between EXPERIMENT.correlation.labels.number 
    % measures pairs
    factorA = repmat(EXPERIMENT.correlation.labels.list.', 1, EXPERIMENT.correlation.subjects * TOPS * SYS);
            
    % factorB: topic size, i.e. the number of topics sampled for a given
    % ranking
    factorB = cell(1, N);     
    
    % factorC: system size, i.e. the number of system sampled for a given
    % ranking
    factorC = cell(1, N);     

    % the current element in the list
    currentElement = 1;
    
    for t = 1:TOPS                
        for s = 1:SYS            
            corrID = EXPERIMENT.pattern.identifier.correlation(correlationID, SYSTEMS{s}, TOPICS{t}, REMOVE_1Q, trackID);
            
            serload(EXPERIMENT.pattern.file.correlation(trackID, corrID), {corrID, 'tmp'});
            
            range = (currentElement-1)*EXPERIMENT.correlation.subjects*EXPERIMENT.correlation.labels.number+1:currentElement*EXPERIMENT.correlation.subjects*EXPERIMENT.correlation.labels.number;
        
            % copy the correlations in the correct range of the data
            data(range) = reshape(tmp.values(1:EXPERIMENT.correlation.subjects, :).', EXPERIMENT.correlation.subjects*EXPERIMENT.correlation.labels.number, 1);
              
            % set the correct grouping variables
            factorB(range) = TOPICS(t);
            factorC(range) = SYSTEMS(s);
        
            % increment the current element counter
            currentElement = currentElement + 1;
                        
            clear('tmp');
        end; % systems
    end % topics
        
    % compute the model
    evalf(EXPERIMENT.analysis.(TAG).compute, ...
        {'data', 'subject', 'factorA', 'factorB', 'factorC'}, ...
        {'p', 'tbl', 'stats'});    

    df_measure = tbl{3,3};
    ss_measure = tbl{3,2};
    F_measure = tbl{3,6};
    
    df_topic = tbl{4,3};
    ss_topic = tbl{4,2};
    F_topic = tbl{4,6};
    
    df_system = tbl{5,3};
    ss_system = tbl{5,2};
    F_system = tbl{5,6};
    
    df_measure_topic = tbl{6,3};
    ss_measure_topic = tbl{6,2};
    F_measure_topic = tbl{6,6};
    
    df_measure_system = tbl{7,3};
    ss_measure_system = tbl{7,2};
    F_measure_system = tbl{7,6};
    
    df_topic_system = tbl{8,3};
    ss_topic_system = tbl{8,2};
    F_topic_system = tbl{8,6};
    
    ss_error = tbl{9, 2};
    df_error = tbl{9, 3};
    
    ss_total = tbl{10, 2};
    
    % compute the strength of association
    soa.omega2.measure = df_measure * (F_measure - 1) / (df_measure * F_measure + df_topic * F_topic + df_system * F_system + df_measure_topic * F_measure_topic + df_measure_system * F_measure_system + df_topic_system * F_topic_system + df_error + 1);
    soa.omega2.topic = df_topic * (F_topic - 1) / (df_measure * F_measure + df_topic * F_topic + df_system * F_system + df_measure_topic * F_measure_topic + df_measure_system * F_measure_system + df_topic_system * F_topic_system + df_error + 1);
    soa.omega2.system = df_system * (F_system - 1) / (df_measure * F_measure + df_topic * F_topic + df_system * F_system + df_measure_topic * F_measure_topic + df_measure_system * F_measure_system + df_topic_system * F_topic_system + df_error + 1);
    soa.omega2.measure_topic = df_measure_topic * (F_measure_topic - 1) / (df_measure * F_measure + df_topic * F_topic + df_system * F_system + df_measure_topic * F_measure_topic + df_measure_system * F_measure_system + df_topic_system * F_topic_system + df_error + 1);
    soa.omega2.measure_system = df_measure_system * (F_measure_system - 1) / (df_measure * F_measure + df_topic * F_topic + df_system * F_system + df_measure_topic * F_measure_topic + df_measure_system * F_measure_system + df_topic_system * F_topic_system + df_error + 1);
    soa.omega2.topic_system = df_topic_system * (F_topic_system - 1) / (df_measure * F_measure + df_topic * F_topic + df_system * F_system + df_measure_topic * F_measure_topic + df_measure_system * F_measure_system + df_topic_system * F_topic_system + df_error + 1);    
    
    soa.omega2p.measure = df_measure * (F_measure - 1) / (df_measure * (F_measure - 1) + N);
    soa.omega2p.topic = df_topic * (F_topic - 1) / (df_topic * (F_topic - 1) + N);
    soa.omega2p.system =  df_system * (F_system - 1) / (df_system * (F_system - 1) + N);
    soa.omega2p.measure_topic = df_measure_topic * (F_measure_topic - 1) / (df_measure_topic * (F_measure_topic - 1) + N);
    soa.omega2p.measure_system =  df_measure_system * (F_measure_system - 1) / (df_measure_system * (F_measure_system - 1) + N);
    soa.omega2p.topic_system = df_topic_system * (F_topic_system - 1) / (df_topic_system * (F_topic_system - 1) + N);

    soa.eta2.measure = ss_measure / ss_total;
    soa.eta2.topic = ss_topic / ss_total;
    soa.eta2.system = ss_system / ss_total;
    soa.eta2.measure_topic = ss_measure_topic / ss_total;
    soa.eta2.measure_system = ss_measure_system / ss_total;
    soa.eta2.topic_system = ss_topic_system / ss_total;
    
    soa.eta2p.measure = ss_measure / (ss_measure + ss_error);
    soa.eta2p.topic = ss_topic / (ss_topic + ss_error);
    soa.eta2p.system = ss_system / (ss_system + ss_error);
    soa.eta2p.measure_topic = ss_measure_topic / (ss_measure_topic + ss_error);
    soa.eta2p.measure_system = ss_measure_system / (ss_measure_system + ss_error);
    soa.eta2p.topic_system = ss_topic_system / (ss_topic_system + ss_error);
    
    % raw observations
    obs.data = data;
    obs.subject = subject;
    obs.factorA = factorA;
    obs.factorB = factorB;
    obs.factorC = factorC;
    
    % main effects
    [me.factorA.mean, me.factorA.ci] = grpstats(data(:), factorA(:), {'mean', 'meanci'});
    [me.factorB.mean, me.factorB.ci] = grpstats(data(:), factorB(:), {'mean', 'meanci'});
    [me.factorC.mean, me.factorC.ci] = grpstats(data(:), factorC(:), {'mean', 'meanci'});
    
    
    % interaction between topic size (x-axis) and measure pair (y-axis)
    % each row is a measure pair, columns are topic size
    ie.factorBA.mean = grpstats(data(:), {factorB(:),factorA(:)}, {'mean'});
    ie.factorBA.mean = reshape(ie.factorBA.mean, EXPERIMENT.correlation.labels.number, TOPS).';
    
    % interaction between system size (x-axis) and measure pair (y-axis)
    % each row is a measure pair, columns are system size
    ie.factorCA.mean = grpstats(data(:), {factorC(:),factorA(:)}, {'mean'});
    ie.factorCA.mean = reshape(ie.factorCA.mean, EXPERIMENT.correlation.labels.number, SYS).';
        
    % interaction between system size (x-axis) and topic size (y-axis)
    % each row is a topic size, columns are system size
    ie.factorCB.mean = grpstats(data(:), {factorC(:),factorB(:)}, {'mean'});
    ie.factorCB.mean = reshape(ie.factorCB.mean, TOPS, SYS).';
    
    anovaObsID = EXPERIMENT.pattern.identifier.anovaObs(TAG, correlationID, trackID);
    anovaMeID = EXPERIMENT.pattern.identifier.anovaMe(TAG, correlationID, trackID);
    anovaIeID = EXPERIMENT.pattern.identifier.anovaIe(TAG, correlationID, trackID);
    anovaTableID = EXPERIMENT.pattern.identifier.anovaTable(TAG, correlationID, trackID);
    anovaStatsID = EXPERIMENT.pattern.identifier.anovaStats(TAG, correlationID, trackID);
    anovaSoAID = EXPERIMENT.pattern.identifier.anovaSoA(TAG, correlationID, trackID);
    
    eval(sprintf('%s = tbl;', anovaTableID));
    eval(sprintf('%s = stats;', anovaStatsID));
    eval(sprintf('%s = obs;', anovaObsID));
    eval(sprintf('%s = me;', anovaMeID));
    eval(sprintf('%s = ie;', anovaIeID));
    eval(sprintf('%s = soa;', anovaSoAID));
    
    sersave(EXPERIMENT.pattern.file.anova(TAG, correlationID, trackID), ...
        anovaTableID(:), anovaStatsID(:), anovaSoAID(:), anovaObsID(:), ...
        anovaMeID(:), anovaIeID(:))
    
       
    fprintf('\n\n######## Total elapsed time for performing %s ANOVA analysis for correlation %s on collection %s (%s): %s ########\n\n', ...
           TAG, correlationID, ...
           EXPERIMENT.collection.(trackID).name, EXPERIMENT.label.paper, elapsedToHourMinutesSeconds(toc(startComputation)));
       
    diary off;
end

