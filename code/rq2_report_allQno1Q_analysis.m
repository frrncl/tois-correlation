%% rq2_report_allQno1Q_analysis
% 
% Reports the all quartive/no first quartile correlation analyses for the  
% given tracke and saves them to a |.tex| file.
%
%% Synopsis
%
%   [] = rq2_report_allQno1Q_analysis(trackID, correlationID)
%  
%
% *Parameters*
%
% * *|trackID|* - the identifier of the track for which the processing is
% performed.
% * *|correlationID|* - the identifier of correlation to be plotted.

%
% *Returns*
%
% Nothing
%

%% Information
% 
% * *Author*: <mailto:ferro@dei.unipd.it Nicola Ferro>
% * *Version*: 1.00
% * *Since*: 1.00
% * *Requirements*: MATTERS 1.0 or higher; Matlab 2013b or higher
% * *Copyright:* (C) 2015 <http://ims.dei.unipd.it/ Information 
% Management Systems> (IMS) research group, <http://www.dei.unipd.it/ 
% Department of Information Engineering> (DEI), <http://www.unipd.it/ 
% University of Padua>, Italy
% * *License:* <http://www.apache.org/licenses/LICENSE-2.0 Apache License, 
% Version 2.0>

%%
%{
 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at
 
      http://www.apache.org/licenses/LICENSE-2.0
 
 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.
%}

function [] = rq2_report_allQno1Q_analysis(trackID, correlationID)

    persistent TAG SYSTEMS SYS TOPICS TOPS;
    
    if isempty(TAG)
        TAG = 'rq2';
        
%        SYSTEMS = {'10', '25', '50', '75', '100', '125', ...
%            '150', '200', '250', '500', '750', '1000', '1250'};
        SYSTEMS = {'10', '25', '50', '75', '100', '125', ...
            '150', '200', '250', '500'};
        SYS = length(SYSTEMS);
        
%        TOPICS = {'10', '20', '30', '40', '50', '60', '70', '80', '90', ...
%                  '100', '110', '120', '130', '140', '148'};
        TOPICS = {'10', '20', '30', '40', '50', '60', '70'};
        TOPS = length(TOPICS);
    end;

    % check the number of input arguments
    narginchk(2, 2);

    % setup common parameters
    common_parameters;
        
    
    % check that trackID is a non-empty string
    validateattributes(trackID, {'char', 'cell'}, {'nonempty', 'vector'}, '', 'trackID');
    
    if iscell(trackID)
        
        % check that trackID is a cell array of strings with one element
        assert(iscellstr(trackID) && numel(trackID) == 1, ...
            'MATTERS:IllegalArgument', 'Expected trackID to be a cell array of strings containing just one string.');
    end
    
    % remove useless white spaces, if any, and ensure it is a char row
    trackID = char(strtrim(trackID));
    trackID = trackID(:).';
    
    % check that trackID assumes a valid value
    validatestring(trackID, ...
        EXPERIMENT.collection.list, '', 'trackID');
    
    if iscell(correlationID)
        % check that correlationID is a cell array of strings with one element
        assert(iscellstr(correlationID) && numel(correlationID) == 1, ...
            'MATTERS:IllegalArgument', 'Expected correlationID to be a cell array of strings containing just one string.');
    end
    
    % remove useless white spaces, if any, and ensure it is a char row
    correlationID = char(strtrim(correlationID));
    correlationID = correlationID(:).';
    
    % check that correlationID assumes a valid value
    validatestring(correlationID, ...
        EXPERIMENT.correlation.list, '', 'correlationID');
    
    % start of overall computations
    startComputation = tic;

    fprintf('\n\n######## Report allQ/no1Q analysis of %s correlation on collection %s (%s) ########\n\n', ...
        correlationID, EXPERIMENT.collection.(trackID).name, EXPERIMENT.label.paper);

    fprintf('+ Settings\n');
    fprintf('  - computed on %s\n', datestr(now, 'yyyy-mm-dd at HH:MM:SS'));


    fprintf('+ Loading data\n');
    
        
    rq2ID = EXPERIMENT.pattern.identifier.rq2(correlationID, trackID);
        
    serload(EXPERIMENT.pattern.file.rq2(rq2ID), {rq2ID, 'data'})
        
    fprintf('+ Printing the report\n');
    
    % the file where the report has to be written
    fid = fopen(EXPERIMENT.pattern.file.report(TAG, sprintf('%s_allQno1Q_%s', correlationID, trackID)), 'w');


    fprintf(fid, '\\documentclass[11pt]{article} \n\n');

    fprintf(fid, '\\usepackage{amsmath}\n');
    fprintf(fid, '\\usepackage{multirow}\n');
    fprintf(fid, '\\usepackage{colortbl}\n');
    fprintf(fid, '\\usepackage{lscape}\n');
    fprintf(fid, '\\usepackage{pdflscape}\n');
    fprintf(fid, '\\usepackage{rotating}\n');
    fprintf(fid, '\\usepackage[a3paper]{geometry}\n\n');
    
    fprintf(fid, '\\usepackage{xcolor}\n');
    fprintf(fid, '\\definecolor{lightgrey}{RGB}{219, 219, 219}\n');
    fprintf(fid, '\\definecolor{verylightblue}{RGB}{204, 229, 255}\n');
    fprintf(fid, '\\definecolor{lightblue}{RGB}{124, 216, 255}\n');
    fprintf(fid, '\\definecolor{blue}{RGB}{32, 187, 253}\n');
    
    fprintf(fid, '\\begin{document}\n\n');
    
    fprintf(fid, '\\title{Summary Report on AllQ/no1Q Analyses}\n\n');
    
    fprintf(fid, '\\maketitle\n\n');
    
    fprintf(fid, 'Tracks:\n');
    fprintf(fid, '\\begin{itemize}\n');
    
    
    fprintf(fid, '\\item %s\n', EXPERIMENT.collection.(trackID).name);
    
    fprintf(fid, '\\end{itemize}\n');
    
    
    %fprintf(fid, '\\begin{landscape}  \n');
    fprintf(fid, '\\begin{table}[p] \n');
    % fprintf(fid, '\\tiny \n');
    fprintf(fid, '\\centering \n');
    %fprintf(fid, '\\hspace*{-6.5em} \n');
    
    fprintf(fid, '\\begin{tabular}{|l|ll|*{%d}{r|}} \n', SYS);
    
    fprintf(fid, '\\cline{4-%d} \n', SYS + 3);
    
    fprintf(fid, '\\multicolumn{3}{c|}{} & \\multicolumn{%d}{c|}{\\textbf{System Size}} \\\\ ', SYS);
    
    fprintf(fid, '\\cline{4-%d} \n', SYS + 3);
    
    fprintf(fid, '\\multicolumn{3}{c|}{} ');
    for s = 1:SYS
        fprintf(fid, '& \\multicolumn{1}{c|}{\\textbf{%s}} ', SYSTEMS{s});       
    end; % systems
    fprintf(fid, '\\\\ \n');
    
    fprintf(fid, '\\hline \n');
    
    fprintf(fid, '\\multirow{%d}{0.25em}{\\rotatebox[origin=c]{90}{\\textbf{Topic Size}}} ', 3*TOPS);	
    
    for t = 1:TOPS    
        
        fprintf(fid, '& \\multirow{3}{0.10em}{\\textbf{%s}} \\hspace{1ex} ', TOPICS{t});	
        
        fprintf(fid, '& $\\mathrm{tauCorr}$ ');	
        for s = 1:SYS            
            fprintf(fid, ' & %.4f ', data.tau(t, s));            
        end;
        fprintf(fid, '\\\\ \n');
        
        fprintf(fid, '& \\hspace{1ex} & $\\mathrm{apCorr}$ ');	
        for s = 1:SYS            
            fprintf(fid, ' & %.4f ', data.tauAP(t, s));            
        end;
        fprintf(fid, '\\\\ \n');
        
        fprintf(fid, '& \\hspace{1ex} & $\\mathrm{RMSE}$ ');	
        for s = 1:SYS            
            fprintf(fid, ' & %.4f ', data.rmse(t, s));             
        end;
        fprintf(fid, '\\\\ \n');
        
        fprintf(fid, '\\cline{2-%d} \n', SYS + 3);
    end;
       
    fprintf(fid, '\\hline \n');
    
    fprintf(fid, '\\end{tabular} \n');
    
    fprintf(fid, '\\caption{allQ/no1Q analysis.}\n');
    
    fprintf(fid, '\\label{tab:allQ-no1Q} \n');
    
    fprintf(fid, '\\end{table} \n\n');
    
    %fprintf(fid, '\\end{landscape}  \n');
    
    fprintf(fid, '\\end{document} \n\n');
    
    
    fclose(fid);
    
    
               
    fprintf('\n\n######## Total elapsed time for reporting allQ/no1Q analysis on correlation %s on collection (%s): %s ########\n\n', ...
            correlationID, EXPERIMENT.label.paper, elapsedToHourMinutesSeconds(toc(startComputation)));

    diary off;
end
