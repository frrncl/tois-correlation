%% compute_correlation
% 
% Computes correlation among measures for the given track and saves them 
% to a |.mat| file.
%
%% Synopsis
%
%   [] = compute_correlation(trackID, systemsID, topicsID, remove1Q, varargin)
%  
%
% *Parameters*
%
% * *|trackID|* - the identifier of the track to process.
% * *|systemsID|* - the identifier of the systems sample to be used.
% * *|topicsID|* - the identifier of the topics sample to be used.
% * *|remove1Q|* - a boolean indicating whether the systems in the first
% quartile of AP performances have to be removed or not.
% * *|correlationID|* - a list of identifiers of correlations to be
% computed.
%
% *Returns*
%
% Nothing
%

%% Information
% 
% * *Author*: <mailto:ferro@dei.unipd.it Nicola Ferro>
% * *Version*: 1.00
% * *Since*: 1.00
% * *Requirements*: MATTERS 1.0 or higher; Matlab 2013b or higher
% * *Copyright:* (C) 2015 <http://ims.dei.unipd.it/ Information 
% Management Systems> (IMS) research group, <http://www.dei.unipd.it/ 
% Department of Information Engineering> (DEI), <http://www.unipd.it/ 
% University of Padua>, Italy
% * *License:* <http://www.apache.org/licenses/LICENSE-2.0 Apache License, 
% Version 2.0>

%%
%{
 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at
 
      http://www.apache.org/licenses/LICENSE-2.0
 
 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.
%}

function [] = compute_correlation(trackID, systemsID, topicsID, remove1Q, varargin)
   
    % check the number of input arguments
    narginchk(5, inf);

    % setup common parameters
    common_parameters;
        
    % check that trackID is a non-empty string
    validateattributes(trackID, {'char', 'cell'}, {'nonempty', 'vector'}, '', 'trackID');

     if iscell(trackID)
        % check that trackID is a cell array of strings with one element
        assert(iscellstr(trackID) && numel(trackID) == 1, ...
            'MATTERS:IllegalArgument', 'Expected trackID to be a cell array of strings containing just one string.');
    end

    % remove useless white spaces, if any, and ensure it is a char row
    trackID = char(strtrim(trackID));
    trackID = trackID(:).';
    
    % check that trackID assumes a valid value
    validatestring(trackID, ...
        EXPERIMENT.collection.list, '', 'trackID');
    
    for k = 1:length(varargin)

        correlationID = varargin{k};
        
        % check that correlationID is a non-empty string
        validateattributes(correlationID, {'char', 'cell'}, {'nonempty', 'vector'}, '', 'correlationID');

         if iscell(correlationID)
            % check that correlationID is a cell array of strings with one element
            assert(iscellstr(correlationID) && numel(correlationID) == 1, ...
                'MATTERS:IllegalArgument', 'Expected correlationID to be a cell array of strings containing just one string.');
        end

        % remove useless white spaces, if any, and ensure it is a char row
        correlationID = char(strtrim(correlationID));
        correlationID = correlationID(:).';
    
        % check that correlationID assumes a valid value
        validatestring(correlationID, ...
            EXPERIMENT.correlation.list, '', 'correlationID');
        
        varargin{k} = correlationID;
    end;
    
    % check that systemsID is a non-empty string
    validateattributes(systemsID, {'char', 'cell'}, {'nonempty', 'vector'}, '', 'systemsID');
    
    if iscell(systemsID)
        % check that systemsID is a cell array of strings with one element
        assert(iscellstr(systemsID) && numel(systemsID) == 1, ...
            'MATTERS:IllegalArgument', 'Expected systemsID to be a cell array of strings containing just one string.');
    end
    
    % remove useless white spaces, if any, and ensure it is a char row
    systemsID = char(strtrim(systemsID));
    systemsID = systemsID(:).';
    
    % check that systemsID assumes a valid value
    validatestring(systemsID, ...
        EXPERIMENT.systems.list, '', 'systemsID');

    % check that topicsID is a non-empty string
    validateattributes(topicsID, {'char', 'cell'}, {'nonempty', 'vector'}, '', 'topicsID');
    
    if iscell(topicsID)
        % check that topicsID is a cell array of strings with one element
        assert(iscellstr(topicsID) && numel(topicsID) == 1, ...
            'MATTERS:IllegalArgument', 'Expected topicsID to be a cell array of strings containing just one string.');
    end
    
    % remove useless white spaces, if any, and ensure it is a char row
    topicsID = char(strtrim(topicsID));
    topicsID = topicsID(:).';
    
    % check that topicsID assumes a valid value
    validatestring(topicsID, ...
        EXPERIMENT.topics.list, '', 'topicsID');   
    
    % check that remove1Q is a non-empty scalar logical value
    validateattributes(remove1Q, {'logical'}, {'nonempty','scalar'}, '', 'remove1Q');
    
    % start of overall computations
    startComputation = tic;

    fprintf('\n\n######## Computing correlation(s) for systems %s on collection %s (%s) ########\n\n', ...
        systemsID, EXPERIMENT.collection.(trackID).name, EXPERIMENT.label.paper);

    fprintf('+ Settings\n');
    fprintf('  - computed on %s\n', datestr(now, 'yyyy-mm-dd at HH:MM:SS'));
    fprintf('  - systems %s\n', systemsID);
    fprintf('  - topics %s\n', topicsID);
    fprintf('  - subjects %d\n', EXPERIMENT.correlation.subjects);
    fprintf('  - remove first quartile %d\n\n', remove1Q);
    
    % indexes of the upper triangle
    idx = logical(tril(ones(EXPERIMENT.measure.number), -1));
    
    fprintf('+ Loading measures\n');
        
    measures = cell(1, EXPERIMENT.measure.number);
    
    % for each measure
    for m = 1:EXPERIMENT.measure.number
        
        fprintf('+ loading %s\n', EXPERIMENT.measure.getAcronym(m));
        
        measureID = EXPERIMENT.pattern.identifier.measure(EXPERIMENT.measure.list{m}, trackID);
        
        % load the measure in the tmp variable
        serload(EXPERIMENT.pattern.file.measure(trackID, measureID), {measureID, 'measure'});
        
        measures{m} = measure;
        
        clear('measure');
        
    end;
    
    fprintf('+\nComputing system rankings\n');
        
    % load the systems sample
    systemSampleID = EXPERIMENT.pattern.identifier.sample.systems(systemsID, EXPERIMENT.correlation.subjects);
    load(EXPERIMENT.pattern.file.sample(systemSampleID), systemSampleID);
    
    % load the topics sample
    topicSampleID = EXPERIMENT.pattern.identifier.sample.topics(topicsID, EXPERIMENT.correlation.subjects, trackID);
    load(EXPERIMENT.pattern.file.sample(topicSampleID), topicSampleID);
    
    % check the pre-conditions assumed below (they simplify the logic of
    % the for)
    assert(strcmp(EXPERIMENT.measure.getID(1), 'ap'), 'AP must be the first measure in the list');

    % each element is a subject, i.e. a set of systems ranked according to
    % different measures.
    % Within each subject rows are (a sample of) systems and columns are
    % measures
    data = cell(1, EXPERIMENT.correlation.subjects);

    
    % for each subject
    for s = 1:EXPERIMENT.correlation.subjects
        
        eval(sprintf('systems = %s(%d, :);', systemSampleID, s));
        eval(sprintf('topics = %s(%d, :);', topicSampleID, s));
        
        % for each measure
        for m = 1:EXPERIMENT.measure.number
            
            % if we have to remove the 1st quartile, select it in terms of
            % AP, which is assumed to be the first measure
            if remove1Q && m == 1
                
                tmp = nanmean(measures{m}{topics, systems}).';
                
                % remove the systems under the 1st quartile
                systems(tmp <= prctile(tmp, 25)) = [];
                
                clear tmp;
            end;
            
            % on the first iteration, initialize the subject with the proper
            % number of systems
            % rows are (a sample of) systems and columns are measures
            if m==1
                subject = NaN(length(systems), EXPERIMENT.measure.number);
            end;
            
            subject(:, m) = nanmean(measures{m}{topics, systems}).';
                        
        end; % for measures
        
        data{s} = subject;
        
        % free space
        clear('systems', 'topics', 'subject');
    end; % subjects
    
    for k = 1:length(varargin)
        
        start = tic;
        
        correlationID = varargin{k};
        
        fprintf('+ Computing the %s correlation\n', correlationID);
        
        % identifier of the computed correlation
        correlation.id = correlationID;
        
        % rows are subjects, columns are correlation values for each pair
        % of measures
        correlation.values = NaN(EXPERIMENT.correlation.subjects, EXPERIMENT.correlation.labels.number);
        
        for s = 1:EXPERIMENT.correlation.subjects
            
            tmp = EXPERIMENT.correlation.(correlationID).compute(data{s});
            
            correlation.values(s, :) = tmp(idx).';
                        
        end;
        
        correlation.mean = mean(correlation.values);
        correlation.ci = confidenceIntervalDelta(correlation.values, EXPERIMENT.analysis.alpha);
        
        corrID = EXPERIMENT.pattern.identifier.correlation(correlationID, systemsID, topicsID, remove1Q, trackID);
        
        eval(sprintf('%s = correlation;', corrID));
        
        sersave(EXPERIMENT.pattern.file.correlation(trackID, corrID), corrID(:));
        
        clear('correlation', corrID);
                    
        fprintf('  - elapsed time %s\n', elapsedToHourMinutesSeconds(toc(start)));
        
    end;
       
    fprintf('\n\n######## Total elapsed time for computing correlation(s) for systems %s on collection %s (%s): %s ########\n\n', ...
            systemsID, ...
            EXPERIMENT.collection.(trackID).name, EXPERIMENT.label.paper, elapsedToHourMinutesSeconds(toc(startComputation)));
end
