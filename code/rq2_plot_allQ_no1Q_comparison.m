%% rq2_allQ_no1Q_comparison
% 
% Plots the comparison of the allQ and no1Q correlation for the given coefficient.

%% Synopsis
%
%   [] = rq2_plot_allQ_no1Q_comparison(trackID, correlationID, systemsID, topicsID)
%  
% *Parameters*
%
% * *|trackID|* - the identifier of the track for which the processing is
% performed.
% * *|correlationID|* - the identifier of correlation to be plotted.
% * *|systemsID|* - the identifier of the systems sample to be used.
% * *|topicsID|* - the identifier of the topics sample to be used.
%
% *Returns*
%
% Nothing.

%% Information
% 
% * *Author*: <mailto:ferro@dei.unipd.it Nicola Ferro>
% * *Version*: 1.00
% * *Since*: 1.00
% * *Requirements*: Matlab 2015b or higher
% * *Copyright:* (C) 2016 <http://ims.dei.unipd.it/ Information 
% Management Systems> (IMS) research group, <http://www.dei.unipd.it/ 
% Department of Information Engineering> (DEI), <http://www.unipd.it/ 
% University of Padua>, Italy
% * *License:* <http://www.apache.org/licenses/LICENSE-2.0 Apache License, 
% Version 2.0>

%%
%{
 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at
 
      http://www.apache.org/licenses/LICENSE-2.0
 
 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.
%}

%%

function [] = rq2_plot_allQ_no1Q_comparison(trackID, correlationID, systemsID, topicsID)

    persistent TAG;
    
    if isempty(TAG)
        TAG = 'rq2';
    end;
    
    % check the number of input parameters
    narginchk(4, 4);

    % load common parameters
    common_parameters

    % check that trackID is a non-empty string
    validateattributes(trackID, {'char', 'cell'}, {'nonempty', 'vector'}, '', 'trackID');

     if iscell(trackID)
        % check that trackID is a cell array of strings with one element
        assert(iscellstr(trackID) && numel(trackID) == 1, ...
            'MATTERS:IllegalArgument', 'Expected trackID to be a cell array of strings containing just one string.');
    end

    % remove useless white spaces, if any, and ensure it is a char row
    trackID = char(strtrim(trackID));
    trackID = trackID(:).';
    
     % check that correlationID is a non-empty string
    validateattributes(correlationID, {'char', 'cell'}, {'nonempty', 'vector'}, '', 'correlationID');
    
    if iscell(correlationID)
        % check that correlationID is a cell array of strings with one element
        assert(iscellstr(correlationID) && numel(correlationID) == 1, ...
            'MATTERS:IllegalArgument', 'Expected correlationID to be a cell array of strings containing just one string.');
    end
    
    % remove useless white spaces, if any, and ensure it is a char row
    correlationID = char(strtrim(correlationID));
    correlationID = correlationID(:).';
    
    % check that correlationID assumes a valid value
    validatestring(correlationID, ...
        EXPERIMENT.correlation.list, '', 'correlationID');
    
    
    % check that systemsID is a non-empty string
    validateattributes(systemsID, {'char', 'cell'}, {'nonempty', 'vector'}, '', 'systemsID');
    
    if iscell(systemsID)
        % check that systemsID is a cell array of strings with one element
        assert(iscellstr(systemsID) && numel(systemsID) == 1, ...
            'MATTERS:IllegalArgument', 'Expected systemsID to be a cell array of strings containing just one string.');
    end
    
    % remove useless white spaces, if any, and ensure it is a char row
    systemsID = char(strtrim(systemsID));
    systemsID = systemsID(:).';
    
    % check that systemsID assumes a valid value
    validatestring(systemsID, ...
        EXPERIMENT.systems.list, '', 'systemsID');

    % check that topicsID is a non-empty string
    validateattributes(topicsID, {'char', 'cell'}, {'nonempty', 'vector'}, '', 'topicsID');
    
    if iscell(topicsID)
        % check that topicsID is a cell array of strings with one element
        assert(iscellstr(topicsID) && numel(topicsID) == 1, ...
            'MATTERS:IllegalArgument', 'Expected topicsID to be a cell array of strings containing just one string.');
    end
    
    % remove useless white spaces, if any, and ensure it is a char row
    topicsID = char(strtrim(topicsID));
    topicsID = topicsID(:).';
    
    % check that topicsID assumes a valid value
    validatestring(topicsID, ...
        EXPERIMENT.topics.list, '', 'topicsID');   
    
    labels = cell(1, EXPERIMENT.correlation.labels.number);
    
    % for each label
    for l = 1:EXPERIMENT.correlation.labels.number
        
        first = EXPERIMENT.measure.(EXPERIMENT.correlation.labels.first{l}).acronym;
        second = EXPERIMENT.measure.(EXPERIMENT.correlation.labels.second{l}).acronym;
        
        labels{l} = sprintf('%s vs %s', first, second);
    end;
    
    xTick = 1:2;

    colors = parula(EXPERIMENT.correlation.labels.number);
    colors = colors(end:-1:1, :);
        
    % start of overall computations
    startComputation = tic;
            
    fprintf('\n\n######## Plotting %s comparison for correlation %s on collection %s (%s) ########\n\n', ...
        TAG, correlationID, EXPERIMENT.collection.(trackID).name, EXPERIMENT.label.paper);
    
    fprintf('+ Settings\n');
    fprintf('  - computed on %s\n', datestr(now, 'yyyy-mm-dd at HH:MM:SS'));
    fprintf('  - analysis %s\n', TAG);
    
           
    % load correlations over all quartiles
    corrID = EXPERIMENT.pattern.identifier.correlation(correlationID, systemsID, topicsID, false, trackID);
            
    serload(EXPERIMENT.pattern.file.correlation(trackID, corrID), {corrID, 'allQ'});
    
    [~, idxAllQ] = sort(allQ.mean, 'ascend');
    
    % load correlations without first quartile
    corrID = EXPERIMENT.pattern.identifier.correlation(correlationID, systemsID, topicsID, true, trackID);
            
    serload(EXPERIMENT.pattern.file.correlation(trackID, corrID), {corrID, 'no1Q'});
    
    [~, idxNo1Q] = sort(no1Q.mean, 'ascend');
    
    % correlation among rankings of evauation measures
    tau = corr(allQ.mean.', no1Q.mean.', 'type', 'Kendall');
    tauAP = apCorr(allQ.mean.', no1Q.mean.', 'Ties', false);

    
    % rmse between vectors
    v1 = allQ.mean - mean(allQ.mean);
    v2 =  no1Q.mean - mean(no1Q.mean);
    rmse = rmseCoeff(v1(:), v2(:));
    
    yTickLabel = cell(2, EXPERIMENT.correlation.labels.number);
    % for each label
    for l = 1:EXPERIMENT.correlation.labels.number        
        yTickLabel{1, l} = sprintf('%s - %-6.4f', labels{l}, allQ.mean(l));
        yTickLabel{2, l} = sprintf('%-6.4f - %s', no1Q.mean(l), labels{l});
    end;
    
    [~, idx] = ismember(idxAllQ, idxNo1Q);
    
    data = 1:EXPERIMENT.correlation.labels.number;
    
    data = [data.' idx.'];

    currentFigure = figure('Visible', 'off');    
    
        ax = axes('Parent', currentFigure);
        ax.TickLabelInterpreter = 'tex';
        ax.FontSize = 42;
        ax.Position = [0.2500 0.1100 0.5000 0.8150];

        ax.XLim = xTick;
        ax.XTick = xTick;
        ax.XTickLabel = {sprintf('%s allQ', EXPERIMENT.correlation.(correlationID).symbol.tex), ...
                         sprintf('%s no1Q', EXPERIMENT.correlation.(correlationID).symbol.tex)};

        ax.YAxisLocation = 'left';
        ax.YLim = [1 EXPERIMENT.correlation.labels.number];
        ax.YTick = 1:EXPERIMENT.correlation.labels.number;
        ax.YTickLabel = yTickLabel(1, idxAllQ);

        ax.YLabel.Interpreter = 'tex';
        ax.YLabel.String = sprintf('RoMP by %s over allQ', EXPERIMENT.correlation.(correlationID).label.tex);
        

        ax2 = axes('Parent', currentFigure);

        ax2.TickLabelInterpreter =  ax.TickLabelInterpreter;
        ax2.FontSize = ax.FontSize;
        ax2.Position = ax.Position;

        ax2.XLim = ax.XLim;
        ax2.XTick = ax.XTick;
        ax2.XTickLabel = ax.XTickLabel;

        ax2.YAxisLocation = 'right';
        ax2.YLim = ax.YLim;
        ax2.YTick = ax.YTick;
        ax2.YTickLabel = yTickLabel(2, idxNo1Q);

        ax2.YLabel.Interpreter = ax.YLabel.Interpreter;
        ax2.YLabel.String = sprintf('RoMP by %s over no1Q', EXPERIMENT.correlation.(correlationID).label.tex);
        
        hold on;

        % for each label
        for l = 1:EXPERIMENT.correlation.labels.number        
            plot(xTick, data(l, :), '-', 'Color', colors(l, :), ...
                'Marker', 'o', 'MarkerSize', 6, 'LineWidth', 3, ...
                'MarkerFaceColor', colors(l, :), ...
                'MarkerEdgeColor',  colors(l, :));
        end;

        title(sprintf('Correlation among RoMP: %s = %-6.4f; %s = %-6.4f', ...
            'tauCorr', tau, ...
            'apCorr', tauAP), ...
            'FontSize', 42, 'Interpreter', 'tex');
        
        
        currentFigure.PaperPositionMode = 'auto';
        currentFigure.PaperUnits = 'centimeters';
        currentFigure.PaperSize = [102 52];
        currentFigure.PaperPosition = [1 1 100 50];

        figureID = EXPERIMENT.pattern.identifier.figure.general(TAG, [correlationID '_' systemsID '_' topicsID], trackID);
        
        print(currentFigure, '-dpdf', EXPERIMENT.pattern.file.figure(TAG, figureID));

        close(currentFigure)
        
    currentFigure = figure('Visible', 'off');
    
        idx = idxAllQ(end:-1:1);
    
        x = 1:EXPERIMENT.correlation.labels.number;
            
        % correlation on all quartiles
        plot(x, allQ.mean(idx) - mean(allQ.mean), ...
            'Color', EXPERIMENT.correlation.(correlationID).color.main, ...
            'LineStyle', '-', 'LineWidth', 3);
        hold on;
                
        % correlation excluding first quartile
        plot(x, no1Q.mean(idx) - mean(no1Q.mean), ...
            'Color', EXPERIMENT.correlation.(correlationID).color.no1Q, ...
            'LineStyle', '--', 'LineWidth', 3);
        
        ax = gca;
        ax.TickLabelInterpreter = 'tex';
        ax.FontSize = 42;    
        
        ax.XTick = 1:EXPERIMENT.correlation.labels.number;
        ax.XTickLabel = labels;
        ax.XTickLabelRotation = 90;

        ax.YLabel.Interpreter = 'tex';
        ax.YLabel.String = sprintf('%s Centered around Zero', EXPERIMENT.correlation.(correlationID).label.tex);        
        
        title(sprintf('RMSE between %s and %s curves = %-6.4f.', ...
            'allQ', 'no1Q', rmse), ...
            'FontSize', 42, 'Interpreter', 'tex');
        
        legend({sprintf('%s allQ', EXPERIMENT.correlation.(correlationID).symbol.tex), ...
                sprintf('%s no1Q', EXPERIMENT.correlation.(correlationID).symbol.tex)}, ...
                'Location', 'SouthWest', 'Interpreter', 'tex', 'FontSize', 42)
                    
        currentFigure.PaperPositionMode = 'auto';
        currentFigure.PaperUnits = 'centimeters';
        currentFigure.PaperSize = [52 52];
        currentFigure.PaperPosition = [1 1 50 50];
    
        figureID = EXPERIMENT.pattern.identifier.figure.general(TAG, [correlationID '_allQno1Q_' systemsID '_' topicsID], trackID);
        
        print(currentFigure, '-dpdf', EXPERIMENT.pattern.file.figure(TAG, figureID));
    
        close(currentFigure)
    
       
    fprintf('\n\n######## Total elapsed time for %s comparison for correlation %s on collection %s (%s): %s ########\n\n', ...
           TAG, correlationID, ...
           EXPERIMENT.collection.(trackID).name, EXPERIMENT.label.paper, elapsedToHourMinutesSeconds(toc(startComputation)));
       
    diary off;
end

