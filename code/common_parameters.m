%% common_parameters
% 
% Sets up parameters common to the different scripts.
%
%% Information
% 
% * *Author*: <mailto:ferro@dei.unipd.it Nicola Ferro>
% * *Version*: 1.00
% * *Since*: 1.00
% * *Requirements*: MATTERS 1.0 or higher; Matlab 2015b or higher
% * *Copyright:* (C) 2016 <http://ims.dei.unipd.it/ Information 
% Management Systems> (IMS) research group, <http://www.dei.unipd.it/ 
% Department of Information Engineering> (DEI), <http://www.unipd.it/ 
% University of Padua>, Italy
% * *License:* <http://www.apache.org/licenses/LICENSE-2.0 Apache License, 
% Version 2.0>

%%
%{
 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at
 
      http://www.apache.org/licenses/LICENSE-2.0
 
 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.
%}

diary off;

%% Path Configuration

% if we are running on the cluster 
if (strcmpi(computer, 'GLNXA64'))
    addpath('/nas1/promise/ims/ferro/matters/base/')
    addpath('/nas1/promise/ims/ferro/matters/base/core/analysis/')
    addpath('/nas1/promise/ims/ferro/matters/base/core/io/')
    addpath('/nas1/promise/ims/ferro/matters/base/core/measure/')
    addpath('/nas1/promise/ims/ferro/matters/base/core/plot/')
    addpath('/nas1/promise/ims/ferro/matters/base/core/util/')
end;

% The base path
if (strcmpi(computer, 'GLNXA64')) % if we are running on the cluster    
    EXPERIMENT.path.base = '/nas1/promise/ims/ferro/TOIS2016-F/experiment/';
else % if we are running on the local machine
    EXPERIMENT.path.base = '/Users/ferro/Documents/pubblicazioni/2016/TOIS2016-F/experiment/';
end;

% The path for the measures, i.e. the GoP
EXPERIMENT.path.measure = sprintf('%1$s%2$s%3$s', EXPERIMENT.path.base, 'measure', filesep);

% The path for samples of systems and topics
EXPERIMENT.path.samples = sprintf('%1$s%2$s%3$s', EXPERIMENT.path.base, 'samples', filesep);

% The path for correlations
EXPERIMENT.path.correlation = sprintf('%1$s%2$s%3$s', EXPERIMENT.path.base, 'correlation', filesep);

% The path for analyses
EXPERIMENT.path.analysis = sprintf('%1$s%2$s%3$s', EXPERIMENT.path.base, 'analysis', filesep);

% The path for figures
EXPERIMENT.path.figure = sprintf('%1$s%2$s%3$s', EXPERIMENT.path.base, 'figure', filesep);

% The path for PDF analyses
EXPERIMENT.path.pdf = sprintf('%1$s%2$s%3$s', EXPERIMENT.path.analysis, 'pdf', filesep);

% The path for reports
EXPERIMENT.path.report = sprintf('%1$s%2$s%3$s', EXPERIMENT.path.base, 'report', filesep);

%% General Configuration

% Label of the paper this experiment is for
EXPERIMENT.label.paper = 'TOIS 2016 F';


%% Overall Experiment Taxonomy
EXPERIMENT.taxonomy.component.list = {'stop', 'lug', 'model', 'ltr', 'query'};
EXPERIMENT.taxonomy.component.number = length(EXPERIMENT.taxonomy.component.list);

EXPERIMENT.taxonomy.stop.id = {'nostop', 'indri', 'lucene', 'smart', 'snowball', 'terrier'};
EXPERIMENT.taxonomy.stop.name = 'Stop Lists';
EXPERIMENT.taxonomy.stop.number = length(EXPERIMENT.taxonomy.stop.id);

EXPERIMENT.taxonomy.lug.list = {'stem', 'grams'};
EXPERIMENT.taxonomy.lug.number = length(EXPERIMENT.taxonomy.lug.list);

EXPERIMENT.taxonomy.lugall.id = {'nolug', 'krovetz', 'lovins', 'porter', 'snowballPorter', 'weakPorter', ...
    '4grams', '5grams', '6grams', '7grams', '8grams', '9grams', '10grams'};
EXPERIMENT.taxonomy.lugall.name = 'Stemmers and N-grams';
EXPERIMENT.taxonomy.lugall.number = length(EXPERIMENT.taxonomy.lugall.id);

EXPERIMENT.taxonomy.grams.id = {'nolug', '4grams', '5grams', '6grams', '7grams', '8grams', '9grams', '10grams'};
EXPERIMENT.taxonomy.grams.name = 'N-grams';
EXPERIMENT.taxonomy.grams.number = length(EXPERIMENT.taxonomy.grams.id);

EXPERIMENT.taxonomy.stem.id = {'nolug', 'krovetz', 'lovins', 'porter', 'snowballPorter', 'weakPorter'};
EXPERIMENT.taxonomy.stem.name = 'Stemmers';
EXPERIMENT.taxonomy.stem.number = length(EXPERIMENT.taxonomy.stem.id);

EXPERIMENT.taxonomy.model.id = {'bb2', 'bm25', 'dfiz', 'dfree', ...
    'dirichletlm', 'dlh', 'dph', 'hiemstralm', 'ifb2', 'inb2', 'inl2', ...
    'inexpb2', 'jskls', 'lemurtfidf', 'lgd',  'pl2', 'tfidf'};
EXPERIMENT.taxonomy.model.name = 'IR Models';
EXPERIMENT.taxonomy.model.number = length(EXPERIMENT.taxonomy.model.id);


%% Configuration for Collections

EXPERIMENT.collection.list = {'T07gop', 'T08gop', 'T09gop', 'T10gop', ...
                              'T13gop', 'T14gop', 'T15gop', ...
                              'T131415gop'};
EXPERIMENT.collection.number = length(EXPERIMENT.collection.list);

% Collection TREC 07, 1998, Ad-hoc
EXPERIMENT.collection.T07.id = 'T07';
EXPERIMENT.collection.T07.name =  'TREC 07, 1998, Ad-hoc';

if (strcmpi(computer, 'GLNXA64')) % if we are running on the cluster     
    EXPERIMENT.collection.T07.path = '';
else % if we a running on the local machine
    EXPERIMENT.collection.T07.path = '/Users/ferro/Documents/experimental-collections/TREC/TREC_07_1998_AdHoc/';
end;

EXPERIMENT.collection.T07.pool.file = sprintf('%1$s%2$s%3$s%4$s', EXPERIMENT.collection.T07.path, 'pool', filesep, 'qrels.adhoc.disk7.txt');
EXPERIMENT.collection.T07.pool.relevanceDegrees = {'NotRelevant', 'Relevant'};
EXPERIMENT.collection.T07.pool.relevanceGrades = 0:1;
EXPERIMENT.collection.T07.pool.delimiter = 'space';
EXPERIMENT.collection.T07.pool.import = @(fileName, id) importPoolFromFileTRECFormat('FileName', fileName, 'Identifier', id, 'RelevanceDegrees', EXPERIMENT.collection.T07.pool.relevanceDegrees, 'RelevanceGrades', EXPERIMENT.collection.T07.pool.relevanceGrades, 'Delimiter', EXPERIMENT.collection.T07.pool.delimiter,  'Verbose', false);

EXPERIMENT.collection.T07.runSet.path = sprintf('%1$s%2$s%3$s%4$s%3$s', EXPERIMENT.collection.T07.path, 'runs', filesep, 'all');
EXPERIMENT.collection.T07.runSet.documentOrdering = 'TrecEvalLexDesc';
EXPERIMENT.collection.T07.runSet.singlePrecision = true;
EXPERIMENT.collection.T07.runSet.delimiter = 'tab';
EXPERIMENT.collection.T07.runSet.import = @(runPath, id, requiredTopics) importRunsFromDirectoryTRECFormat('Path', runPath, 'Identifier', id, 'RequiredTopics', requiredTopics, 'DocumentOrdering', EXPERIMENT.collection.T07.runSet.documentOrdering, 'SinglePrecision', EXPERIMENT.collection.T07.runSet.singlePrecision, 'Delimiter', EXPERIMENT.collection.T07.runSet.delimiter, 'Verbose', false);

EXPERIMENT.collection.T07.topicSet.id = '351-400';
EXPERIMENT.collection.T07.topicSet.number = 50;
EXPERIMENT.collection.T07.corpus.id = 'TIPSTER';

% Collection TREC 08, 1999, Ad-hoc
EXPERIMENT.collection.T08.id = 'T08';
EXPERIMENT.collection.T08.name =  'TREC 08, 1998, Ad-hoc';

if (strcmpi(computer, 'GLNXA64')) % if we are running on the cluster     
    EXPERIMENT.collection.T08.path = '';
else % if we a running on the local machine
    EXPERIMENT.collection.T08.path = '/Users/ferro/Documents/experimental-collections/TREC/TREC_08_1999_AdHoc/';
end;

EXPERIMENT.collection.T08.pool.file = sprintf('%1$s%2$s%3$s%4$s', EXPERIMENT.collection.T08.path, 'pool', filesep, 'qrels.trec8.adhoc.txt');
EXPERIMENT.collection.T08.pool.relevanceDegrees = {'NotRelevant', 'Relevant'};
EXPERIMENT.collection.T08.pool.relevanceGrades = 0:1;
EXPERIMENT.collection.T08.pool.delimiter = 'space';
EXPERIMENT.collection.T08.pool.import = @(fileName, id) importPoolFromFileTRECFormat('FileName', fileName, 'Identifier', id, 'RelevanceDegrees', EXPERIMENT.collection.T08.pool.relevanceDegrees, 'RelevanceGrades', EXPERIMENT.collection.T08.pool.relevanceGrades, 'Delimiter', EXPERIMENT.collection.T08.pool.delimiter,  'Verbose', false);

EXPERIMENT.collection.T08.runSet.path = sprintf('%1$s%2$s%3$s%4$s%3$s', EXPERIMENT.collection.T08.path, 'runs', filesep, 'all');
EXPERIMENT.collection.T08.runSet.documentOrdering = 'TrecEvalLexDesc';
EXPERIMENT.collection.T08.runSet.singlePrecision = true;
EXPERIMENT.collection.T08.runSet.delimiter = 'tab';
EXPERIMENT.collection.T08.runSet.import = @(runPath, id, requiredTopics) importRunsFromDirectoryTRECFormat('Path', runPath, 'Identifier', id, 'RequiredTopics', requiredTopics, 'DocumentOrdering', EXPERIMENT.collection.T08.runSet.documentOrdering, 'SinglePrecision', EXPERIMENT.collection.T08.runSet.singlePrecision, 'Delimiter', EXPERIMENT.collection.T08.runSet.delimiter, 'Verbose', false);

EXPERIMENT.collection.T08.topicSet.id = '401-450';
EXPERIMENT.collection.T08.topicSet.number = 50;
EXPERIMENT.collection.T08.corpus.id = 'TIPSTER';


% Collection TREC 09, 2000, Web
EXPERIMENT.collection.T09.id = 'T09';
EXPERIMENT.collection.T09.name =  'TREC 09, 2000, Web';

if (strcmpi(computer, 'GLNXA64')) % if we are running on the cluster     
    EXPERIMENT.collection.T09.path = '';
else % if we a running on the local machine
    EXPERIMENT.collection.T09.path = '/Users/ferro/Documents/experimental-collections/TREC/TREC_09_2000_Web/';
end;

EXPERIMENT.collection.T09.pool.file = sprintf('%1$s%2$s%3$s%4$s', EXPERIMENT.collection.T09.path, 'pool', filesep, 'qrels.trec9.main_web.txt');
EXPERIMENT.collection.T09.pool.relevanceDegrees = {'NotRelevant', 'Relevant', 'HighlyRelevant'};
EXPERIMENT.collection.T09.pool.relevanceGrades = 0:2;
EXPERIMENT.collection.T09.pool.delimiter = 'space';
EXPERIMENT.collection.T09.pool.import = @(fileName, id) importPoolFromFileTRECFormat('FileName', fileName, 'Identifier', id, 'RelevanceDegrees', EXPERIMENT.collection.T09.pool.relevanceDegrees, 'RelevanceGrades', EXPERIMENT.collection.T09.pool.relevanceGrades, 'Delimiter', EXPERIMENT.collection.T09.pool.delimiter,  'Verbose', false);

EXPERIMENT.collection.T09.runSet.path = sprintf('%1$s%2$s%3$s%4$s%3$s', EXPERIMENT.collection.T09.path, 'runs', filesep, 'all');
EXPERIMENT.collection.T09.runSet.documentOrdering = 'TrecEvalLexDesc';
EXPERIMENT.collection.T09.runSet.singlePrecision = true;
EXPERIMENT.collection.T09.runSet.delimiter = 'tab';
EXPERIMENT.collection.T09.runSet.import = @(runPath, id, requiredTopics) importRunsFromDirectoryTRECFormat('Path', runPath, 'Identifier', id, 'RequiredTopics', requiredTopics, 'DocumentOrdering', EXPERIMENT.collection.T09.runSet.documentOrdering, 'SinglePrecision', EXPERIMENT.collection.T09.runSet.singlePrecision, 'Delimiter', EXPERIMENT.collection.T09.runSet.delimiter, 'Verbose', false);

EXPERIMENT.collection.T09.topicSet.id = '451-500';
EXPERIMENT.collection.T09.topicSet.number = 50;
EXPERIMENT.collection.T09.corpus.id = 'WT10g';


% Collection TREC 10, 2001, Web
EXPERIMENT.collection.T10.id = 'T10';
EXPERIMENT.collection.T10.name =  'TREC 10, 2001, Web';

if (strcmpi(computer, 'GLNXA64')) % if we are running on the cluster     
    EXPERIMENT.collection.T10.path = '';
else % if we a running on the local machine
    EXPERIMENT.collection.T10.path = '/Users/ferro/Documents/experimental-collections/TREC/TREC_10_2001_Web/';
end;

EXPERIMENT.collection.T10.pool.file = sprintf('%1$s%2$s%3$s%4$s', EXPERIMENT.collection.T10.path, 'pool', filesep, 'adhoc_trec2001_qrels.txt');
EXPERIMENT.collection.T10.pool.relevanceDegrees = {'NotRelevant', 'Relevant', 'HighlyRelevant'};
EXPERIMENT.collection.T10.pool.relevanceGrades = 0:2;
EXPERIMENT.collection.T10.pool.delimiter = 'space';
EXPERIMENT.collection.T10.pool.import = @(fileName, id) importPoolFromFileTRECFormat('FileName', fileName, 'Identifier', id, 'RelevanceDegrees', EXPERIMENT.collection.T10.pool.relevanceDegrees, 'RelevanceGrades', EXPERIMENT.collection.T10.pool.relevanceGrades, 'Delimiter', EXPERIMENT.collection.T10.pool.delimiter,  'Verbose', false);

EXPERIMENT.collection.T10.runSet.path = sprintf('%1$s%2$s%3$s%4$s%3$s', EXPERIMENT.collection.T10.path, 'runs', filesep, 'all');
EXPERIMENT.collection.T10.runSet.documentOrdering = 'TrecEvalLexDesc';
EXPERIMENT.collection.T10.runSet.singlePrecision = true;
EXPERIMENT.collection.T10.runSet.delimiter = 'tab';
EXPERIMENT.collection.T10.runSet.import = @(runPath, id, requiredTopics) importRunsFromDirectoryTRECFormat('Path', runPath, 'Identifier', id, 'RequiredTopics', requiredTopics, 'DocumentOrdering', EXPERIMENT.collection.T10.runSet.documentOrdering, 'SinglePrecision', EXPERIMENT.collection.T10.runSet.singlePrecision, 'Delimiter', EXPERIMENT.collection.T10.runSet.delimiter, 'Verbose', false);

EXPERIMENT.collection.T10.topicSet.id = '501-550';
EXPERIMENT.collection.T10.topicSet.number = 50;
EXPERIMENT.collection.T10.corpus.id = 'WT10g';


% Collection TREC 13, 2004, Terabyte
EXPERIMENT.collection.T13.id = 'T13';
EXPERIMENT.collection.T13.name =  'TREC 13, 2004, Terabyte';

if (strcmpi(computer, 'GLNXA64')) % if we are running on the cluster     
    EXPERIMENT.collection.T13.path = '';
else % if we a running on the local machine
    EXPERIMENT.collection.T13.path = '/Users/ferro/Documents/experimental-collections/TREC/TREC_13_2004_Terabyte/';
end;

EXPERIMENT.collection.T13.pool.file = sprintf('%1$s%2$s%3$s%4$s', EXPERIMENT.collection.T13.path, 'pool', filesep, 'qrels.2004.Terabyte.txt');
EXPERIMENT.collection.T13.pool.relevanceDegrees = {'NotRelevant', 'Relevant', 'HighlyRelevant'};
EXPERIMENT.collection.T13.pool.relevanceGrades = 0:2;
EXPERIMENT.collection.T13.pool.delimiter = 'space';
EXPERIMENT.collection.T13.pool.import = @(fileName, id) importPoolFromFileTRECFormat('FileName', fileName, 'Identifier', id, 'RelevanceDegrees', EXPERIMENT.collection.T13.pool.relevanceDegrees, 'RelevanceGrades', EXPERIMENT.collection.T13.pool.relevanceGrades, 'Delimiter', EXPERIMENT.collection.T13.pool.delimiter,  'Verbose', false);

EXPERIMENT.collection.T13.runSet.path = sprintf('%1$s%2$s%3$s', EXPERIMENT.collection.T13.path, 'runs', filesep);
EXPERIMENT.collection.T13.runSet.documentOrdering = 'TrecEvalLexDesc';
EXPERIMENT.collection.T13.runSet.singlePrecision = true;
EXPERIMENT.collection.T13.runSet.delimiter = 'tab';
EXPERIMENT.collection.T13.runSet.import = @(runPath, id, requiredTopics) importRunsFromDirectoryTRECFormat('Path', runPath, 'Identifier', id, 'RequiredTopics', requiredTopics, 'DocumentOrdering', EXPERIMENT.collection.T13.runSet.documentOrdering, 'SinglePrecision', EXPERIMENT.collection.T13.runSet.singlePrecision, 'Delimiter', EXPERIMENT.collection.T13.runSet.delimiter, 'Verbose', false);

EXPERIMENT.collection.T13.topicSet.id = '701-750';
EXPERIMENT.collection.T13.topicSet.number = 49;
EXPERIMENT.collection.T13.corpus.id = 'GOV2';


% Collection TREC 14, 2005, Terabyte
EXPERIMENT.collection.T14.id = 'T14';
EXPERIMENT.collection.T14.name =  'TREC 14, 2005, Terabyte';

if (strcmpi(computer, 'GLNXA64')) % if we are running on the cluster     
    EXPERIMENT.collection.T14.path = '';
else % if we a running on the local machine
    EXPERIMENT.collection.T14.path = '/Users/ferro/Documents/experimental-collections/TREC/TREC_14_2005_Terabyte/';
end;

EXPERIMENT.collection.T14.pool.file = sprintf('%1$s%2$s%3$s%4$s', EXPERIMENT.collection.T14.path, 'pool', filesep, 'terabyte.2005.adhoc_qrels.txt');
EXPERIMENT.collection.T14.pool.relevanceDegrees = {'NotRelevant', 'Relevant', 'HighlyRelevant'};
EXPERIMENT.collection.T14.pool.relevanceGrades = 0:2;
EXPERIMENT.collection.T14.pool.delimiter = 'space';
EXPERIMENT.collection.T14.pool.import = @(fileName, id) importPoolFromFileTRECFormat('FileName', fileName, 'Identifier', id, 'RelevanceDegrees', EXPERIMENT.collection.T14.pool.relevanceDegrees, 'RelevanceGrades', EXPERIMENT.collection.T14.pool.relevanceGrades, 'Delimiter', EXPERIMENT.collection.T14.pool.delimiter,  'Verbose', false);

EXPERIMENT.collection.T14.runSet.path = sprintf('%1$s%2$s%3$s', EXPERIMENT.collection.T14.path, 'runs', filesep);
EXPERIMENT.collection.T14.runSet.documentOrdering = 'TrecEvalLexDesc';
EXPERIMENT.collection.T14.runSet.singlePrecision = true;
EXPERIMENT.collection.T14.runSet.delimiter = 'tab';
EXPERIMENT.collection.T14.runSet.import = @(runPath, id, requiredTopics) importRunsFromDirectoryTRECFormat('Path', runPath, 'Identifier', id, 'RequiredTopics', requiredTopics, 'DocumentOrdering', EXPERIMENT.collection.T14.runSet.documentOrdering, 'SinglePrecision', EXPERIMENT.collection.T14.runSet.singlePrecision, 'Delimiter', EXPERIMENT.collection.T14.runSet.delimiter, 'Verbose', false);

EXPERIMENT.collection.T14.topicSet.id = '751-800';
EXPERIMENT.collection.T14.topicSet.number = 50;
EXPERIMENT.collection.T14.corpus.id = 'GOV2';


% Collection TREC 15, 2006, Terabyte
EXPERIMENT.collection.T15.id = 'T15';
EXPERIMENT.collection.T15.name =  'TREC 15, 2006, Terabyte';

if (strcmpi(computer, 'GLNXA64')) % if we are running on the cluster     
    EXPERIMENT.collection.T15.path = '';
else % if we a running on the local machine
    EXPERIMENT.collection.T15.path = '/Users/ferro/Documents/experimental-collections/TREC/TREC_15_2006_Terabyte/';
end;

EXPERIMENT.collection.T15.pool.file = sprintf('%1$s%2$s%3$s%4$s', EXPERIMENT.collection.T15.path, 'pool', filesep, 'qrels.tb06.top50.txt');
EXPERIMENT.collection.T15.pool.relevanceDegrees = {'NotRelevant', 'Relevant', 'HighlyRelevant'};
EXPERIMENT.collection.T15.pool.relevanceGrades = 0:2;
EXPERIMENT.collection.T15.pool.delimiter = 'space';
EXPERIMENT.collection.T15.pool.import = @(fileName, id) importPoolFromFileTRECFormat('FileName', fileName, 'Identifier', id, 'RelevanceDegrees', EXPERIMENT.collection.T15.pool.relevanceDegrees, 'RelevanceGrades', EXPERIMENT.collection.T15.pool.relevanceGrades, 'Delimiter', EXPERIMENT.collection.T15.pool.delimiter,  'Verbose', false);

EXPERIMENT.collection.T15.runSet.path = sprintf('%1$s%2$s%3$s', EXPERIMENT.collection.T15.path, 'runs', filesep);
EXPERIMENT.collection.T15.runSet.documentOrdering = 'TrecEvalLexDesc';
EXPERIMENT.collection.T15.runSet.singlePrecision = true;
EXPERIMENT.collection.T15.runSet.delimiter = 'tab';
EXPERIMENT.collection.T15.runSet.import = @(runPath, id, requiredTopics) importRunsFromDirectoryTRECFormat('Path', runPath, 'Identifier', id, 'RequiredTopics', requiredTopics, 'DocumentOrdering', EXPERIMENT.collection.T15.runSet.documentOrdering, 'SinglePrecision', EXPERIMENT.collection.T15.runSet.singlePrecision, 'Delimiter', EXPERIMENT.collection.T15.runSet.delimiter, 'Verbose', false);

EXPERIMENT.collection.T15.topicSet.id = '801-850';
EXPERIMENT.collection.T15.topicSet.number = 50;
EXPERIMENT.collection.T15.corpus.id = 'GOV2';


% TREC 07, 1998, Adhoc, Grid-of-Points
EXPERIMENT.collection.T07gop.id = 'T07gop';
EXPERIMENT.collection.T07gop.name =  'TREC 07, 1998, Adhoc, GoP';
EXPERIMENT.collection.T07gop.topicSet.id = '351-400';
EXPERIMENT.collection.T07gop.topicSet.number = 50;
EXPERIMENT.collection.T07gop.corpus.id = 'TIPSTER';
EXPERIMENT.collection.T07gop.original = 'T07';

% TREC 08, 1999, Adhoc, Grid-of-Points
EXPERIMENT.collection.T08gop.id = 'T08gop';
EXPERIMENT.collection.T08gop.name =  'TREC 08, 1999, Adhoc, GoP';
EXPERIMENT.collection.T08gop.topicSet.id = '401-450';
EXPERIMENT.collection.T08gop.topicSet.number = 50;
EXPERIMENT.collection.T08gop.corpus.id = 'TIPSTER';
EXPERIMENT.collection.T08gop.original = 'T08';

% TREC 09, 2000, Web, Grid-of-Points
EXPERIMENT.collection.T09gop.id = 'T09gop';
EXPERIMENT.collection.T09gop.name =  'TREC 09, 2000, Web, GoP';
EXPERIMENT.collection.T09gop.topicSet.id = '451-500';
EXPERIMENT.collection.T09gop.topicSet.number = 50;
EXPERIMENT.collection.T09gop.corpus.id = 'WT10g';
EXPERIMENT.collection.T09gop.original = 'T09';

% TREC 10, 2001, Web, Grid-of-Points
EXPERIMENT.collection.T10gop.id = 'T10gop';
EXPERIMENT.collection.T10gop.name =  'TREC 10, 2001, Web, GoP';
EXPERIMENT.collection.T10gop.topicSet.id = '501-550';
EXPERIMENT.collection.T10gop.topicSet.number = 50;
EXPERIMENT.collection.T10gop.corpus.id = 'WT10g';
EXPERIMENT.collection.T10gop.original = 'T10';

% TREC 13, 2004, Terabyte, Grid-of-Points
EXPERIMENT.collection.T13gop.id = 'T13gop';
EXPERIMENT.collection.T13gop.name =  'TREC 13, 2004, Terabyte, GoP';
EXPERIMENT.collection.T13gop.topicSet.id = '701-750';
EXPERIMENT.collection.T13gop.topicSet.number = 49;
EXPERIMENT.collection.T13gop.corpus.id = 'GOV2';
EXPERIMENT.collection.T13gop.original = 'T13';

% TREC 14, 2005, Terabyte, Grid-of-Points
EXPERIMENT.collection.T14gop.id = 'T14gop';
EXPERIMENT.collection.T14gop.name =  'TREC 14, 2005, Terabyte, GoP';
EXPERIMENT.collection.T14gop.topicSet.id = '751-800';
EXPERIMENT.collection.T14gop.topicSet.number = 50;
EXPERIMENT.collection.T14gop.corpus.id = 'GOV2';
EXPERIMENT.collection.T14gop.original = 'T14';

% TREC 15, 2006, Terabyte, Grid-of-Points
EXPERIMENT.collection.T15gop.id = 'T15gop';
EXPERIMENT.collection.T15gop.name =  'TREC 16, 2006, Terabyte, GoP';
EXPERIMENT.collection.T15gop.topicSet.id = '801-850';
EXPERIMENT.collection.T15gop.topicSet.number = 50;
EXPERIMENT.collection.T15gop.corpus.id = 'GOV2';
EXPERIMENT.collection.T15gop.original = 'T15';

% TREC 13-15, 2004-2006, Terabyte, Grid-of-Points
EXPERIMENT.collection.T131415gop.id = 'T131415gop';
EXPERIMENT.collection.T131415gop.name =  'TREC 13-15, 2004-2006, Terabyte, GoP';


% Returns the name of a collection given its index in EXPERIMENT.collection.list
EXPERIMENT.collection.getName = @(idx) ( EXPERIMENT.collection.(EXPERIMENT.collection.list{idx}).name ); 


%% Patterns for file names

% MAT - Pattern EXPERIMENT.path.base/dataset/<trackID>/dataset_<trackID>.mat
EXPERIMENT.pattern.file.dataset = @(trackID) sprintf('%1$s%2$s%3$sdataset_%2$s.mat', EXPERIMENT.path.dataset, trackID, filesep);

% MAT - Pattern EXPERIMENT.path.base/measure/<trackID>/<measureID>.mat
EXPERIMENT.pattern.file.measure = @(trackID, measureID) sprintf('%1$s%2$s%3$s%4$s.mat', EXPERIMENT.path.measure, trackID, filesep, measureID);

% MAT - Pattern EXPERIMENT.path.base/samples/<sampleID>.mat
EXPERIMENT.pattern.file.sample = @(sampleID) sprintf('%1$s%2$s.mat', EXPERIMENT.path.samples, sampleID);

% MAT - Pattern EXPERIMENT.path.base/correlation/<trackID>/<correlationID>.mat
EXPERIMENT.pattern.file.correlation = @(trackID, correlationID) sprintf('%1$s%2$s%3$s%4$s.mat', EXPERIMENT.path.correlation, trackID, filesep, correlationID);

% MAT - Pattern EXPERIMENT.path.analysis/<rqID>/<rqID>_<correlationID>_<trackID>
EXPERIMENT.pattern.file.anova = @(rqID, correlationID, trackID) ...
    sprintf('%1$s%2$s%3$s%2$s_%4$s_%5$s.mat', EXPERIMENT.path.analysis, rqID, filesep, correlationID, trackID);

% MAT - Pattern EXPERIMENT.path.analysis/rq2/<rq1ID>
EXPERIMENT.pattern.file.rq2 = @(rq1ID) sprintf('%1$s%2$s%3$s%4$s.mat', EXPERIMENT.path.analysis, 'rq2', filesep, rq1ID);

% MAT - Pattern EXPERIMENT.path.base/pdf/<trackID>/<kldID>.mat
EXPERIMENT.pattern.file.pdf = @(trackID, kldID) sprintf('%1$s%2$s%3$s%4$s.mat', EXPERIMENT.path.pdf, trackID, filesep, kldID);

% PDF - Pattern EXPERIMENT.path.figure/<trackID>/<figureID>
EXPERIMENT.pattern.file.figure = @(trackID, figureID) ...
    sprintf('%1$s%2$s%3$s%4$s.pdf', EXPERIMENT.path.figure, trackID, filesep, figureID);

% TEX - Pattern EXPERIMENT.path.report/<rqID>_<info>_report.tex
EXPERIMENT.pattern.file.report = @(rqID, info) ...
    sprintf('%1$s%2$s_%3$s_report.tex', EXPERIMENT.path.report, rqID, info);


%% Patterns for identifiers

% Pattern <measureID>_<trackID>
EXPERIMENT.pattern.identifier.measure =  @(measureID, trackID) sprintf('%1$s_%2$s', measureID, trackID);

% Pattern sample_<systemsID>_<subjects>
EXPERIMENT.pattern.identifier.sample.systems = @(systemsID, subjects) sprintf('sample_%1$s_s%2$04d', systemsID, subjects);

% Pattern sample_<topicsID>_<subjects>_<trackID>
EXPERIMENT.pattern.identifier.sample.topics = @(topicsID, subjects, trackID) sprintf('sample_%1$s_s%2$04d_%3$s', topicsID, subjects, trackID);

% Pattern <correlationID>_<systemsID>_<topicsID>_<remove1Q>_<trackID>
EXPERIMENT.pattern.identifier.correlation = @(correlationID, systemsID, topicsID, remove1Q, trackID) sprintf('%1$s_%2$s_%3$s_%4$s_%5$s', correlationID, systemsID, topicsID, char(remove1Q*'no1Q' + (1-remove1Q)*'allQ'), trackID);

% Pattern <analysis>_<correlationID>_obs_<trackID>
EXPERIMENT.pattern.identifier.anovaObs =  @(analysis, correlationID, trackID) sprintf('%1$s_%2$s_obs_%3$s', analysis, correlationID, trackID);

% Pattern <analysis>_<correlationID>_me_<trackID>
EXPERIMENT.pattern.identifier.anovaMe =  @(analysis, correlationID, trackID) sprintf('%1$s_%2$s_me_%3$s', analysis, correlationID, trackID);

% Pattern <analysis>_<correlationID>_ie_<trackID>
EXPERIMENT.pattern.identifier.anovaIe =  @(analysis, correlationID, trackID) sprintf('%1$s_%2$s_ie_%3$s', analysis, correlationID, trackID);

% Pattern <analysis>_<correlationID>_table_<trackID>
EXPERIMENT.pattern.identifier.anovaTable =  @(analysis, correlationID, trackID) sprintf('%1$s_%2$s_table_%3$s', analysis, correlationID, trackID);

% Pattern <analysis>_<correlationID>_stats_<trackID>
EXPERIMENT.pattern.identifier.anovaStats =  @(analysis,correlationID, trackID) sprintf('%1$s_%2$s_stats_%3$s', analysis, correlationID, trackID);

% Pattern <analysis>_<correlationID>_soa_<trackID>
EXPERIMENT.pattern.identifier.anovaSoA =  @(analysis, correlationID, trackID) sprintf('%1$s_%2$s_soa_%3$s', analysis,correlationID, trackID);

% Pattern rq2_<correlationID>_<trackID>
EXPERIMENT.pattern.identifier.rq2 =  @(correlationID, trackID) sprintf('rq2_%1$s_%2$s', correlationID, trackID);

% Pattern pdf_<measureID>_<trackID>
EXPERIMENT.pattern.identifier.pdf =  @(measureID, trackID) sprintf('pdf_%1$s_%2$s', measureID, trackID);

% Pattern kld_<measureID>_<trackID>
EXPERIMENT.pattern.identifier.kld =  @(measureID, trackID) sprintf('kld_%1$s_%2$s', measureID, trackID);

% Pattern pdf_<measureID>_<trackID>
EXPERIMENT.pattern.identifier.figure.pdf = @(measureID, trackID) sprintf('pdf_%1$s_%2$s', measureID, trackID);

% Pattern <rqID>_<info>_<trackID>
EXPERIMENT.pattern.identifier.figure.general = @(rqID, info, trackID) sprintf('%1$s_%2$s_%3$s', rqID, info, trackID);

% Pattern <rqID>_mainEffects_<correlationID>_<trackID>
EXPERIMENT.pattern.identifier.figure.mainEffects = @(rqID, correlationID, trackID) ...
    EXPERIMENT.pattern.identifier.figure.general(rqID, sprintf('%s_mainEffects', correlationID) , trackID);

% Pattern <rqID>_interactionEffects_<correlationID>_<trackID>
EXPERIMENT.pattern.identifier.figure.interactionEffects = @(rqID, correlationID, trackID) ...
    EXPERIMENT.pattern.identifier.figure.general(rqID, sprintf('%s_interactionEffects', correlationID) , trackID);
    
% Pattern <rqID>_multcompare_<correlationID>_<trackID>
EXPERIMENT.pattern.identifier.figure.multcompare = @(rqID, correlationID, trackID) ...
    EXPERIMENT.pattern.identifier.figure.general(rqID, sprintf('%s_multcompare', correlationID) , trackID);
 
% Pattern <correlationID>_<systemsID>_<topicsID>_allQno1Q_ranks_<trackID>
EXPERIMENT.pattern.identifier.figure.allQ_no1Q = @(correlationID, systemsID, topicsID, trackID) ...
    sprintf('%1$s_%2$s_%3$s_allQno1Q_ranks_%4$s', correlationID, systemsID, topicsID, trackID);


%% Configuration for measures

% The list of measures under experimentation
EXPERIMENT.measure.list = {'ap', 'p10', 'rprec', 'rbp', 'ndcg', 'ndcg20', 'err', 'twist'};
EXPERIMENT.measure.number = length(EXPERIMENT.measure.list);

% Configuration for AP
EXPERIMENT.measure.ap.id = 'ap';
EXPERIMENT.measure.ap.acronym = 'AP';
EXPERIMENT.measure.ap.name = 'Average Precision';

% Configuration for P@10
EXPERIMENT.measure.p10.id = 'p10';
EXPERIMENT.measure.p10.acronym = 'P@10';
EXPERIMENT.measure.p10.name = 'Precision at 10 Retrieved Documents';

% Configuration for R-prec
EXPERIMENT.measure.rprec.id = 'rprec';
EXPERIMENT.measure.rprec.acronym = 'R-prec';
EXPERIMENT.measure.rprec.name = 'Precision at the Recall Base';

% Configuration for RBP
EXPERIMENT.measure.rbp.id = 'rbp';
EXPERIMENT.measure.rbp.acronym = 'RBP';
EXPERIMENT.measure.rbp.name = 'Rank-biased Precision';

% Configuration for nDCG
EXPERIMENT.measure.ndcg.id = 'ndcg';
EXPERIMENT.measure.ndcg.acronym = 'nDCG';
EXPERIMENT.measure.ndcg.name = 'Normalized Discounted Cumulated Gain at Last Retrieved Document';

% Configuration for nDCG@20
EXPERIMENT.measure.ndcg20.id = 'ndcg20';
EXPERIMENT.measure.ndcg20.acronym = 'nDCG@20';
EXPERIMENT.measure.ndcg20.name = 'Normalized Discounted Cumulated Gain at 20 Retrieved Documents';

% Configuration for ERR
EXPERIMENT.measure.err.id = 'err';
EXPERIMENT.measure.err.acronym = 'ERR';
EXPERIMENT.measure.err.name = 'Expected Reciprocal Rank at Last Retrieved Document';

% Configuration for Twist
EXPERIMENT.measure.twist.id = 'twist';
EXPERIMENT.measure.twist.acronym = 'Twist';
EXPERIMENT.measure.twist.name = 'Twist';

% Returns the identifier of a measure given its index in EXPERIMENT.measure.list
EXPERIMENT.measure.getID = @(idx) ( EXPERIMENT.measure.(EXPERIMENT.measure.list{idx}).id ); 

% Returns the acronym of a measure given its index in EXPERIMENT.measure.list
EXPERIMENT.measure.getAcronym = @(idx) ( EXPERIMENT.measure.(EXPERIMENT.measure.list{idx}).acronym ); 

% Returns the name of a measure given its index in EXPERIMENT.measure.list
EXPERIMENT.measure.getName = @(idx) ( EXPERIMENT.measure.(EXPERIMENT.measure.list{idx}).name ); 

%% Configuration for correlations

% The list of correlation measures under experimentation
EXPERIMENT.correlation.list = {'tau', 'tauAP'};
EXPERIMENT.correlation.number = length(EXPERIMENT.correlation.list);

% Number of subjects to be drawn
EXPERIMENT.correlation.subjects = 100;

% Labels of the correlation pairs
EXPERIMENT.correlation.labels.list = EXPERIMENT.measure.list(combnk(1:EXPERIMENT.measure.number, 2));
EXPERIMENT.correlation.labels.first = EXPERIMENT.correlation.labels.list(:, 1);
EXPERIMENT.correlation.labels.second = EXPERIMENT.correlation.labels.list(:, 2);
EXPERIMENT.correlation.labels.list(:, 3) = EXPERIMENT.correlation.labels.list(:, 2);
EXPERIMENT.correlation.labels.list(:, 2) = {'_'};
EXPERIMENT.correlation.labels.list = strcat(EXPERIMENT.correlation.labels.list(:, 1), EXPERIMENT.correlation.labels.list(:, 2), EXPERIMENT.correlation.labels.list(:, 3));
EXPERIMENT.correlation.labels.number = length(EXPERIMENT.correlation.labels.list);

% Configuration for Kendall's tau correlation
EXPERIMENT.correlation.tau.id = 'tau';
EXPERIMENT.correlation.tau.symbol.tex = '\tau';
EXPERIMENT.correlation.tau.symbol.latex = '$\tau$';
EXPERIMENT.correlation.tau.label.tex = 'Kendall''s \tau Correlation';
EXPERIMENT.correlation.tau.label.latex = 'Kendall''s $\tau$ Correlation';
EXPERIMENT.correlation.tau.name = 'Kendall''s tau correlation';
EXPERIMENT.correlation.tau.color.main = rgb('RoyalBlue');
EXPERIMENT.correlation.tau.color.no1Q = rgb('FireBrick');
EXPERIMENT.correlation.tau.color.alternate = rgb('RoyalBlue');
EXPERIMENT.correlation.tau.compute = @(data) corr(data, 'type', 'Kendall');

% Configuration for AP correlation without handling ties
EXPERIMENT.correlation.tauAP.id = 'tauAP';
EXPERIMENT.correlation.tauAP.symbol.tex = '\tau_{AP}';
EXPERIMENT.correlation.tauAP.symbol.latex = '$\tau_{AP}$';
EXPERIMENT.correlation.tauAP.label.tex = 'AP Correlation \tau_{AP}';
EXPERIMENT.correlation.tauAP.label.latex = 'AP Correlation $\tau_{AP}$';
EXPERIMENT.correlation.tauAP.name = 'AP correlation';
EXPERIMENT.correlation.tauAP.color.main = rgb('ForestGreen');
EXPERIMENT.correlation.tauAP.color.no1Q = rgb('DarkOrange');
EXPERIMENT.correlation.tauAP.color.alternate = rgb('ForestGreen');
EXPERIMENT.correlation.tauAP.compute = @(data) apCorr(data, 'Ties', false);

%% Configuration for list of systems

% The list of set of systems to be used for computing correlation
EXPERIMENT.systems.list = {'s0010', 's0025', 's0050', 's0075', 's0100', ...
                           's0125', 's0150', 's0200', 's0250', 's0500', ...
                           's0750', 's1000', 's1250'};
EXPERIMENT.systems.number = length(EXPERIMENT.systems.list);

EXPERIMENT.systems.s0010.id = 's0010';
EXPERIMENT.systems.s0010.description = 'List 10 random systems in a GoP';
EXPERIMENT.systems.s0010.sample = 10;
EXPERIMENT.systems.s0010.compute = @() rnd_systems(EXPERIMENT.systems.s0010.sample);

EXPERIMENT.systems.s0025.id = 's0025';
EXPERIMENT.systems.s0025.description = 'List 25 random systems in a GoP';
EXPERIMENT.systems.s0025.sample = 25;
EXPERIMENT.systems.s0025.compute = @() rnd_systems(EXPERIMENT.systems.s0025.sample);

EXPERIMENT.systems.s0050.id = 's0050';
EXPERIMENT.systems.s0050.description = 'List 50 random systems in a GoP';
EXPERIMENT.systems.s0050.sample = 50;
EXPERIMENT.systems.s0050.compute = @() rnd_systems(EXPERIMENT.systems.s0050.sample);

EXPERIMENT.systems.s0075.id = 's0075';
EXPERIMENT.systems.s0075.description = 'List 75 random systems in a GoP';
EXPERIMENT.systems.s0075.sample = 75;
EXPERIMENT.systems.s0075.compute = @() rnd_systems(EXPERIMENT.systems.s0075.sample);

EXPERIMENT.systems.s0100.id = 's0100';
EXPERIMENT.systems.s0100.description = 'List 100 random systems in a GoP';
EXPERIMENT.systems.s0100.sample = 100;
EXPERIMENT.systems.s0100.compute = @() rnd_systems(EXPERIMENT.systems.s0100.sample);

EXPERIMENT.systems.s0125.id = 's0125';
EXPERIMENT.systems.s0125.description = 'List 125 random systems in a GoP';
EXPERIMENT.systems.s0125.sample = 125;
EXPERIMENT.systems.s0125.compute = @() rnd_systems(EXPERIMENT.systems.s0125.sample);

EXPERIMENT.systems.s0150.id = 's0150';
EXPERIMENT.systems.s0150.description = 'List 150 random systems in a GoP';
EXPERIMENT.systems.s0150.sample = 150;
EXPERIMENT.systems.s0150.compute = @() rnd_systems(EXPERIMENT.systems.s0150.sample);

EXPERIMENT.systems.s0200.id = 's0200';
EXPERIMENT.systems.s0200.description = 'List 150 random systems in a GoP';
EXPERIMENT.systems.s0200.sample = 200;
EXPERIMENT.systems.s0200.compute = @() rnd_systems(EXPERIMENT.systems.s0200.sample);

EXPERIMENT.systems.s0250.id = 's0250';
EXPERIMENT.systems.s0250.description = 'List 250 random systems in a GoP';
EXPERIMENT.systems.s0250.sample = 250;
EXPERIMENT.systems.s0250.compute = @() rnd_systems(EXPERIMENT.systems.s0250.sample);

EXPERIMENT.systems.s0500.id = 's0500';
EXPERIMENT.systems.s0500.description = 'List 500 random systems in a GoP';
EXPERIMENT.systems.s0500.sample = 500;
EXPERIMENT.systems.s0500.compute = @() rnd_systems(EXPERIMENT.systems.s0500.sample);

EXPERIMENT.systems.s0750.id = 's0750';
EXPERIMENT.systems.s0750.description = 'List 750 random systems in a GoP';
EXPERIMENT.systems.s0750.sample = 750;
EXPERIMENT.systems.s0750.compute = @() rnd_systems(EXPERIMENT.systems.s0750.sample);

EXPERIMENT.systems.s1000.id = 's1000';
EXPERIMENT.systems.s1000.description = 'List 1000 random systems in a GoP';
EXPERIMENT.systems.s1000.sample = 1000;
EXPERIMENT.systems.s1000.compute = @() rnd_systems(EXPERIMENT.systems.s1000.sample);

EXPERIMENT.systems.s1250.id = 's1250';
EXPERIMENT.systems.s1250.description = 'List 1250 random systems in a GoP';
EXPERIMENT.systems.s1250.sample = 1250;
EXPERIMENT.systems.s1250.compute = @() rnd_systems(EXPERIMENT.systems.s1250.sample);


%% Configuration for list of topics

% The list of set of topics to be used for computing correlation
EXPERIMENT.topics.list = {'t010', 't020', 't030', 't040', 't050', ...
                          't060', 't070', 't080', 't090', 't100', ...
                          't110', 't120', 't130', 't140', 't148', ...
                          'tall'};
EXPERIMENT.topics.number = length(EXPERIMENT.topics.list);

EXPERIMENT.topics.tall.id = 'tall';
EXPERIMENT.topics.tall.description = 'List all the topics in a GoP';
EXPERIMENT.topics.tall.sample = 50;
EXPERIMENT.topics.tall.compute = @(topics) topics;

EXPERIMENT.topics.t010.id = 't010';
EXPERIMENT.topics.t010.description = 'List 10 random topics in a GoP';
EXPERIMENT.topics.t010.sample = 10;
EXPERIMENT.topics.t010.compute = @(topics) rnd_topics(topics, EXPERIMENT.topics.t010.sample);

EXPERIMENT.topics.t020.id = 't020';
EXPERIMENT.topics.t020.description = 'List 20 random topics in a GoP';
EXPERIMENT.topics.t020.sample = 20;
EXPERIMENT.topics.t020.compute = @(topics) rnd_topics(topics, EXPERIMENT.topics.t020.sample);

EXPERIMENT.topics.t030.id = 't030';
EXPERIMENT.topics.t030.description = 'List 30 random topics in a GoP';
EXPERIMENT.topics.t030.sample = 30;
EXPERIMENT.topics.t030.compute = @(topics) rnd_topics(topics, EXPERIMENT.topics.t030.sample);

EXPERIMENT.topics.t040.id = 't040';
EXPERIMENT.topics.t040.description = 'List 40 random topics in a GoP';
EXPERIMENT.topics.t040.sample = 40;
EXPERIMENT.topics.t040.compute = @(topics) rnd_topics(topics, EXPERIMENT.topics.t040.sample);

EXPERIMENT.topics.t050.id = 't050';
EXPERIMENT.topics.t050.description = 'List 50 random topics in a GoP';
EXPERIMENT.topics.t050.sample = 50;
EXPERIMENT.topics.t050.compute = @(topics) rnd_topics(topics, EXPERIMENT.topics.t050.sample);

EXPERIMENT.topics.t060.id = 't060';
EXPERIMENT.topics.t060.description = 'List 60 random topics in a GoP';
EXPERIMENT.topics.t060.sample = 60;
EXPERIMENT.topics.t060.compute = @(topics) rnd_topics(topics, EXPERIMENT.topics.t060.sample);

EXPERIMENT.topics.t070.id = 't070';
EXPERIMENT.topics.t070.description = 'List 70 random topics in a GoP';
EXPERIMENT.topics.t070.sample = 70;
EXPERIMENT.topics.t070.compute = @(topics) rnd_topics(topics, EXPERIMENT.topics.t070.sample);

EXPERIMENT.topics.t080.id = 't080';
EXPERIMENT.topics.t080.description = 'List 80 random topics in a GoP';
EXPERIMENT.topics.t080.sample = 80;
EXPERIMENT.topics.t080.compute = @(topics) rnd_topics(topics, EXPERIMENT.topics.t080.sample);

EXPERIMENT.topics.t090.id = 't090';
EXPERIMENT.topics.t090.description = 'List 90 random topics in a GoP';
EXPERIMENT.topics.t090.sample = 90;
EXPERIMENT.topics.t090.compute = @(topics) rnd_topics(topics, EXPERIMENT.topics.t090.sample);

EXPERIMENT.topics.t100.id = 't100';
EXPERIMENT.topics.t100.description = 'List 100 random topics in a GoP';
EXPERIMENT.topics.t100.sample = 100;
EXPERIMENT.topics.t100.compute = @(topics) rnd_topics(topics, EXPERIMENT.topics.t100.sample);

EXPERIMENT.topics.t110.id = 't110';
EXPERIMENT.topics.t110.description = 'List 110 random topics in a GoP';
EXPERIMENT.topics.t110.sample = 110;
EXPERIMENT.topics.t110.compute = @(topics) rnd_topics(topics, EXPERIMENT.topics.t110.sample);

EXPERIMENT.topics.t120.id = 't120';
EXPERIMENT.topics.t120.description = 'List 120 random topics in a GoP';
EXPERIMENT.topics.t120.sample = 120;
EXPERIMENT.topics.t120.compute = @(topics) rnd_topics(topics, EXPERIMENT.topics.t120.sample);

EXPERIMENT.topics.t130.id = 't130';
EXPERIMENT.topics.t130.description = 'List 130 random topics in a GoP';
EXPERIMENT.topics.t130.sample = 130;
EXPERIMENT.topics.t130.compute = @(topics) rnd_topics(topics, EXPERIMENT.topics.t130.sample);

EXPERIMENT.topics.t140.id = 't140';
EXPERIMENT.topics.t140.description = 'List 140 random topics in a GoP';
EXPERIMENT.topics.t140.sample = 140;
EXPERIMENT.topics.t140.compute = @(topics) rnd_topics(topics, EXPERIMENT.topics.t140.sample);

EXPERIMENT.topics.t148.id = 't148';
EXPERIMENT.topics.t148.description = 'List 148 random topics in a GoP';
EXPERIMENT.topics.t148.sample = 148;
EXPERIMENT.topics.t148.compute = @(topics) rnd_topics(topics, EXPERIMENT.topics.t148.sample);

%% Configuration for analyses

% The significance level for the analyses
EXPERIMENT.analysis.alpha = 0.05;

% Configuration for KL divergence analyses
EXPERIMENT.analysis.pdf.id = 'pdf';
EXPERIMENT.analysis.pdf.name = 'Original vs GoP Analysis of Measure PDF';
EXPERIMENT.analysis.pdf.description = 'Analysis of the Probability Density Function (PDF) of a Measure Computed on the Original Runset and on the GoP Runset';
EXPERIMENT.analysis.pdf.kde.xi = -0.1:0.01:1.1;
EXPERIMENT.analysis.pdf.kde.b = 0.015;
EXPERIMENT.analysis.pdf.kde.compute = @(measure) kdEstimation(measure{:,:}(:), EXPERIMENT.analysis.pdf.kde.xi, EXPERIMENT.analysis.pdf.kde.b);
EXPERIMENT.analysis.pdf.kld.compute = @(origPDF, gopPDF) klDivergence(EXPERIMENT.analysis.pdf.kde.xi, gopPDF, origPDF);
EXPERIMENT.analysis.pdf.kld.plot = @(pdf, style) plot(EXPERIMENT.analysis.pdf.kde.xi, pdf, 'Linewidth', 3, 'LineStyle', style);


% Configuration for ANOVA factorial analyses
EXPERIMENT.analysis.rq1.id = 'rq1';
EXPERIMENT.analysis.rq1.name = 'Measure Pair/Topic Size/System Size Effects';
EXPERIMENT.analysis.rq1.description = 'Crossed effects GLMM: subjects are system rankings; measure pairs, topic size and system size are effects';
EXPERIMENT.analysis.rq1.subject = 'System Ranking';
EXPERIMENT.analysis.rq1.factorA = 'Measure Pair';
EXPERIMENT.analysis.rq1.factorB = 'Topic Size';
EXPERIMENT.analysis.rq1.factorC = 'System Size';
% the model = System Ranking (subject) + Measure Pair (factorA) + Topic Size (factorB) + System Size (factorC) + 
%             Measure Pair*Topic Size + Measure Pair*System Size + Topic Size*System Size 
EXPERIMENT.analysis.rq1.model = [1 0 0 0; ... 
                                 0 1 0 0; ...
                                 0 0 1 0; ...         
                                 0 0 0 1; ... 
                                 0 1 1 0; ... 
                                 0 1 0 1; ... 
                                 0 0 1 1];
EXPERIMENT.analysis.rq1.compute = @(data, subject, factorA, factorB, factorC) anovan(data, {subject, factorA, factorB, factorC}, ...
        'Model', EXPERIMENT.analysis.rq1.model, ...
        'VarNames', {EXPERIMENT.analysis.rq1.subject, EXPERIMENT.analysis.rq1.factorA, EXPERIMENT.analysis.rq1.factorB, EXPERIMENT.analysis.rq1.factorC}, ...
        'alpha', EXPERIMENT.analysis.alpha, 'display', 'off');

EXPERIMENT.analysis.rq3.id = 'rq3';
EXPERIMENT.analysis.rq3.name = 'Measure Pair/Corpus/Topic Set Effects';
EXPERIMENT.analysis.rq3.description = 'Mixed effects GLMM: subjects are system rankings, measure pairs, corpora and topic sets, nested within corpora, are effects';
EXPERIMENT.analysis.rq3.subject = 'System Ranking';
EXPERIMENT.analysis.rq3.factorA = 'Measure Pair';
EXPERIMENT.analysis.rq3.factorB = 'Corpus';
EXPERIMENT.analysis.rq3.factorC = 'Topic Set';
% the model = System Ranking (subject) + Measure Pair (factorA) + 
%             Corpus (factorB) + Topic Set|Corpus (factorC) + 
%             Measure Pair*Corpus + Measure Pair*Topics Set
EXPERIMENT.analysis.rq3.model = [1 0 0 0; ... 
                                 0 1 0 0; ...
                                 0 0 1 0; ...
                                 0 0 0 1; ...
                                 0 1 1 0; ...
                                 0 1 0 1];
EXPERIMENT.analysis.rq3.nesting = [0 0 0 0; ...
                                   0 0 0 0; ...
                                   0 0 0 0; ...
                                   0 0 1 0];
EXPERIMENT.analysis.rq3.compute = @(data, subject, factorA, factorB, factorC) anovan(data, {subject, factorA, factorB, factorC}, ...
        'Model', EXPERIMENT.analysis.rq3.model, ...
        'VarNames', {EXPERIMENT.analysis.rq3.subject, EXPERIMENT.analysis.rq3.factorA, EXPERIMENT.analysis.rq3.factorB, EXPERIMENT.analysis.rq3.factorC}, ...
        'Nested', EXPERIMENT.analysis.rq3.nesting, ...        
        'alpha', EXPERIMENT.analysis.alpha, 'display', 'off');
       