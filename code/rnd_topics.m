%% rnd_topics
% 
% List the requested number of topics randomly sampled in a GoP.

%% Synopsis
%
%   [list] = all_topics(topics, sample)
%
% *Parameters*
%
% * *|topics|* - the list of the topics in a GoP.
% * *|sample|* - the number of topics to be randomly sampled.
%
% *Returns*
%
% * *|list|*  - the list of all the topics identifiers in a GoP.

%% Information
% 
% * *Author*: <mailto:ferro@dei.unipd.it Nicola Ferro>
% * *Version*: 1.00
% * *Since*: 1.00
% * *Requirements*: Matlab 2015b or higher
% * *Copyright:* (C) 2016 <http://ims.dei.unipd.it/ Information 
% Management Systems> (IMS) research group, <http://www.dei.unipd.it/ 
% Department of Information Engineering> (DEI), <http://www.unipd.it/ 
% University of Padua>, Italy
% * *License:* <http://www.apache.org/licenses/LICENSE-2.0 Apache License, 
% Version 2.0>

%%
%{
 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at
 
      http://www.apache.org/licenses/LICENSE-2.0
 
 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.
%}

%%

function [list] = rnd_topics(topics, sample)

    % check the number of input arguments
    narginchk(2, 2);

    % check that topics is a non-empty cell of string
    validateattributes(topics, {'cell'}, {'nonempty', 'vector'}, '', 'topics');
    
    % check that topics is a cell array of strings with at least one element
    assert(numel(topics) >= 1, ...
        'MATTERS:IllegalArgument', 'Expected topics to be a cell array of strings containing at least one string.');
 
    l = length(topics);
    
    validateattributes(sample, {'numeric'}, ...
            {'nonempty', 'integer', 'scalar', '>=', 1, '<=', l }, '', 'sample');

    % the ids of the topics
     list = topics(randperm(l, sample));
end

