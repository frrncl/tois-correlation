%% rq3_plot_rq1_comparison
% 
% Plots the comparison of the measure pair main effects for the given
% correlation coefficient betwwen the Measure Pair/Topic Size/System Size
% and Measure Pair/Corpus/Topic Set ANOVA.

%% Synopsis
%
%   [] = rq3_plot_rq1_comparison(correlationID, trackID, varargin)
%  
% *Parameters*
%
% * *|correlationID|* - the identifier of correlation to be plotted.
% * *|trackID|* - the identifier of the track for which the processing is
% performed for RQ1.
% * *|trackID|* - the identifier of the track for which the processing is
% performed for RQ3 (varargin).
%
% *Returns*
%
% Nothing.

%% Information
% 
% * *Author*: <mailto:ferro@dei.unipd.it Nicola Ferro>
% * *Version*: 1.00
% * *Since*: 1.00
% * *Requirements*: Matlab 2015b or higher
% * *Copyright:* (C) 2016 <http://ims.dei.unipd.it/ Information 
% Management Systems> (IMS) research group, <http://www.dei.unipd.it/ 
% Department of Information Engineering> (DEI), <http://www.unipd.it/ 
% University of Padua>, Italy
% * *License:* <http://www.apache.org/licenses/LICENSE-2.0 Apache License, 
% Version 2.0>

%%
%{
 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at
 
      http://www.apache.org/licenses/LICENSE-2.0
 
 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.
%}

%%

function [] = rq3_plot_rq1_comparison(correlationID, trackID, varargin)

    persistent TAG1 TAG2;
    
    if isempty(TAG1)
        TAG1 = 'rq3';
        TAG2 = 'rq1';
    end;
    
    % check the number of input parameters
    narginchk(3, inf);

    % load common parameters
    common_parameters
    
     % check that correlationID is a non-empty string
    validateattributes(correlationID, {'char', 'cell'}, {'nonempty', 'vector'}, '', 'correlationID');
    
    if iscell(correlationID)
        % check that correlationID is a cell array of strings with one element
        assert(iscellstr(correlationID) && numel(correlationID) == 1, ...
            'MATTERS:IllegalArgument', 'Expected correlationID to be a cell array of strings containing just one string.');
    end
    
    % remove useless white spaces, if any, and ensure it is a char row
    correlationID = char(strtrim(correlationID));
    correlationID = correlationID(:).';
    
    % check that correlationID assumes a valid value
    validatestring(correlationID, ...
        EXPERIMENT.correlation.list, '', 'correlationID');

    tracks = length(varargin);
    
    for t = 1:tracks
        
        % check that trackID is a non-empty string
        validateattributes(varargin{t}, {'char', 'cell'}, {'nonempty', 'vector'}, '', 'trackID');
        
        if iscell(varargin{t})
            
            % check that trackID is a cell array of strings with one element
            assert(iscellstr(varargin{t}) && numel(varargin{t}) == 1, ...
                'MATTERS:IllegalArgument', 'Expected trackID to be a cell array of strings containing just one string.');
        end
        
        % remove useless white spaces, if any, and ensure it is a char row
        varargin{t} = char(strtrim(varargin{t}));
        varargin{t} = varargin{t}(:).';
                
        % check that trackID assumes a valid value
        validatestring(varargin{t}, ...
            EXPERIMENT.collection.list, '', 'trackID');
    end;
    
    tracks_list = strjoin(varargin, '');
    
    
    % check that trackID is a non-empty string
    validateattributes(trackID, {'char', 'cell'}, {'nonempty', 'vector'}, '', 'trackID');

     if iscell(trackID)
        % check that trackID is a cell array of strings with one element
        assert(iscellstr(trackID) && numel(trackID) == 1, ...
            'MATTERS:IllegalArgument', 'Expected trackID to be a cell array of strings containing just one string.');
    end

    % remove useless white spaces, if any, and ensure it is a char row
    trackID = char(strtrim(trackID));
    trackID = trackID(:).';
    
    % check that trackID assumes a valid value
    validatestring(trackID, ...
        EXPERIMENT.collection.list, '', 'trackID');
       
    labels = cell(1, EXPERIMENT.correlation.labels.number);
    
    % for each label
    for l = 1:EXPERIMENT.correlation.labels.number
        
        first = EXPERIMENT.measure.(EXPERIMENT.correlation.labels.first{l}).acronym;
        second = EXPERIMENT.measure.(EXPERIMENT.correlation.labels.second{l}).acronym;
        
        labels{l} = sprintf('%s vs %s', first, second);
    end;
    
    xTick = 1:2;

    colors = parula(EXPERIMENT.correlation.labels.number);
    colors = colors(end:-1:1, :);
    
    % start of overall computations
    startComputation = tic;
            
    fprintf('\n\n######## Plotting %s vs %s comparison for correlation %s on collection %s (%s) ########\n\n', ...
        TAG1, TAG2, correlationID, tracks_list, EXPERIMENT.label.paper);
    
    fprintf('+ Settings\n');
    fprintf('  - computed on %s\n', datestr(now, 'yyyy-mm-dd at HH:MM:SS'));
    fprintf('  - analysis %s vs %s\n', TAG1, TAG2);
    fprintf('  - %s tracks %s\n', TAG1, tracks_list);
    fprintf('  - %s track %s\n', TAG2, trackID);
    
           
    anovaMeID = EXPERIMENT.pattern.identifier.anovaMe(TAG2, correlationID, trackID);
               
    serload(EXPERIMENT.pattern.file.anova(TAG2, correlationID, trackID), {anovaMeID 'me'});
    
    rq1_factorA = me.factorA.mean;
    
    [~, idxRq1] = sort(rq1_factorA, 'ascend');
    
    clear me;
    
    anovaMeID = EXPERIMENT.pattern.identifier.anovaMe(TAG1, correlationID, tracks_list);
               
    serload(EXPERIMENT.pattern.file.anova(TAG1, correlationID, tracks_list), {anovaMeID 'me'});
    
    rq3_factorA = me.factorA.mean;
    
    [~, idxRq3] = sort(rq3_factorA, 'ascend');
    
    clear me;
          
    % correlation among rankings of evauation measures
    tau = corr(rq1_factorA, rq3_factorA, 'type', 'Kendall');
    tauAP = apCorr(rq1_factorA, rq3_factorA, 'Ties', false);

    % rmse between vectors
    v1 = rq1_factorA - mean(rq1_factorA);
    v2 = rq3_factorA - mean(rq3_factorA);
    rmse = rmseCoeff(v1(:), v2(:));
    
    
    yTickLabel = cell(2, EXPERIMENT.correlation.labels.number);
    % for each label
    for l = 1:EXPERIMENT.correlation.labels.number        
        yTickLabel{1, l} = sprintf('%s - %-6.4f', labels{l}, rq1_factorA(l));
        yTickLabel{2, l} = sprintf('%-6.4f - %s', rq3_factorA(l), labels{l});
    end;
    
    [~, idx] = ismember(idxRq1, idxRq3);
    
    data = 1:EXPERIMENT.correlation.labels.number;
    
    data = [data.' idx];

    currentFigure = figure('Visible', 'off');    
    
        ax = axes('Parent', currentFigure);
        ax.TickLabelInterpreter = 'tex';
        ax.FontSize = 42;
        ax.Position = [0.2500 0.1100 0.5000 0.8150];

        ax.XLim = xTick;
        ax.XTick = xTick;
        ax.XTickLabel = {sprintf('%s as in %s', EXPERIMENT.correlation.(correlationID).symbol.tex, upper(TAG2)), ...
                         sprintf('%s as in %s', EXPERIMENT.correlation.(correlationID).symbol.tex, upper(TAG1))};

        ax.YAxisLocation = 'left';
        ax.YLim = [1 EXPERIMENT.correlation.labels.number];
        ax.YTick = 1:EXPERIMENT.correlation.labels.number;
        ax.YTickLabel = yTickLabel(1, idxRq1);

        ax.YLabel.Interpreter = 'tex';
        ax.YLabel.String = sprintf('RoMP by %s as in %s', EXPERIMENT.correlation.(correlationID).label.tex, upper(TAG2));


        ax2 = axes('Parent', currentFigure);

        ax2.TickLabelInterpreter =  ax.TickLabelInterpreter;
        ax2.FontSize = ax.FontSize;
        ax2.Position = ax.Position;

        ax2.XLim = ax.XLim;
        ax2.XTick = ax.XTick;
        ax2.XTickLabel = ax.XTickLabel;

        ax2.YAxisLocation = 'right';
        ax2.YLim = ax.YLim;
        ax2.YTick = ax.YTick;
        ax2.YTickLabel = yTickLabel(2, idxRq3);

        ax2.YLabel.Interpreter = ax.YLabel.Interpreter;
        ax2.YLabel.String = sprintf('RoMP by %s as in %s', EXPERIMENT.correlation.(correlationID).label.tex, upper(TAG1));

        hold on;

        % for each label
        for l = 1:EXPERIMENT.correlation.labels.number        
            plot(xTick, data(l, :), '-', 'Color', colors(l, :), ...
                'Marker', 'o', 'MarkerSize', 6, 'LineWidth', 3, ...
                'MarkerFaceColor', colors(l, :), ...
                'MarkerEdgeColor',  colors(l, :));
        end;

        title(sprintf('Correlation among RoMP: %s = %-6.4f; %s = %-6.4f', ...
            'tauCorr', tau, ...
            'apCorr', tauAP), ...
            'FontSize', 42, 'Interpreter', 'tex');
        
        currentFigure.PaperPositionMode = 'auto';
        currentFigure.PaperUnits = 'centimeters';
        currentFigure.PaperSize = [102 52];
        currentFigure.PaperPosition = [1 1 100 50];
        
        figureID = EXPERIMENT.pattern.identifier.figure.general([TAG1 '_' TAG2], [correlationID '_ranks'], tracks_list);

        print(currentFigure, '-dpdf', EXPERIMENT.pattern.file.figure(TAG1, figureID));

        close(currentFigure)
        
    currentFigure = figure('Visible', 'off');
    
        idx = idxRq1(end:-1:1);
    
        plot(1:EXPERIMENT.correlation.labels.number, rq1_factorA(idx) - mean(rq1_factorA(idx)), ...
            'Color', EXPERIMENT.correlation.(correlationID).color.alternate, ...
            'LineStyle', '-', 'LineWidth', 3);
        hold on
        plot(1:EXPERIMENT.correlation.labels.number, rq3_factorA(idx) - mean(rq3_factorA(idx)), ...
            'Color', EXPERIMENT.correlation.(correlationID).color.main, ...
            'LineStyle', '--', 'LineWidth', 3);
        
        
        ax = gca;
    
        ax.FontSize = 42;    
        
        ax.XTick = 1:EXPERIMENT.correlation.labels.number;
        ax.XTickLabel = labels;
        ax.XTickLabelRotation = 90;
    
        title(sprintf('RMSE between %s and %s curves = %-6.4f.', ...
            upper(TAG2), ...
            upper(TAG1), ...
            rmse), ...
            'FontSize', 42, 'Interpreter', 'tex');
        
        legend({sprintf('%s as in %s', EXPERIMENT.correlation.(correlationID).symbol.tex, upper(TAG2)), ...
                sprintf('%s as in %s', EXPERIMENT.correlation.(correlationID).symbol.tex, upper(TAG1))}, ...
                'FontSize', 32, 'Location', 'SouthWest', 'Interpreter', 'tex')
            
        ax.YLabel.Interpreter = 'tex';
        %ax.YLabel.String = sprintf('%s Marginal Mean Centered around Zero',  EXPERIMENT.correlation.(correlationID).label.tex);    
        ax.YLabel.String = sprintf('Correlation Marginal Mean Centered around Zero');    
            
        currentFigure.PaperPositionMode = 'auto';
        currentFigure.PaperUnits = 'centimeters';
        currentFigure.PaperSize = [52 52];
        currentFigure.PaperPosition = [1 1 50 50];
    
        figureID = EXPERIMENT.pattern.identifier.figure.general([TAG1 '_' TAG2], [correlationID '_mp_me'], tracks_list);
    
        print(currentFigure, '-dpdf', EXPERIMENT.pattern.file.figure(TAG1, figureID));
    
        close(currentFigure)
    
       
    fprintf('\n\n######## Total elapsed time for plotting %s vs %s main effects for correlation %s on collection %s (%s): %s ########\n\n', ...
           TAG1, TAG2, correlationID, ...
           tracks_list, EXPERIMENT.label.paper, elapsedToHourMinutesSeconds(toc(startComputation)));
       
    diary off;
end

