%% rq3_analysis
% 
% Computes the Measure Pair/Corpus/Topic Set Effects ANOVA.

%% Synopsis
%
%   [] = rq3_analysis(correlationID, varargin)
%  
% *Parameters*
%
% * *|correlationID|* - the identifier of correlation to be plotted.
% * *|trackID|* - the identifier of the track for which the processing is
% performed.
%
%
% *Returns*
%
% Nothing.

%% Information
% 
% * *Author*: <mailto:ferro@dei.unipd.it Nicola Ferro>
% * *Version*: 1.00
% * *Since*: 1.00
% * *Requirements*: Matlab 2015b or higher
% * *Copyright:* (C) 2016 <http://ims.dei.unipd.it/ Information 
% Management Systems> (IMS) research group, <http://www.dei.unipd.it/ 
% Department of Information Engineering> (DEI), <http://www.unipd.it/ 
% University of Padua>, Italy
% * *License:* <http://www.apache.org/licenses/LICENSE-2.0 Apache License, 
% Version 2.0>

%%
%{
 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at
 
      http://www.apache.org/licenses/LICENSE-2.0
 
 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.
%}

%%

function [] = rq3_analysis(correlationID, varargin)

    persistent TAG REMOVE_1Q TOPICS SYSTEMS;
    
    if isempty(TAG)
        TAG = 'rq3';
        REMOVE_1Q = false;
        TOPICS = 'tall';
        SYSTEMS = 's0100';
    end;

    % check the number of input parameters
    narginchk(2, inf);

    % load common parameters
    common_parameters
          
    % check that correlationID is a non-empty string
    validateattributes(correlationID, {'char', 'cell'}, {'nonempty', 'vector'}, '', 'correlationID');
    
    if iscell(correlationID)
        % check that correlationID is a cell array of strings with one element
        assert(iscellstr(correlationID) && numel(correlationID) == 1, ...
            'MATTERS:IllegalArgument', 'Expected correlationID to be a cell array of strings containing just one string.');
    end
    
    % remove useless white spaces, if any, and ensure it is a char row
    correlationID = char(strtrim(correlationID));
    correlationID = correlationID(:).';
    
    % check that correlationID assumes a valid value
    validatestring(correlationID, ...
        EXPERIMENT.correlation.list, '', 'correlationID');
    
    tracks = length(varargin);
    
    for t = 1:tracks
        
        % check that trackID is a non-empty string
        validateattributes(varargin{t}, {'char', 'cell'}, {'nonempty', 'vector'}, '', 'trackID');
        
        if iscell(varargin{t})
            
            % check that trackID is a cell array of strings with one element
            assert(iscellstr(varargin{t}) && numel(varargin{t}) == 1, ...
                'MATTERS:IllegalArgument', 'Expected trackID to be a cell array of strings containing just one string.');
        end
        
        % remove useless white spaces, if any, and ensure it is a char row
        varargin{t} = char(strtrim(varargin{t}));
        varargin{t} = varargin{t}(:).';
                
        % check that trackID assumes a valid value
        validatestring(varargin{t}, ...
            EXPERIMENT.collection.list, '', 'trackID');
    end;
    
    tracks_list = strjoin(varargin, '');
    
    % start of overall computations
    startComputation = tic;
            
    fprintf('\n\n######## Performing %s ANOVA analysis for correlation %s on collection(s) %s (%s) ########\n\n', ...
        TAG, correlationID, tracks_list, EXPERIMENT.label.paper);
    
    fprintf('+ Settings\n');
    fprintf('  - computed on %s\n', datestr(now, 'yyyy-mm-dd at HH:MM:SS'));
    fprintf('  - analysis %s\n', TAG);
    fprintf('  - tracks %s\n', tracks_list);
    fprintf('  - subject %d\n', EXPERIMENT.correlation.subjects);
    fprintf('  - remove first quartile %d\n\n', REMOVE_1Q);
    
    fprintf('+ Analysing the correlations\n');
        
    % total number of data points
    N = EXPERIMENT.correlation.labels.number * EXPERIMENT.correlation.subjects * tracks;
        
    % the data
    data = NaN(1, N);         

    
    % subjects: for each pair (topic size, system size) there are 
    % EXPERIMENT.correlation.subjects system rankings; for each
    % system ranking the correlations between measures pairs are computed
    subject = repmat(cellstr(reshape(num2str(1:EXPERIMENT.correlation.subjects, 'sbj%04d'), 7, EXPERIMENT.correlation.subjects).'), 1, EXPERIMENT.correlation.labels.number).';
    subject = subject(:);
    subject = repmat(subject, tracks, 1);
    
    % factorA: correlation between EXPERIMENT.correlation.labels.number 
    % measures pairs
    factorA = repmat(EXPERIMENT.correlation.labels.list.', 1, EXPERIMENT.correlation.subjects * tracks);
        
    % factorB: corpus
    factorB = cell(1, N);     
    
    % factorC: topic set, nested within corpus
    factorC = cell(1, N);     
    
    % the current element in the list
    currentElement = 1;
    
    % for each track
    for t = 1:tracks
        
        trackID = varargin{t};
        corrID = EXPERIMENT.pattern.identifier.correlation(correlationID, SYSTEMS, TOPICS, REMOVE_1Q, trackID);
        
        serload(EXPERIMENT.pattern.file.correlation(trackID, corrID), {corrID, 'tmp'});
        
        range = (currentElement-1)*EXPERIMENT.correlation.subjects*EXPERIMENT.correlation.labels.number+1:currentElement*EXPERIMENT.correlation.subjects*EXPERIMENT.correlation.labels.number;
        
        % copy the correlations in the correct range of the data
        data(range) = reshape(tmp.values(1:EXPERIMENT.correlation.subjects, :).', EXPERIMENT.correlation.subjects*EXPERIMENT.correlation.labels.number, 1);
        
        % set the correct grouping variables
        factorB(range) = {EXPERIMENT.collection.(trackID).corpus.id};
        factorC(range) = {EXPERIMENT.collection.(trackID).topicSet.id};        
        
        % increment the current element counter
        currentElement = currentElement + 1;
        
        clear('tmp');
        
    end % track
        
    % compute the model
    evalf(EXPERIMENT.analysis.(TAG).compute, ...
        {'data', 'subject', 'factorA', 'factorB', 'factorC'}, ...
        {'p', 'tbl', 'stats'});    

    df_measure = tbl{3,3};
    ss_measure = tbl{3,2};
    F_measure = tbl{3,6};
    
    df_corpus = tbl{4,3};
    ss_corpus = tbl{4,2};
    F_corpus = tbl{4,6};
    
    df_topicSet = tbl{5,3};
    ss_topicSet = tbl{5,2};
    F_topicSet = tbl{5,6};
    
    df_measure_corpus = tbl{6,3};
    ss_measure_corpus = tbl{6,2};
    F_measure_corpus = tbl{6,6};
    
    df_measure_topicSet = tbl{7,3};
    ss_measure_topicSet = tbl{7,2};
    F_measure_topicSet = tbl{7,6};
              
    ss_error = tbl{8, 2};
    df_error = tbl{8, 3};
    
    ss_total = tbl{9, 2};
    
    % compute the strength of association
    soa.omega2.measure = df_measure * (F_measure - 1) / (df_measure * F_measure + df_corpus * F_corpus +  df_topicSet * F_topicSet + df_measure_corpus * F_measure_corpus + df_measure_topicSet * F_measure_topicSet + df_error + 1);
    soa.omega2.corpus = df_corpus * (F_corpus - 1) / (df_measure * F_measure + df_corpus * F_corpus +  df_topicSet * F_topicSet + df_measure_corpus * F_measure_corpus + df_measure_topicSet * F_measure_topicSet + df_error + 1);
    soa.omega2.topicSet = df_topicSet * (F_topicSet - 1) / (df_measure * F_measure + df_corpus * F_corpus +  df_topicSet * F_topicSet + df_measure_corpus * F_measure_corpus + df_measure_topicSet * F_measure_topicSet + df_error + 1);
    soa.omega2.measure_corpus = df_measure_corpus * (F_measure_corpus - 1) / (df_measure * F_measure + df_corpus * F_corpus +  df_topicSet * F_topicSet + df_measure_corpus * F_measure_corpus + df_measure_topicSet * F_measure_topicSet + df_error + 1);
    soa.omega2.measure_topicSet = df_measure_topicSet * (F_measure_topicSet - 1) / (df_measure * F_measure + df_corpus * F_corpus +  df_topicSet * F_topicSet + df_measure_corpus * F_measure_corpus + df_measure_topicSet * F_measure_topicSet + df_error + 1);
    
    soa.omega2p.measure = df_measure * (F_measure - 1) / (df_measure * (F_measure - 1) + N);
    soa.omega2p.corpus = df_corpus * (F_corpus - 1) / (df_corpus * (F_corpus - 1) + N);
    soa.omega2p.topicSet = df_topicSet * (F_topicSet - 1) / (df_topicSet * (F_topicSet - 1) + N);    
    soa.omega2p.measure_corpus =  df_measure_corpus * (F_measure_corpus - 1) / (df_measure_corpus * (F_measure_corpus - 1) + N);
    soa.omega2p.measure_topicSet =  df_measure_topicSet * (F_measure_topicSet - 1) / (df_measure_topicSet * (F_measure_topicSet - 1) + N);
    
    soa.eta2.measure = ss_measure / ss_total;
    soa.eta2.corpus = ss_corpus / ss_total;
    soa.eta2.topicSet = ss_topicSet / ss_total;    
    soa.eta2.measure_corpus = ss_measure_corpus / ss_total;
    soa.eta2.measure_topicSet = ss_measure_topicSet / ss_total;
    
    soa.eta2p.measure = ss_measure / (ss_measure + ss_error);
    soa.eta2p.corpus = ss_corpus / (ss_corpus + ss_error);
    soa.eta2p.topicSet = ss_topicSet / (ss_topicSet + ss_error);    
    soa.eta2p.measure_corpus = ss_measure_corpus / (ss_measure_corpus + ss_error);
    soa.eta2p.measure_topicSet = ss_measure_topicSet / (ss_measure_topicSet + ss_error);
        
    % raw data
    obs.data = data;
    obs.subject = subject;
    obs.factorA = factorA;
    obs.factorB = factorB;
    obs.factorC = factorC;
    obs.tracks = varargin;
    obs.corpora = unique(factorB, 'stable');
    obs.topicSets = unique(factorC, 'stable');

    % main effects
    [me.factorA.mean, me.factorA.ci] = grpstats(data(:), factorA(:), {'mean', 'meanci'});
    [me.factorB.mean, me.factorB.ci] = grpstats(data(:), factorB(:), {'mean', 'meanci'});
    [me.factorC.mean, me.factorC.ci] = grpstats(data(:), factorC(:), {'mean', 'meanci'});
    
    % interaction between corpora (x-axis) and measure pair (y-axis)
    % each row is a measure pair, columns are tracks
    ie.factorBA.mean = grpstats(data(:), {factorB(:),factorA(:)}, {'mean'});
    ie.factorBA.mean = reshape(ie.factorBA.mean, EXPERIMENT.correlation.labels.number, length(obs.corpora)).';
    
    % interaction between topic sets (x-axis) and measure pair (y-axis)
    % each row is a measure pair, columns are tracks
    ie.factorCA.mean = grpstats(data(:), {factorC(:),factorA(:)}, {'mean'});
    ie.factorCA.mean = reshape(ie.factorCA.mean, EXPERIMENT.correlation.labels.number, length(obs.topicSets)).';
    
    anovaObsID = EXPERIMENT.pattern.identifier.anovaObs(TAG, correlationID, tracks_list);
    anovaMeID = EXPERIMENT.pattern.identifier.anovaMe(TAG, correlationID, tracks_list);
    anovaIeID = EXPERIMENT.pattern.identifier.anovaIe(TAG, correlationID, tracks_list);
    anovaTableID = EXPERIMENT.pattern.identifier.anovaTable(TAG, correlationID, tracks_list);
    anovaStatsID = EXPERIMENT.pattern.identifier.anovaStats(TAG, correlationID, tracks_list);
    anovaSoAID = EXPERIMENT.pattern.identifier.anovaSoA(TAG, correlationID, tracks_list);
    
    eval(sprintf('%s = tbl;', anovaTableID));
    eval(sprintf('%s = stats;', anovaStatsID));
    eval(sprintf('%s = obs;', anovaObsID));
    eval(sprintf('%s = me;', anovaMeID));
    eval(sprintf('%s = ie;', anovaIeID));
    eval(sprintf('%s = soa;', anovaSoAID));
    
    sersave(EXPERIMENT.pattern.file.anova(TAG, correlationID, tracks_list), ...
        anovaTableID(:), anovaStatsID(:), anovaSoAID(:), anovaObsID(:), anovaMeID(:), anovaIeID(:))
     
       
    fprintf('\n\n######## Total elapsed time for performing %s ANOVA analysis for correlation %s on collection(s) %s (%s): %s ########\n\n', ...
           TAG, correlationID, tracks_list, ...
           EXPERIMENT.label.paper, elapsedToHourMinutesSeconds(toc(startComputation)));
       
    diary off;
end

