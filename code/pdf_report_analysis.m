%% pdf_report_analysis
% 
% Reports the the PDF and KLD analyses between GoP and original runs 
% and saves them to a |.tex| file.
%
%% Synopsis
%
%   [] = pdf_report_analysis(varargin)
%  
%
% *Parameters*
%
% * *|trackID|* - the identifiers of the tracks for which the processing is
% performed.

%
% *Returns*
%
% Nothing
%

%% Information
% 
% * *Author*: <mailto:ferro@dei.unipd.it Nicola Ferro>
% * *Version*: 1.00
% * *Since*: 1.00
% * *Requirements*: MATTERS 1.0 or higher; Matlab 2013b or higher
% * *Copyright:* (C) 2015 <http://ims.dei.unipd.it/ Information 
% Management Systems> (IMS) research group, <http://www.dei.unipd.it/ 
% Department of Information Engineering> (DEI), <http://www.unipd.it/ 
% University of Padua>, Italy
% * *License:* <http://www.apache.org/licenses/LICENSE-2.0 Apache License, 
% Version 2.0>

%%
%{
 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at
 
      http://www.apache.org/licenses/LICENSE-2.0
 
 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.
%}

function [] = pdf_report_analysis(varargin)

    persistent TAG;
    
    if isempty(TAG)
        TAG = 'pdf';
    end

    % check the number of input arguments
    narginchk(1, inf);

    % setup common parameters
    common_parameters;
    
    trk = length(varargin);
    
    
    for t = 1:trk
        
        trackID = varargin{t};
        
    
        % check that trackID is a non-empty string
        validateattributes(trackID, {'char', 'cell'}, {'nonempty', 'vector'}, '', 'trackID');

        if iscell(trackID)

            % check that trackID is a cell array of strings with one element
            assert(iscellstr(trackID) && numel(trackID) == 1, ...
                'MATTERS:IllegalArgument', 'Expected trackID to be a cell array of strings containing just one string.');
        end

        % remove useless white spaces, if any, and ensure it is a char row
        trackID = char(strtrim(trackID));
        trackID = trackID(:).';

        % check that trackID assumes a valid value
        validatestring(trackID, ...
            EXPERIMENT.collection.list, '', 'trackID');
        
        varargin{t} = trackID;
    end
    
    trackList = strjoin(varargin, '_');
        
    % start of overall computations
    startComputation = tic;

    fprintf('\n\n######## Report PDF analyses (%s) ########\n\n', ...
        EXPERIMENT.label.paper);

    fprintf('+ Settings\n');
    fprintf('  - computed on %s\n', datestr(now, 'yyyy-mm-dd at HH:MM:SS'));
        
    fprintf('+ Printing the report\n');
    
    % the file where the report has to be written
    fid = fopen(EXPERIMENT.pattern.file.report(TAG, trackList), 'w');


    fprintf(fid, '\\documentclass[11pt]{article} \n\n');

    fprintf(fid, '\\usepackage{amsmath}\n');
    fprintf(fid, '\\usepackage{multirow}\n');
    fprintf(fid, '\\usepackage{colortbl}\n');
    fprintf(fid, '\\usepackage{lscape}\n');
    fprintf(fid, '\\usepackage{pdflscape}\n');
    fprintf(fid, '\\usepackage{rotating}\n');
    fprintf(fid, '\\usepackage[a3paper]{geometry}\n\n');
        
    fprintf(fid, '\\begin{document}\n\n');
    
    fprintf(fid, '\\title{Summary Report on PDF Analyses}\n\n');
    
    fprintf(fid, '\\maketitle\n\n');
    
    fprintf(fid, 'Tracks:\n');
    fprintf(fid, '\\begin{itemize}\n');
    
    for t = 1:trk
        fprintf(fid, '\\item %s\n', EXPERIMENT.collection.(varargin{t}).name);
    end
    
    fprintf(fid, '\\end{itemize}\n');
    
    
    %fprintf(fid, '\\begin{landscape}  \n');
    fprintf(fid, '\\begin{table}[p] \n');
    % fprintf(fid, '\\tiny \n');
    fprintf(fid, '\\centering \n');
    %fprintf(fid, '\\hspace*{-6.5em} \n');
    
    fprintf(fid, '\\begin{tabular}{|l|*{%d}{r|}} \n', EXPERIMENT.measure.number);
    
    
    fprintf(fid, '\\multicolumn{1}{|c|}{\\textbf{Track}}');
    for m = 1:EXPERIMENT.measure.number
        fprintf(fid, '& \\multicolumn{1}{c|}{\\textbf{%s}} ', EXPERIMENT.measure.getAcronym(m));       
    end; % measures
    fprintf(fid, '\\\\ \n');
    
    fprintf(fid, '\\hline \n');
    
    for t = 1:trk
        
        trackID = varargin{t};
        
        fprintf(fid, '%s vs %s  ', EXPERIMENT.collection.(trackID).original, trackID);
        
        for m = 1:EXPERIMENT.measure.number
            
            kldGopID = EXPERIMENT.pattern.identifier.kld(EXPERIMENT.measure.list{m}, trackID);
            
            serload2(EXPERIMENT.pattern.file.pdf(trackID, kldGopID), ...
                'WorkspaceVarNames', {'kld'}, ...
                'FileVarNames', {kldGopID});
            
            fprintf(fid, '& %-6.4f ', kld);
            
            clear kld;
            
        end; % measures
        
        fprintf(fid, '\\\\ \n');
    
        fprintf(fid, '\\hline \n');
        
    end; % tracks
           
    fprintf(fid, '\\hline \n');
    
    fprintf(fid, '\\end{tabular} \n');
    
    fprintf(fid, '\\caption{PDF analysis.}\n');
    
    fprintf(fid, '\\label{tab:pdf-kld} \n');
    
    fprintf(fid, '\\end{table} \n\n');
    
    %fprintf(fid, '\\end{landscape}  \n');
    
    fprintf(fid, '\\end{document} \n\n');
    
    
    fclose(fid);
    
    
               
    fprintf('\n\n######## Total elapsed time for reporting PDF analysis (%s): %s ########\n\n', ...
            EXPERIMENT.label.paper, elapsedToHourMinutesSeconds(toc(startComputation)));

    diary off;
end
